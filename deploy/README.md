# InternaSUS deployment instructions

## Standalone docker

Requirements:

* docker

Running: `docker run -p 3000:3000 registry.gitlab.com/interscity/health-dashboard/internasus`

## Ansible

Requirements:

* [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* [git-lfs](https://git-lfs.github.com/) client

Configuration:

* Create inventory file
  - https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
  - You can base yourself on the [staging](deploy/staging)
* Retrieve large files
  - `git lfs install`
  - `git lfs fetch origin master`
* Create the `.vault-pass.txt` where the password for file decryption must be placed
  - If you do not have access to the vault password, you must remove the following encrypted files and start your own vault:
    * `procedures.csv`
  - You can find more about the ansible vault [here](https://docs.ansible.com/ansible/latest/user_guide/vault.html)

Running:

* `ansible-playbook -i staging setup.yml -K`
  - This playbook installs system dependencies, you probably won't want to run it more than once
* `ansible-playbook -i staging deploy.yml`
  - This will take care of bringing up the application, invoke it after every release (the CI should be doing this)
  - **Before running**
    * make sure you have set the `INTERNASUS_DATABASE_PASSWORD` environment variable
    * login to the docker registry within the host with the deployer user (`docker login registry.gitlab.com`)
