# InternaSUS

Application for spatial visualization of hospitalization data for São Paulo city.

## Ruby version

The version is defined by the `.ruby-version` file.

## System dependencies

* Ruby
  - Our preferred way of installation is using [RVM](https://rvm.io/)

* [Yarn](https://yarnpkg.com/lang/en/)
  - You can install it using npm by running `npm install -g yarn`

* [wkhtmltopdf](https://wkhtmltopdf.org/)
  - You can install it from your OS package manager
  - Or run `gem install wkhtmltopdf-binary`
  - Or [download](https://wkhtmltopdf.org/downloads.html) it

## Database

This platform needs the insertion of the following data files to work:
* `/vendor/*_health_centres.csv`, to load health facilities data
* `/vendor/procedures.csv`, to load the hospitalizations and patients data
* `/vendor/cid10/*.csv`, to load all diagnosis ICD codes
* `/vendor/census_sectors/*.json`, to load census sectors shapes
* `/vendor/*.geojson`, to load the sector shapes are shown on the map
  - This application uses Sao Paulo city sectors: Administrative Sector, Subprefecture, Regional Health Coordination, and Technical Health Supervision

## Configuration

Just run `./bin/setup` to get an development environment working.

## How to run the test suite

* Linter:
  - for code style run `rubocop`
  - for code duplications run `flay -s **/*.rb`
  - for TypeScript run `yarn lint`
* All tests: `rake`
* Unit tests: `rails spec`
* JS unit tests: `yarn test`
* Acceptance tests: `rails cucumber`

## Deployment instructions

Check [deploy/README.md](deploy/README.md).

## Building the docker image

If you wish to build your image from source instead of using [our registry ](https://gitlab.com/interscity/health-dashboard/internasus/container_registry), run:

* `docker build . -t internasus`

## License

[Mozilla Public License v2](COPYING)
