# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IcdCategory, type: :model do
  it { is_expected.to have_many(:icd_subcategories) }
  it { is_expected.to belong_to(:icd_group).required(false) }
  it { is_expected.to belong_to(:icd_chapter).required(false) }
  it { is_expected.to have_many(:hospitalization_data) }
end
