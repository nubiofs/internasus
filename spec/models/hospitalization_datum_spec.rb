# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/procedure_id_association'

RSpec.describe HospitalizationDatum, type: :model do
  subject { FactoryBot.build(:hospitalization_datum) }

  it { is_expected.to belong_to(:health_facility) }
  it { is_expected.to belong_to(:icd_subcategory) }
  it { is_expected.to belong_to(:icd_category) }
  it { is_expected.to belong_to(:secondary_icd1).optional }
  it { is_expected.to belong_to(:secondary_icd2).optional }
  it { is_expected.to have_one(:patient_datum) }
end
