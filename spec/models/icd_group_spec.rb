# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IcdGroup, type: :model do
  it { is_expected.to have_many(:icd_categories) }
  it { is_expected.to belong_to(:icd_chapter).optional }
end
