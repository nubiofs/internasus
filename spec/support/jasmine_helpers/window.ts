import browserenv = require('browser-env');

declare global {
  interface Window {
    ProceduresMap: any;
    LimitsFeatures: any;
    LoadingOverlay: any;
    CensusSectorsPopulationFilters: any;
    ImageGenerator: any;
    GeneralData: any;
  }
}

if (typeof window === 'undefined') {
  browserenv();
}
