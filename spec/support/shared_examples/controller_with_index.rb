# frozen_string_literal: true

RSpec.shared_examples 'controller with index' do |controller|
  it "is expected to route #{controller}/ to #{controller}#index" do
    expect(get: controller).to route_to(controller: controller, action: 'index')
  end
end
