# frozen_string_literal: true

RSpec.shared_examples 'queries, group and count' do |klass, query, group|
  it "is expected to query for #{klass}, group by #{group} and count" do
    expect(klass).to have_received(:where).with(query)
    expect(klass.where).to have_received(:group).with(group)
    expect(klass.where.group).to have_received(:count)
  end
end
