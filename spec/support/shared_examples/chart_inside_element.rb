# frozen_string_literal: true

RSpec.shared_examples 'chart inside element' do |stats, selector, id_list|
  before do
    allow(view).to receive(:render).and_call_original

    assign(:stats, stats)

    render
  end

  it 'is expected to have the selector for each id of the list' do
    id_list.each do |id|
      expect(rendered).to have_selector(selector, id: id)
    end
  end
end
