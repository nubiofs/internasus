# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'homepage/index.html.erb', type: :view do
  let(:hospitalization_counter) { 554.202 }
  let(:health_facility_counter) { 97 }
  let(:specialty_counter) { 9 }
  let(:mean_distance) { 10 }

  before do
    assign(:hospitalization_counter, hospitalization_counter)
    assign(:health_facility_counter, health_facility_counter)
    assign(:specialty_counter, specialty_counter)
    assign(:mean_distance, mean_distance)
    render
  end

  context 'slider images' do
    it 'is expected to display the first slide' do
      expect(rendered).to have_selector('div', class: 'carousel-item')
      expect(rendered).to match(/slide1.*\.jpg/)
    end

    it 'is expected to display the second slide' do
      expect(rendered).to have_selector('div', class: 'carousel-item')
      expect(rendered).to match(/slide2.*\.jpg/)
    end

    it 'is expected to display the third slide' do
      expect(rendered).to have_selector('div', class: 'carousel-item')
      expect(rendered).to match(/slide3.*\.jpg/)
    end

    it 'is expected to display the fourth slide' do
      expect(rendered).to have_selector('div', class: 'carousel-item')
      expect(rendered).to match(/slide4.*\.jpg/)
    end
  end

  context 'counter' do
    it 'is expected to display the hospitalization counter' do
      expect(rendered).to have_selector('p', class: 'counter')
      expect(rendered).to match(/#{number_to_human(hospitalization_counter)}/)
    end

    it 'is expected to display the health facility counter' do
      expect(rendered).to have_selector('p', class: 'counter')
      expect(rendered).to match(/#{health_facility_counter}/)
    end

    it 'is expected to display the specialty counter' do
      expect(rendered).to have_selector('p', class: 'counter')
      expect(rendered).to match(/#{specialty_counter}/)
    end

    it 'is expected to display the mean distance counter' do
      expect(rendered).to have_selector('p', class: 'counter')
      expect(rendered).to match(/#{number_to_human(mean_distance)} Km/)
    end
  end

  context 'services' do
    it 'is expected to display the advanced search image' do
      expect(rendered).to have_selector('img', class: 'card-img-top')
      expect(rendered).to match(/search.*\.png/)
    end

    it 'is expected to display the health facilities image' do
      expect(rendered).to have_selector('img', class: 'card-img-top')
      expect(rendered).to match(/facilities.*\.png/)
    end

    it 'is expected to display the general data image' do
      expect(rendered).to have_selector('img', class: 'card-img-top')
      expect(rendered).to match(/data.*\.png/)
    end

    it 'is expected to display the about page image' do
      expect(rendered).to have_selector('img', class: 'card-img-top')
      expect(rendered).to match(/about.*\.png/)
    end

    it 'is expected to display a link to advanced search' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.advanced_search'),
        href: procedures_path
      )
    end

    xit 'is expected to display a link to health facilities' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.health_facilities'),
        href: '#'
      )
    end

    it 'is expected to display a link to general data' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.general_data'),
        href: general_data_path
      )
    end

    xit 'is expected to display a link to about' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.footer.about'),
        href: '#'
      )
    end
  end
end
