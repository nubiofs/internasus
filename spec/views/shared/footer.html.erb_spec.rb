# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'shared/_footer.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to display the secretariat name' do
    expect(rendered).to have_content(
      I18n.t('views.shared.footer.sms')
    )
  end

  it 'is expected to display the contact' do
    expect(rendered).to have_content(
      I18n.t('views.shared.footer.contact')
    )
  end

  xit 'is expected to display a link to about' do
    expect(rendered).to have_selector(
      :link,
      I18n.t('views.shared.footer.about'),
      href: '#',
      class: 'link'
    )
  end

  xit 'is expected to display a link to FAQ' do
    expect(rendered).to have_selector(
      :link,
      I18n.t('views.shared.footer.faq'),
      href: '#',
      class: 'link'
    )
  end
end
