# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'shared/_header.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to display the partner logo' do
    expect(rendered).to have_selector('img', class: 'partner-logo')
    expect(rendered).to match(/partner-logo.*\.png/)
  end

  it 'is expected to display the application name' do
    expect(rendered).to have_selector(
      :link,
      I18n.t('views.shared.header.application_name'),
      href: root_path,
      class: 'navbar-brand'
    )
  end

  context 'menu links' do
    it 'is expected to display a link to advanced search' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.advanced_search'),
        href: procedures_path,
        class: 'nav-link'
      )
    end

    xit 'is expected to display a link to health facilities' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.health_facilities'),
        href: '#',
        class: 'nav-link'
      )
    end

    it 'is expected to display a link to general data' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.general_data'),
        href: general_data_path,
        class: 'nav-link'
      )
    end

    it 'is expected to display the user menu toggle' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.user'),
        href: '#',
        class: 'nav-link dropdown-toggle'
      )
    end

    context 'without a signed in user' do
      it 'is expected to display a link to login' do
        expect(rendered).to have_selector(
          :link,
          I18n.t('views.shared.header.login'),
          href: new_user_session_path,
          class: 'dropdown-item'
        )
      end
    end

    context 'with a signed in user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user

        render
      end

      it 'is expected to display a link to logout' do
        expect(rendered).to have_selector(
          :link,
          I18n.t('views.shared.header.logout'),
          href: destroy_user_session_path,
          class: 'dropdown-item'
        )
      end

      it 'is expected to not display a link to admin' do
        expect(rendered).to_not have_selector(
          :link,
          I18n.t('views.shared.header.admin')
        )
      end

      context 'and this user is an admin' do
        before do
          user.update!(admin: true)
          sign_out user
          sign_in user

          render
        end

        it 'is expected to display a link to admin' do
          expect(rendered).to have_selector(
            :link,
            I18n.t('views.shared.header.admin'),
            href: trestle.root_path,
            class: 'dropdown-item'
          )
        end
      end
    end
  end
end
