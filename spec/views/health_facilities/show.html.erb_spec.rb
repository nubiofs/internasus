# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'health_facilities/show.html.erb', type: :view do
  let(:health_facility_data) do
    FactoryBot.build(
      :health_facility,
      administrative_sector: FactoryBot.build(:administrative_sector),
      subprefecture: FactoryBot.build(:subprefecture),
      technical_health_supervision: FactoryBot.build(:technical_health_supervision),
      regional_health_coordination: FactoryBot.build(:regional_health_coordination)
    )
  end

  before do
    assign(:health_facility, health_facility_data)

    render
  end

  [
    I18n.t('health_facilities.show.name'),
    I18n.t('health_facilities.show.administrative_sector'),
    I18n.t('health_facilities.show.subprefecture'),
    I18n.t('health_facilities.show.technical_health_supervision'),
    I18n.t('health_facilities.show.regional_health_coordination'),
    I18n.t('health_facilities.show.beds')
  ].each do |content|
    it "is expected to have content \"#{content}\"" do
      expect(rendered).to have_content(content)
    end
  end

  it 'is expected to contain the health facility data' do
    [
      health_facility_data.name,
      health_facility_data.administrative_sector.name,
      health_facility_data.subprefecture.name,
      health_facility_data.technical_health_supervision.name,
      health_facility_data.regional_health_coordination.name,
      health_facility_data.beds
    ].each do |content|
      expect(rendered).to have_content(content)
    end
  end
end
