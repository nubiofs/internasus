# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'

RSpec.describe 'procedures/map/_options.html.erb', type: :view do
  before do
    render
  end

  describe 'fields' do
    include_examples 'contain field',
                     'input[type=checkbox]',
                     'clustering_enabled',
                     I18n.t('procedures.map.options.enabled')

    include_examples 'contain field',
                     'input[type=text]',
                     'clustering_radius',
                     I18n.t('procedures.map.options.radius')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'heat_map_enabled',
                     I18n.t('procedures.map.options.enabled')

    include_examples 'contain field',
                     'input[type=text]',
                     'heat_map_radius',
                     I18n.t('procedures.map.options.radius')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'heat_map_high_contrast',
                     I18n.t('procedures.map.options.high_contrast')

    include_examples 'contain field',
                     'input[type=text]',
                     'heat_map_opacity',
                     I18n.t('procedures.map.options.opacity')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_city_limits',
                     I18n.t('procedures.map.options.city')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'csw_heat_map_enabled',
                     I18n.t('procedures.map.options.enabled')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_rhc_limits',
                     I18n.t('procedures.map.options.regional_health_coordination')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_ths_limits',
                     I18n.t('procedures.map.options.technical_health_supervision')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_subprefecture_limits',
                     I18n.t('procedures.map.options.subprefecture')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_administrative_sectors_limits',
                     I18n.t('procedures.map.options.administrative_sector')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_ubs_limits',
                     I18n.t('procedures.map.options.ubs')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_fhs_limits',
                     I18n.t('procedures.map.options.fhs')
  end
end
