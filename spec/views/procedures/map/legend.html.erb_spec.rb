# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'

RSpec.describe 'procedures/map/_legend.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to contain the title' do
    expect(rendered).to have_content(I18n.t('procedures.map.legend.title'))
  end

  [
    { image: 'ESTADUAL', translation: 'state' },
    { image: 'MUNICIPAL', translation: 'city' }
  ].each do |item|
    it 'is expected to contain the state health facility legend' do
      expect(rendered).to have_selector("img[src*='#{item[:image]}']")
      expect(rendered).to have_content(I18n.t("procedures.map.legend.#{item[:translation]}"))
    end
  end
end
