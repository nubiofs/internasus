# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'
require 'support/helpers'

RSpec.configure do |c|
  c.include Ransack::Helpers::FormHelper
  c.include TestHelpers
end

RSpec.describe 'procedures/filters/_procedures.html.erb', type: :view do
  before do
    render_with_ransack_form 'procedures/filters/procedures.html.erb'
  end

  include_examples 'contain field',
                   'select',
                   'q_icd_category',
                   I18n.t('procedures.index.procedure.icd_category'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_icd_subcategory',
                   I18n.t('procedures.index.procedure.icd_subcategory'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_hospitalization_type',
                   I18n.t('procedures.index.procedure.hospitalization_type'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_complexity',
                   I18n.t('procedures.index.procedure.complexity'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_specialty',
                   I18n.t('procedures.index..procedure.specialty'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_competence',
                   I18n.t('procedures.index.procedure.competence'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_hospitalization_group',
                   I18n.t('procedures.index.procedure.hospitalization_group'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_financing',
                   I18n.t('procedures.index.procedure.financing'),
                   'in'

  include_examples 'contain field',
                   'input',
                   'q_hospitalization_days',
                   I18n.t('procedures.index.procedure.hospitalization_days'),
                   'between'

  include_examples 'contain field',
                   'input',
                   'q_hospitalization_rates',
                   I18n.t('procedures.index.procedure.hospitalization_rates'),
                   'between'

  include_examples 'contain field',
                   'input',
                   'q_uti_rates',
                   I18n.t('procedures.index.procedure.uti_rates'),
                   'between'

  include_examples 'contain field',
                   'input',
                   'q_ui_rates',
                   I18n.t('procedures.index.procedure.ui_rates'),
                   'between'

  include_examples 'contain field',
                   'input',
                   'q_service_value',
                   I18n.t('procedures.index.procedure.service_value'),
                   'between'
end
