# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'
require 'support/helpers'

RSpec.configure do |c|
  c.include Ransack::Helpers::FormHelper
  c.include TestHelpers
end

RSpec.describe 'procedures/filters/_patients.html.erb', type: :view do
  before do
    render_with_ransack_form 'procedures/filters/patients.html.erb'
  end

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_age_code',
                   I18n.t('procedures.index.patient.age_code'),
                   'cont_any'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_race',
                   I18n.t('procedures.index.patient.race'),
                   'cont_any'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_educational_level',
                   I18n.t('procedures.index.patient.educational_level'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_gender',
                   I18n.t('procedures.index.patient.gender'),
                   'eq_any'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_administrative_sector',
                   I18n.t('procedures.index.administrative_sector'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_subprefecture',
                   I18n.t('procedures.index.subprefecture'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_technical_health_supervision',
                   I18n.t('procedures.index.technical_health_supervision'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_regional_health_coordination',
                   I18n.t('procedures.index.regional_health_coordination'),
                   'id_in'

  include_examples 'contain field',
                   'input',
                   'q_distance',
                   I18n.t('procedures.index.procedure.distance'),
                   'between'
end
