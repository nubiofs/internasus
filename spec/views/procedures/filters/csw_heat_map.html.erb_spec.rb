# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'

RSpec.describe 'procedures/filters/_csw_heat_map.html.erb', type: :view do
  before do
    render
  end

  describe 'fields' do
    CensusSectorsProceduresCounterService::ALL_FILTERS.each do |filter|
      include_examples 'contain field',
                       'input[type=checkbox]',
                       "csw_heat_map_#{filter}",
                       I18n.t("procedures.filters.csw_heat_map.#{filter}")
    end
  end
end
