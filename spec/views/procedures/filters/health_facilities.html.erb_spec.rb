# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'
require 'support/helpers'

RSpec.configure do |c|
  c.include Ransack::Helpers::FormHelper
  c.include TestHelpers
end

RSpec.describe 'procedures/filters/_health_facilities.html.erb', type: :view do
  before do
    render_with_ransack_form 'procedures/filters/health_facilities.html.erb'
  end

  include_examples 'contain field',
                   'select',
                   'q_health_facility',
                   I18n.t('procedures.index.hf.name'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_health_facility_administration',
                   I18n.t('procedures.index.hf.administration'),
                   'cont_any'

  include_examples 'contain field',
                   'select',
                   'q_health_facility_administrative_sector',
                   I18n.t('procedures.index.administrative_sector'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_health_facility_subprefecture',
                   I18n.t('procedures.index.subprefecture'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_health_facility_technical_health_supervision',
                   I18n.t('procedures.index.technical_health_supervision'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_health_facility_regional_health_coordination',
                   I18n.t('procedures.index.regional_health_coordination'),
                   'id_in'

  include_examples 'contain field',
                   'input',
                   'q_health_facility_beds',
                   I18n.t('procedures.index.hf.beds'),
                   'between'
end
