# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/print.html.erb', type: :view do
  let(:encoded_map_content) { 'base64_encoded_map' }
  let(:active_filters) { {} }
  let(:filter_partial_params) do
    {
      partial: 'shared/services/active_filters',
      locals: { active_filters: active_filters }
    }
  end

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(**filter_partial_params)

    assign(:base64_map, encoded_map_content)
    assign(:active_filters, active_filters)

    render
  end

  it 'is expected to render the map as an image' do
    expect(rendered).to match(encoded_map_content)
  end

  it 'is expected to show the search active filters' do
    expect(view).to have_received(:render).with(**filter_partial_params)
  end
end
