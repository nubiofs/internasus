# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/index.html.erb', type: :view do
  let(:patients_data) { double('patients_data') }
  let(:search) { HospitalizationDatum.ransack([]) }
  let(:census_sectors) { double('census_sectors') }

  before do
    expect(patients_data).to receive(:find_in_batches)
    expect(census_sectors).to receive(:find_in_batches)

    assign(:patients_data, patients_data)
    assign(:search, search)
    assign(:census_sectors, census_sectors)
    assign(:census_sectors_counts, {})

    render
  end

  context 'page element' do
    context 'map' do
      it 'is expected to have the div where the map will be loaded' do
        expect(rendered).to have_selector('div', id: 'map', class: 'col-md-8')
      end
    end

    context 'filters list' do
      it 'is expected to have a form' do
        expect(rendered).to have_selector("form[method=post][action=#{search_procedures_path.gsub('/', '\/')}]")
      end

      it 'is expected to display the search button' do
        expect(rendered).to have_button(I18n.t('procedures.index.search'))
      end
    end

    it 'is expected to have a link for CSV download' do
      expect(rendered).to have_link(I18n.t('procedures.index.download_csv'), href: export_procedures_path(format: :csv))
    end
  end
end
