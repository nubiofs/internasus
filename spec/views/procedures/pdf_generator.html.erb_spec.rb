# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/_pdf_generator.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to have a form' do
    expect(rendered).to have_selector(
      :xpath, "//form[@action='#{print_procedures_path}'][@method='post']", id: 'image_form'
    )
  end

  it 'is expected to have a hidden input to hold the map image' do
    expect(rendered).to have_selector(:xpath, '//input[@type="hidden"]', id: 'map_image', visible: false)
  end

  it 'is expected to have a link' do
    expect(rendered).to have_link(
      I18n.t('procedures.pdf_generator.pdf'), href: 'javascript:;', class: 'm-2 btn btn-secondary'
    )
  end
end
