# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/_procedure.csv.erb', type: :view do
  let(:administrative_sector) { FactoryBot.build(:administrative_sector) }
  let(:subprefecture) { FactoryBot.build(:subprefecture) }
  let(:technical_health_supervision) { FactoryBot.build(:technical_health_supervision) }
  let(:regional_health_coordination) { FactoryBot.build(:regional_health_coordination) }
  let(:health_facility) { FactoryBot.build(:health_facility) }
  let(:icd_subcategory) { FactoryBot.build(:icd_subcategory) }
  let(:hospitalization_datum) do
    FactoryBot.build(
      :hospitalization_datum,
      health_facility: health_facility,
      icd_subcategory: icd_subcategory
    )
  end
  let(:patient_datum) do
    FactoryBot.build(
      :patient_datum,
      hospitalization_datum: hospitalization_datum,
      administrative_sector: administrative_sector,
      subprefecture: subprefecture,
      technical_health_supervision: technical_health_supervision,
      regional_health_coordination: regional_health_coordination
    )
  end

  before do
    render partial: 'procedures/procedure.csv', locals: { patient_data: patient_datum }
  end

  %w[procedure_id latitude longitude gender age_year race educational_level hospitalization_datum.health_facility.cnes
     hospitalization_datum.hospitalization_type hospitalization_datum.competence hospitalization_datum.issue_date
     hospitalization_datum.admission_date hospitalization_datum.leave_date hospitalization_datum.complexity
     hospitalization_datum.hospitalization_group hospitalization_datum.icd_subcategory.code
     hospitalization_datum.hospitalization_rates hospitalization_datum.uti_rates hospitalization_datum.uti_rates
     hospitalization_datum.hospitalization_days hospitalization_datum.financing hospitalization_datum.service_value
     administrative_sector.name subprefecture.name technical_health_supervision.name regional_health_coordination.name
     hospitalization_datum.secondary_icd1\ ?\ patient_data.hospitalization_datum.secondary_icd1.code\ :\ nil
     hospitalization_datum.secondary_icd2\ ?\ patient_data.hospitalization_datum.secondary_icd2.code\ :\ nil
     hospitalization_datum.distance hospitalization_datum.gestor_id].each do |attr|
    it "is expected to render the attribute #{attr} value" do
      # This is a test and there is no user input here, so I'm using eval knowing the risk and saying it does not apply
      # to this case. This is fair use.
      #
      # rubocop:disable Security/Eval
      attr_value = eval <<-ATTR_V, binding, __FILE__, __LINE__ + 1
        patient_datum.#{attr}
      ATTR_V
      # rubocop:enable Security/Eval
      expect(rendered).to match(/#{attr_value}/)
    end
  end
end
