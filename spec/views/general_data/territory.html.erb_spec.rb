# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_inside_element'

RSpec.describe 'general_data/_territory.html.erb', type: :view do
  it_behaves_like 'chart inside element',
                  {
                    administrative_sectors_by_name: {},
                    regional_health_coordinations: {},
                    technical_health_supervisions: {}
                  },
                  'div', %w[territory-AS-chart territory-RHC-chart territory-THS-chart]
end
