# frozen_string_literal: true

require 'rails_helper'

def i18nt(key)
  I18n.t("general_data.characterization.#{key}")
end

RSpec.describe 'general_data/_characterization.html.erb', type: :view do
  let(:total_records) { 42 }
  let(:procedures_ids) { double('procedures_ids') }
  let(:total_filtered_records) { 314 }
  let(:active_filters) { double('active_filters') }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(partial: 'shared/services/active_filters',
                                         locals: { active_filters: active_filters })

    expect(procedures_ids).to receive(:count).and_return(total_filtered_records)

    assign(:total_records, total_records)
    assign(:procedures_ids, procedures_ids)
    assign(:active_filters, active_filters)

    render
  end

  it 'is expected to contain the data source' do
    expect(rendered).to have_content("#{i18nt('source')}: Sistema de Internações Hospitalares SUS - SMS/SP")
  end

  it 'is expected to contain the total records count' do
    expect(rendered).to have_content("#{i18nt('total_records')}: #{total_records}")
  end

  it 'is expected to contain the territory' do
    expect(rendered).to have_content("#{i18nt('territory')}: Município de São Paulo")
  end

  it 'is expected to contain the total filtered records found' do
    expect(rendered).to have_content("#{i18nt('total_filtered_records')}: #{total_filtered_records}")
  end

  it 'is expected to contain the active filters' do
    expect(view).to have_received(:render).with(partial: 'shared/services/active_filters',
                                                locals: { active_filters: active_filters })
  end
end
