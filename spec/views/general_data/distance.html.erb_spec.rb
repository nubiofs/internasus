# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_distance.html.erb', type: :view do
  let(:distances) do
    {
      distances_buckets: {},
      specialties_means: {},
      facility_type_means_per_specialty: { one: {}, two: {} }
    }
  end

  before do
    allow(view).to receive(:render).and_call_original
    expect(view).to receive(:render).with(partial: 'general_data/distance_chart',
                                          collection: distances[:facility_type_means_per_specialty], as: :chart)
    assign(:distances, distances)
    render
  end

  it 'is expected to have the selector for each id of the list' do
    %w[distances-bucket-chart mean-distance-by-specialty-chart].each do |id|
      expect(rendered).to have_selector('div', id: id)
    end
  end
end
