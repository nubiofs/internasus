# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_inside_element'

RSpec.describe 'general_data/_historic_series.html.erb', type: :view do
  it_behaves_like 'chart inside element', { historic_series: {} }, 'div', ['historic-series-chart']
end
