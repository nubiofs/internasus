# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_inside_element'

RSpec.describe 'general_data/_health_facilities.html.erb', type: :view do
  stats = {
    administrations: {},
    beds: {},
    facility_types: {}
  }

  it_behaves_like 'chart inside element', stats, 'div', ['administration-pie-chart']
  it_behaves_like 'chart inside element', stats, 'div', ['facility-types-pie-chart']
  it_behaves_like 'chart inside element', stats, 'div', ['beds-bar-chart']
end
