# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_card.html.erb', type: :view do
  let(:kind) { 'distance' }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(partial: kind)
    allow(view).to receive(:render).with(partial: 'pdf_generator', locals: { kind: kind })

    render partial: 'general_data/card', locals: { kind: kind }
  end

  it 'is expected contain the card div' do
    expect(rendered).to have_selector('div', class: 'card')
  end

  it 'is expected contain the header div' do
    expect(rendered).to have_selector('div', id: "headingGeneralData#{kind}", class: 'card-header')
  end

  it 'is expected to render the kind partial' do
    expect(view).to have_received(:render).with(partial: kind)
  end

  it 'is expected to render the pdf generator partial' do
    expect(view).to have_received(:render).with(partial: 'pdf_generator', locals: { kind: kind })
  end

  it 'is expected to contain the body' do
    expect(rendered).to have_selector('div', id: "collapseGeneralData#{kind}", class: 'collapse')
    expect(rendered).to have_selector('div', class: 'card-body')
  end
end
