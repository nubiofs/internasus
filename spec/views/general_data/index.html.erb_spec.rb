# frozen_string_literal: true

require 'rails_helper'

SECTIONS = %w[characterization rankings historic_series specialties descriptive_statistics census_sector_rates distance
              health_facilities health_facilities_specialty_distances hospitalizations territory dynamic].freeze
MISSING_SECTIONS = %w[].freeze

RSpec.describe 'general_data/index.html.erb', type: :view do
  before do
    allow(view).to receive(:render).and_call_original

    SECTIONS.each do |section|
      allow(view).to receive(:render).with(partial: section)
    end

    render
  end

  it 'is expected to contain an accordion' do
    expect(rendered).to have_selector('div', id: 'general-data-accordion', class: 'accordion')
  end

  it 'is expected to render the sections' do
    SECTIONS.each do |section|
      expect(view).to have_received(:render).with(partial: section)
    end
  end

  MISSING_SECTIONS.each do |section|
    xit "is expected to render the #{section} section" do
      expect(view).to have_received(:render).with(partial: section)
    end
  end
end
