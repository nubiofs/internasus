# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_ranking_item.html.erb', type: :view do
  let(:ranking_item) { ['a', 7] }
  let(:ranking_item_counter) { 5 }

  before do
    render partial: 'general_data/ranking_item.html', locals: {
      ranking_item: ranking_item,
      ranking_item_counter: ranking_item_counter
    }
  end

  it 'is expected to have table values' do
    expect(rendered).to have_selector('th', text: ranking_item_counter + 1)
    expect(rendered).to have_selector('td', text: ranking_item.first)
    expect(rendered).to have_selector('td', text: ranking_item.second)
  end
end
