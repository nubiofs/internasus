# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_inside_element'

RSpec.describe 'general_data/_hospitalizations.html.erb', type: :view do
  it_behaves_like 'chart inside element',
                  {
                    genders: {},
                    ages_ordered_by_code: {},
                    hospitalization_days: {},
                    hospitalization_types: {},
                    races: {}
                  }, 'div',
                  %w[
                    hospitalizations-per-gender-pie-chart
                    hospitalizations-per-age-code-bar-chart
                    hospitalizations-per-days-bar-chart
                    hospitalizations-per-type-pie-chart
                    hospitalizations-per-race-pie-chart
                  ]
end
