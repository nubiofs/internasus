# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_descriptive_statistics_item.html.erb', type: :view do
  let(:descriptive_statistics_item) { ['a', 7] }

  before do
    render partial: 'general_data/descriptive_statistics_item.html', locals: {
      descriptive_statistics_item: descriptive_statistics_item
    }
  end

  it 'is expected to have table values' do
    expect(rendered).to have_selector('td', text: descriptive_statistics_item.second)
  end
end
