# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_pdf_generator.html.erb', type: :view do
  let(:kind) { 'rankings' }

  before do
    render partial: 'general_data/pdf_generator', locals: { kind: kind }
  end

  it 'is expected to have a form' do
    expect(rendered).to have_selector(
      :xpath, "//form[@action='#{print_general_data_path}'][@method='post']", id: "#{kind}_print_form"
    )
  end

  it 'is expected to have a hidden input to hold the map image' do
    expect(rendered).to have_selector(:xpath, '//input[@type="hidden"]', id: "#{kind}_image", visible: false)
  end

  it 'is expected to have a hidden input to section type' do
    expect(rendered).to have_selector(:xpath, '//input[@type="hidden"]', id: 'kind', visible: false)
  end
end
