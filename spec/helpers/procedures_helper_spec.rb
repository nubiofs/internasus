# frozen_string_literal: true

require 'rails_helper'

def before_options(helper, method, klass, return_value)
  allow(klass).to receive_message_chain(:distinct, :where, :not, :order, :pluck).and_return(return_value)

  helper.send(method, klass)
end

RSpec.describe ProceduresHelper do
  describe 'model_icd_options' do
    let(:klass) { double('class') }
    let(:model_codes_ids_and_descriptions) { [['A00', 314, 'Colera']] }

    before do
      before_options(helper, 'model_icd_options', klass, model_codes_ids_and_descriptions)
    end

    it 'is expected to find model records where the code is not null' do
      expect(klass.distinct.where).to have_received(:not).with(code: nil)
    end

    it 'is expected to order by code the results' do
      expect(klass.distinct.where.not).to have_received(:order).with(:code)
    end

    it 'is expected to pluck the code, id and description from records' do
      expect(klass.distinct.where.not.order).to have_received(:pluck).with(:code, :id, :description)
    end

    it 'is expected to concatenate the code and titleized description of the results' do
      expect(helper.model_icd_options(klass)[0][0]).to eq(model_codes_ids_and_descriptions[0][0] +
                                                          ' - ' +
                                                          model_codes_ids_and_descriptions[0][2])
    end
  end

  describe 'model_number_attribute_options' do
    let(:klass) { double('class') }
    let(:attribute) { double('attribute') }
    let(:model_attributes) { [['attribute value']] }

    before do
      allow(klass).to receive_message_chain(:distinct, :where, :not, :order, :pluck).and_return(model_attributes)

      helper.model_number_attribute_options(klass, attribute)
    end

    it 'is expected to find model records where the attribute is not null' do
      expect(klass.distinct.where).to have_received(:not).with(attribute => nil)
    end

    it 'is expected to order by attribute the results' do
      expect(klass.distinct.where.not).to have_received(:order).with(attribute)
    end

    it 'is expected to pluck the attribute from records' do
      expect(klass.distinct.where.not.order).to have_received(:pluck).with(attribute)
    end
  end

  [
    { method: 'max_hospitalization_days', attribute: :hospitalization_days },
    { method: 'max_hospitalization_rates', attribute: :hospitalization_rates },
    { method: 'max_uti_rates', attribute: :uti_rates },
    { method: 'max_ui_rates', attribute: :ui_rates },
    { method: 'max_service_value', attribute: :service_value },
    { method: 'max_distance', attribute: :distance }
  ].each do |test_case|
    describe test_case[:method] do
      it "is expected to return maximum among HospitalizationDatum #{test_case[:attribute]} values" do
        maximum = 42

        expect(HospitalizationDatum).to receive(:maximum).with(test_case[:attribute]).and_return(maximum)

        expect(helper.send(test_case[:method])).to eq(maximum)
      end
    end
  end
end
