# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HealthFacilityHelper do
  describe 'health_facilities_options' do
    let(:health_facilities_name_and_id) { [[201, 'hospital geral', 314]] }
    let(:health_facility_title) do
      "#{health_facilities_name_and_id[0][0]} - #{health_facilities_name_and_id[0][1].titleize}"
    end

    before do
      allow(HealthFacility).to receive_message_chain(:distinct, :where, :not, :pluck)
        .and_return(health_facilities_name_and_id)

      helper.health_facilities_options
    end

    it 'is expected to find HealthFacility records where the name is not null' do
      expect(HealthFacility.distinct.where).to have_received(:not).with(name: nil)
    end

    it 'is expected to pluck the name and id from records' do
      expect(HealthFacility.distinct.where.not).to have_received(:pluck).with(:cnes, :name, :id)
    end

    it 'is expected to titleize the results' do
      expect(helper.health_facilities_options[0][0]).to eq(health_facility_title)
    end
  end

  describe 'administration_options' do
    before do
      allow(HealthFacility).to receive_message_chain(:distinct, :where, :not, :pluck, :map)

      helper.administration_options
    end

    it 'is expected to find HealthFacility records where the administration is not null' do
      expect(HealthFacility.distinct.where).to have_received(:not).with(administration: nil)
    end

    it 'is expected to pluck the administration from records' do
      expect(HealthFacility.distinct.where.not).to have_received(:pluck).with(:administration)
    end

    it 'is expected to titleize the results' do
      expect(HealthFacility.distinct.where.not.pluck).to have_received(:map)
    end
  end

  describe 'max beds' do
    it 'is expected to return maximum among HealthFacility beds values' do
      maximum = 42

      expect(HealthFacility).to receive(:maximum).with(:beds).and_return(maximum)

      expect(helper.max_beds).to eq(maximum)
    end
  end

  describe 'distance_percentiles' do
    let(:health_facility) { FactoryBot.build(:health_facility) }
    let(:distances) { double('distances') }
    let(:first_perc) { 3.14 }
    let(:second_perc) { 4.2 }
    let(:third_perc) { 5.1 }

    before do
      allow(health_facility).to receive_message_chain(:hospitalization_data, :pluck, :extend).and_return(distances)
      allow(distances).to receive(:percentile).with(25).and_return(first_perc)
      allow(distances).to receive(:percentile).with(50).and_return(second_perc)
      allow(distances).to receive(:percentile).with(75).and_return(third_perc)

      @result = helper.distance_percentiles(health_facility)
    end

    it 'is expected to query for the HealthFacility distances' do
      expect(health_facility).to have_received(:hospitalization_data)
      expect(health_facility.hospitalization_data).to have_received(:pluck).with(:distance)
    end

    context 'when all percentiles return numbers' do
      it 'is expected to return the percentiles into a hash' do
        expect(@result).to be_a(Hash)
        expect(@result).to eq({ 25 => first_perc, 50 => second_perc, 75 => third_perc })

        expect(health_facility.hospitalization_data.pluck).to have_received(:extend).with(DescriptiveStatistics)

        expect(distances).to have_received(:percentile).with(25)
        expect(distances).to have_received(:percentile).with(50)
        expect(distances).to have_received(:percentile).with(75)
      end
    end

    context 'when there is a percentile that returns a nil value' do
      let(:second_perc) { nil }

      it 'is expected to not include this percentile in the hash' do
        expect(@result).to eq({ 25 => first_perc, 75 => third_perc })

        expect(health_facility.hospitalization_data.pluck).to have_received(:extend).with(DescriptiveStatistics)

        expect(distances).to have_received(:percentile).with(25)
        expect(distances).to have_received(:percentile).with(50)
        expect(distances).to have_received(:percentile).with(75)
      end
    end
  end
end
