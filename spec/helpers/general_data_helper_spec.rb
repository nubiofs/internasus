# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'creates a string with ratio' do
  context 'with a count on sector' do
    let(:count_number) { 4 }

    it 'is expected to construct the string with the provided value' do
      expect(helper).to have_received(:cell_rate_string).with(count_number, census_sector.total_population)
    end
  end

  context 'with no statistics on sector' do
    it 'is expected to construct the string with zero as value' do
      expect(helper).to have_received(:cell_rate_string).with(0, census_sector.total_population)
    end
  end
end

RSpec.describe GeneralDataHelper do
  let(:dummy_data) { { 'a' => 1, 'b' => 2, 'c' => 3 } }
  let(:ranking_data) do
    {
      health_facilities_ranking: dummy_data,
      administrative_sectors: dummy_data,
      age_codes: dummy_data,
      genders: dummy_data,
      icd_categories: dummy_data
    }
  end
  let(:descriptive_statistics_data) do
    {
      age_year: dummy_data,
      hospitalization_rates: dummy_data,
      uti_rates: dummy_data,
      ui_rates: dummy_data,
      service_values: dummy_data,
      hospitalization_days: dummy_data
    }
  end

  describe 'descriptive_statistics_translations' do
    it 'is expected to return a hash with translated titles as keys' do
      titles = [
        t('general_data.descriptive_statistics.age'),
        t('general_data.descriptive_statistics.hospitalization_rates'),
        t('general_data.descriptive_statistics.uti_rates'),
        t('general_data.descriptive_statistics.ui_rates'),
        t('general_data.descriptive_statistics.service_value'),
        t('general_data.descriptive_statistics.hospitalization_days')
      ]

      response = helper.descriptive_statistics_translations(descriptive_statistics_data)

      titles.each do |title|
        expect(response).to have_key(title)
      end
    end
  end

  describe 'rankings_translations' do
    it 'is expected to return a hash with translated titles as keys' do
      titles = [
        HealthFacility.model_name.human,
        AdministrativeSector.model_name.human,
        I18n.t('procedures.index.patient.age_code'),
        I18n.t('procedures.index.patient.gender'),
        IcdCategory.model_name.human
      ]

      response = helper.rankings_translations(ranking_data)

      titles.each do |title|
        expect(response).to have_key(title)
      end
    end
  end

  describe 'dynamic_section_titles' do
    it 'is expected to translate keys into titles' do
      ChartsConfigurationsConcern::DYNAMIC_CHARTS_INFO.each_key do |key|
        expect(helper).to receive(:t).with(key, scope: 'general_data.dynamic')
      end

      helper.dynamic_section_titles
    end
  end

  describe 'census_sector_rate_value_total' do
    let(:census_sector) { FactoryBot.build(:census_sector) }
    let(:counts) { double('counts') }
    let(:count_number) { nil }

    before do
      allow(helper).to receive(:cell_rate_string)
      expect(counts).to receive_message_chain(:[], :[]).and_return(count_number)

      helper.census_sector_rate_value_total census_sector, counts
    end

    it_behaves_like 'creates a string with ratio'
  end

  describe 'census_sector_rate_value' do
    let(:census_sector) { FactoryBot.build(:census_sector) }
    let(:counts) { double('counts') }
    let(:count_number) { nil }

    before do
      allow(helper).to receive(:cell_rate_string)
      expect(counts).to receive_message_chain(:[], :[]).and_return(count_number)

      helper.census_sector_rate_value census_sector, counts, 'index', 'total_population'
    end

    it_behaves_like 'creates a string with ratio'
  end

  describe 'cell_rate_string' do
    it 'is expected to format the ratio between two numbers' do
      expect(helper.cell_rate_string(25, 100)).to eq('25/100 (0.25%)')
    end

    it 'is expected not to calculate the percentage if the total is zero' do
      expect(helper.cell_rate_string(4, 0)).to eq('4/0 (-)')
    end
  end
end
