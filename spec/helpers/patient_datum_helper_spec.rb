# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PatientDatumHelper do
  describe 'patient_model_options' do
    let(:patient_datum) { FactoryBot.build(:patient_datum) }
    let(:attribute_translation) { 'translation' }
    let(:attribute) { :age_code }

    before do
      allow(PatientDatum).to receive_message_chain(
        :select,
        :distinct,
        :where,
        :not,
        :order
      ).and_return([patient_datum])

      expect(helper).to receive(:human_attribute_value).and_return(attribute_translation)

      @options = helper.patient_model_options(attribute)
    end

    it 'is expected to return the translation and value for the attribute' do
      expect(@options).to eq([[attribute_translation, patient_datum[attribute]]])
    end

    it 'is expected to query for distinct values for PatientDatum attributes' do
      expect(PatientDatum).to have_received(:select).with(attribute)
      expect(PatientDatum.select).to have_received(:distinct)
      expect(PatientDatum.select.distinct).to have_received(:where)
      expect(PatientDatum.select.distinct.where).to have_received(:not).with(attribute => nil)
      expect(PatientDatum.select.distinct.where.not).to have_received(:order).with(attribute)
    end
  end

  [
    { method: 'age_options', attribute: :age_code, result: ['0 a 4 anos', 'TP_0A4'] },
    { method: 'race_options', attribute: :race, result: %w[Branca 1] },
    { method: 'education_options', attribute: :educational_level, result: ['Analfabeto', 1] }
  ].each do |test_case|
    describe test_case[:method] do
      before do
        allow(helper).to receive(:patient_model_options).and_return(test_case[:result])
      end

      it 'is expected to fetch the PatientDatum options' do
        helper.send(test_case[:method])

        expect(helper).to have_received(:patient_model_options).with(test_case[:attribute])
      end

      it 'is expected to return the found options' do
        expect(helper.send(test_case[:method])).to eq(test_case[:result])
      end
    end
  end
end
