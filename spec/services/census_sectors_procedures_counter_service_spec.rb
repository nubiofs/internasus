# frozen_string_literal: true

require 'rails_helper'

require 'support/shared_examples/queries_group_and_count'

RSpec.shared_examples 'mass disableable for' do |method, disableable_filters|
  describe method.to_s do
    before do
      described_class::ALL_FILTERS.each { |type| subject.send(:filters)[type] = true }

      subject.send(method)
    end

    disableable_filters.each do |filter|
      it "is expected to disable #{filter}" do
        expect(subject.send(:filters)[filter]).to be false
      end
    end

    (described_class::ALL_FILTERS - disableable_filters).each do |filter|
      it "is expected to leave #{filter} unchanged" do
        expect(subject.send(:filters)[filter]).to be true
      end
    end
  end
end

RSpec.shared_examples 'mass disabling conflicting filter after enablement' do |filters, disabling_method|
  filters.each do |filter|
    context "when enabling the filter #{filter}" do
      before do
        allow(subject).to receive(disabling_method)

        subject.set_filter(filter, true)
      end

      it "is expected to call #{disabling_method}" do
        expect(subject).to have_received(disabling_method)
      end
    end

    context "when disabling the filter #{filter}" do
      before do
        allow(subject).to receive(disabling_method)

        subject.set_filter(filter, false)
      end

      it "is expected to not call #{disabling_method}" do
        expect(subject).to_not have_received(disabling_method)
      end
    end
  end
end

RSpec.describe CensusSectorsProceduresCounterService, type: :service do
  @patient_datum = FactoryBot.build(:patient_datum)
  @procedures_ids = @patient_datum.procedure_id

  let(:census_sector) { FactoryBot.build(:census_sector) }
  let(:census_sector_total_procedures) { 42 }
  let(:total_procedures_by_census_sector) do
    { census_sector => census_sector_total_procedures }
  end

  before do
    allow(PatientDatum).to receive_message_chain(:where, :group, :count).and_return(total_procedures_by_census_sector)
  end

  describe 'initialize' do
    before do
      @subject = described_class.new(@procedures_ids)
    end

    it_behaves_like 'queries, group and count', PatientDatum, { procedure_id: @procedures_ids }, :census_sector

    it 'is expected to initialize gender filters to true' do
      %i[women men].each do |type|
        expect(@subject.send(:filters)[type]).to be true
      end
    end

    it 'is expected to initialize race filters to false' do
      %i[black mulatto native white yellow].each do |type|
        expect(@subject.send(:filters)[type]).to be false
      end
    end
  end

  describe 'instance method' do
    subject { described_class.new(@procedures_ids) }

    it_behaves_like 'mass disableable for', :disable_all_race_filters, described_class::RACE_FILTERS
    it_behaves_like 'mass disableable for', :disable_all_gender_filters, described_class::GENDER_FILTERS

    describe 'set_filter' do
      it_behaves_like(
        'mass disabling conflicting filter after enablement',
        described_class::GENDER_FILTERS,
        :disable_all_race_filters
      )
      it_behaves_like(
        'mass disabling conflicting filter after enablement',
        described_class::RACE_FILTERS,
        :disable_all_gender_filters
      )

      it 'is expected to set the filter value' do
        %i[women men].each do |type|
          expect(subject.send(:filters)[type]).to be true

          subject.set_filter(type, false)

          expect(subject.send(:filters)[type]).to be false
        end

        %i[black mulatto native white yellow].each do |type|
          expect(subject.send(:filters)[type]).to be false

          subject.set_filter(type, true)

          expect(subject.send(:filters)[type]).to be true
        end
      end
    end

    describe 'census_sector_filtered_total_population' do
      context 'when all gender filters are enabled' do
        before do
          %i[women men].each { |type| subject.send(:filters)[type] = true }
          %i[black mulatto native white yellow].each { |type| subject.send(:filters)[type] = false }
        end

        it 'is expected to sum all' do
          expected_total = census_sector.women_population +
                           census_sector.men_population

          expect(subject.census_sector_filtered_total_population(census_sector)).to eq(expected_total)
        end
      end

      context 'when all race filters are enabled' do
        before do
          %i[women men].each { |type| subject.send(:filters)[type] = false }
          %i[black mulatto native white yellow].each { |type| subject.send(:filters)[type] = true }
        end

        it 'is expected to sum all' do
          expected_total = census_sector.black_population +
                           census_sector.mulatto_population +
                           census_sector.native_population +
                           census_sector.white_population +
                           census_sector.yellow_population

          expect(subject.census_sector_filtered_total_population(census_sector)).to eq(expected_total)
        end
      end
    end

    describe 'census_sector_counts' do
      let(:total_population) { 123 }

      before do
        allow(subject).to receive(:census_sector_filtered_total_population).and_return(total_population)

        @result = subject.census_sector_counts(census_sector)
      end

      it 'is expected to call the total population counter method' do
        expect(subject).to have_received(:census_sector_filtered_total_population).with(census_sector)
      end

      it 'is expected to return a hash with the poulation and procedures totals and the rate' do
        expected_rate = census_sector_total_procedures / total_population.to_f

        expect(@result).to eq(
          total_population: total_population,
          total_procedures: census_sector_total_procedures,
          total_rate: expected_rate
        )
      end
    end

    describe 'counts' do
      let(:census_sector_counts) { double('census_sector_counts') }

      before do
        allow(subject).to receive(:census_sector_counts).and_return(census_sector_counts)

        @result = subject.counts
      end

      it 'is expected to call the counter method for the census sector' do
        expect(subject).to have_received(:census_sector_counts).with(census_sector)
      end

      it 'is expected to return a Hash with the CensusSector as key and its counts as value' do
        expect(@result).to eq(census_sector => census_sector_counts)
      end
    end

    describe 'census_sector_counts_by' do
      let(:attribute) { :gender }
      let(:filtered_procedures) { double('filtered_procedures') }

      before do
        allow(subject).to receive(:filtered_procedures).and_return(filtered_procedures)
        allow(filtered_procedures).to receive_message_chain(:group, :count, :group_by, :transform_values)

        subject.census_sector_counts_by attribute
      end

      it 'is expected to return the count grouped by census_sector and gender' do
        expect(filtered_procedures).to have_received(:group).with(:census_sector_id, attribute)
        expect(filtered_procedures.group).to have_received(:count)
      end

      it 'is expected to mangle the data structure' do
        expect(filtered_procedures.group.count).to have_received(:group_by)
        expect(filtered_procedures.group.count.group_by).to have_received(:transform_values)
      end
    end

    describe 'census_sector_statistics' do
      let(:count_by_race) { { a: { c: 3 }, b: { c: 4 } } }
      let(:count_by_gender) { { a: { b: 3 }, b: {} } }
      let(:ids) { %i[a b] }

      before do
        allow(subject).to receive(:census_sector_counts_by).with(:race).and_return(count_by_race)
        allow(subject).to receive(:census_sector_counts_by).with(:gender).and_return(count_by_gender)
        allow(subject).to receive(:total_procedures_by_census_sector)

        @returned = subject.census_sector_statistics
      end

      it 'is is expected to get the total count' do
        expect(subject).to have_received(:total_procedures_by_census_sector)
      end

      it 'is is expected to get the counts by race and gender' do
        expect(subject).to have_received(:census_sector_counts_by).with(:race)
        expect(subject).to have_received(:census_sector_counts_by).with(:gender)
      end

      it 'it is expected to return a list of ids' do
        expect(@returned[:ids]).to eq(ids)
      end
    end
  end
end
