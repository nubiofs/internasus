# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'select keys from hash' do |method, wanted_keys|
  it 'is expected to return a hash with only the desired keys from the stats hash' do
    response_hash = Hash[wanted_keys.product([nil])]
    response_hash[:not_wanted_key] = nil
    expect(@subject).to receive(:stats).at_least(1).times.and_return response_hash

    response = @subject.send(method)

    wanted_keys.each do |key|
      expect(response).to have_key(key)
    end
    expect(response).to_not have_key(:not_wanted_key)
  end
end

RSpec.shared_examples 'order_and_count_data call' do |method, data, group, order|
  before do
    allow(data).to receive_message_chain(:where, :group, :order, :count)
    @subject.send(method)
  end

  it 'is expected to get the data about the procedures asked' do
    expect(data).to have_received(:where).with(procedure_id: procedures_ids)
  end

  it "is expected to group the data by #{group}" do
    expect(data.where).to have_received(:group).with(group)
  end

  it "is expected to order the data according to #{order}" do
    expect(data.where.group).to have_received(:order).with(order)
  end

  it 'is expected to call count to be able to order by count' do
    expect(data.where.group.order).to have_received(:count)
  end
end

RSpec.shared_examples 'join_filter_order_and_count_data call' do |method, data, table, group, order|
  before do
    allow(data).to receive_message_chain(:joins, :where, :group, :order, :count)
    @subject.send(method)
  end

  it "is expected to join the data table with #{table}" do
    expect(data).to have_received(:joins).with(table)
  end

  it 'is expected to get the data about the procedures asked' do
    expect(data.joins).to have_received(:where).with(procedure_id: procedures_ids)
  end

  it "is expected to group the data by #{group}" do
    expect(data.joins.where).to have_received(:group).with(group)
  end

  it "is expected to order the data according to #{order}" do
    expect(data.joins.where.group).to have_received(:order).with(order)
  end

  it 'is expected to call count to be able to order by count' do
    expect(data.joins.where.group.order).to have_received(:count)
  end
end

RSpec.describe GeneralDataService, type: :service do
  let(:data) { double 'data' }
  let(:group) { double 'group' }
  let(:order) { double 'order' }
  let(:table) { double 'table' }
  let(:filter) { double 'filter' }
  let(:health_facilities) { double 'health_facilities' }
  let(:procedures_ids) { [42, 314] }

  before do
    @subject = described_class.new(health_facilities, procedures_ids)
  end

  describe 'initialize' do
    it 'is expected to set health facilities and procedures variables' do
      expect(@subject.health_facilities).to eq(health_facilities)
      expect(@subject.procedures_ids).to eq(procedures_ids)
    end
  end

  describe 'stats' do
    it 'is expected to gather patient, hospitalization and health_facilities stats' do
      expect(@subject).to receive(:patient_stats).and_return({})
      expect(@subject).to receive(:hospitalization_stats).and_return({})
      expect(@subject).to receive(:health_facilities_stats).and_return({})
      expect(@subject).to receive(:translations_for)

      @subject.stats
    end
  end

  describe 'patient_stats' do
    before do
      %i[
        administrative_sector_count_ordered_by ages_ordered_by ages_ordered_by order_and_count subprefecture_count count
        administrative_sector_count_ordered_by technical_health_supervision_count regional_health_coordination_count
      ].each do |method|
        allow(@subject).to receive(method)
      end
    end

    it 'is expected to build a hash with the desired keys' do
      response = @subject.patient_stats

      %i[
        administrative_sectors age_year age_codes ages_ordered_by_code genders subprefectures educational_levels races
        administrative_sectors_by_name technical_health_supervisions regional_health_coordinations
      ].each do |key|
        expect(response).to have_key(key)
      end
    end

    it 'is expected to call all aggregation methods' do
      @subject.patient_stats

      expect(@subject).to have_received(:administrative_sector_count_ordered_by).with(count_all: :desc)
      expect(@subject).to have_received(:ages_ordered_by).with(count_all: :desc)
      expect(@subject).to have_received(:ages_ordered_by).with(:age_code)
      expect(@subject).to have_received(:order_and_count).with(PatientDatum, :gender, count_all: :desc)
      expect(@subject).to have_received(:subprefecture_count)
      expect(@subject).to have_received(:count).with(PatientDatum, :educational_level)
      expect(@subject).to have_received(:count).with(PatientDatum, :race)
      expect(@subject).to have_received(:administrative_sector_count_ordered_by).with({ name: :desc })
      expect(@subject).to have_received(:technical_health_supervision_count)
      expect(@subject).to have_received(:regional_health_coordination_count)
    end
  end

  describe 'hospitalization_stats' do
    before do
      %i[historic_series order_and_count count].each do |method|
        allow(@subject).to receive(method)
      end
      allow(@subject).to receive(:hospitalization_rates_data).and_return({})
      allow(@subject).to receive(:icd_categories_data).and_return({})
    end

    it 'is expected to add the below keys to the return hash' do
      response = @subject.hospitalization_stats

      %i[
        historic_series competences hospitalization_groups specialties hospitalization_types complexities
        hospitalization_days service_values distances
      ].each do |key|
        expect(response).to have_key(key)
      end
    end

    it 'is expected to perform the following method calls to gather data' do
      @subject.hospitalization_stats

      %i[competence hospitalization_group hospitalization_days service_value distance].each do |param|
        expect(@subject).to have_received(:order_and_count).with(HospitalizationDatum, param, param)
      end

      %i[historic_series hospitalization_rates_data icd_categories_data].each do |method|
        expect(@subject).to have_received(method)
      end

      %i[specialty complexity financing].each do |param|
        expect(@subject).to have_received(:count).with(HospitalizationDatum, param)
      end

      expect(@subject).to have_received(:order_and_count)
        .with(HospitalizationDatum, :hospitalization_type, count_all: :desc)
    end
  end

  describe 'health_facilities_stats' do
    it 'is expected to return a hash with health facilities stats' do
      expect(@subject).to receive(:health_facilities_procedures)
      expect(@subject).to receive(:health_facilities_ranking)
      expect(@subject).to receive(:health_facilities_specialty_distances)
      expect(@subject).to receive(:health_facility_types_count)
      expect(@subject).to receive(:health_facilities_beds)
      expect(@subject).to receive(:administration_count)

      response = @subject.health_facilities_stats

      expect(response).to have_key(:health_facilities)
      expect(response).to have_key(:health_facilities_ranking)
      expect(response).to have_key(:health_facilities_specialty_distances)
      expect(response).to have_key(:facility_types)
      expect(response).to have_key(:administrations)
    end
  end

  describe 'rankings' do
    it_behaves_like 'select keys from hash', :rankings, ChartsConfigurationsConcern::RANKING_KEYS
  end

  describe 'health_facilities_specialty_distances' do
    distances_absolute = [1, 2, 3, 4]
    distances_relative = [10.0, 20.0, 30.0, 40.0]
    final_data = { health_facility: [{ specialty: distances_absolute }, { specialty: distances_relative }] }
    data_with_options = { options: ChartsConfigurationsConcern::HEALTH_FACILITIES_SPECIALTY_DISTANCES_INFO,
                          data: final_data }

    before do
      expect(@subject).to receive(:health_facilities_specialty_distances_complete).and_return(final_data)
    end

    it 'is expected return data with options hash' do
      response = @subject.send(:health_facilities_specialty_distances)
      expect(response).to eql(data_with_options)
    end
  end

  describe 'dynamic' do
    it_behaves_like 'select keys from hash', :dynamic, ChartsConfigurationsConcern::DYNAMIC_CHARTS_INFO.keys
  end

  describe 'descriptive_statistics' do
    it_behaves_like 'select keys from hash',
                    :descriptive_statistics,
                    %i[age_year hospitalization_rates uti_rates ui_rates service_values hospitalization_days]
  end

  describe 'distances' do
    let(:distances_hash) { double 'distances_hash' }
    let(:translated_hash) { double 'translated_hash' }

    before do
      expect(@subject).to receive(:distances_hash).and_return(distances_hash)
      expect(@subject).to receive(:translations_for).with(distances_hash).and_return(translated_hash)
    end

    it 'is expected to return a translated hash from distanes hash' do
      response = @subject.send(:distances)
      expect(response).to eq(translated_hash)
    end
  end

  describe 'private method' do
    describe 'historic_series' do
      it_behaves_like 'order_and_count_data call',
                      'historic_series',
                      HospitalizationDatum,
                      "DATE_TRUNC('month', admission_date)",
                      "DATE_TRUNC('month', admission_date)"
    end

    describe 'administration_count' do
      it 'is expected to group the data by administration and count' do
        expect(@subject).to receive(:join_filter_order_and_count)
          .with(
            health_facilities, :hospitalization_data, :administration, :administration,
            hospitalization_data: { procedure_id: procedures_ids }
          )

        @subject.send(:administration_count)
      end
    end

    describe 'ages_ordered_by' do
      it 'is expected to query age codes ordering by a given column' do
        column = :age_code
        expect(@subject).to receive(:order_and_count).with(PatientDatum, :age_code, column)

        @subject.send(:ages_ordered_by, column)
      end
    end

    describe 'administrative_sector_count_ordered_by' do
      it 'is expected to query administrative sectors ordering by a given column' do
        column = :name
        expect(@subject).to receive(:join_filter_order_and_count)
          .with(PatientDatum, :administrative_sector, :name, column, procedure_id: procedures_ids)

        @subject.send(:administrative_sector_count_ordered_by, column)
      end
    end

    describe 'regional_health_coordination_count' do
      it_behaves_like 'join_filter_order_and_count_data call',
                      'regional_health_coordination_count',
                      PatientDatum,
                      :regional_health_coordination,
                      :name,
                      { name: :desc }
    end

    describe 'technical_health_supervision_count' do
      it_behaves_like 'join_filter_order_and_count_data call',
                      'technical_health_supervision_count',
                      PatientDatum,
                      :technical_health_supervision,
                      :name,
                      { name: :desc }
    end

    describe 'subprefecture_count' do
      it_behaves_like 'join_filter_order_and_count_data call',
                      'subprefecture_count',
                      PatientDatum,
                      :subprefecture,
                      :name,
                      :name
    end

    describe 'hospitalization_rates_data' do
      it 'is expected to include data rates values in a hash' do
        rates_keys = %i[uti_rates ui_rates hospitalization_rates]
        rates_keys.each do |key|
          expect(@subject).to receive(:order_and_count).with(HospitalizationDatum, key, key)
        end

        response = @subject.send(:hospitalization_rates_data)

        rates_keys.each do |key|
          expect(response).to have_key(key)
        end
      end
    end

    describe 'icd_categories_data' do
      it 'is expected to include icd categories data in a hash' do
        key_pair = [
          %i[icd_categories icd_category_id],
          %i[icd_secondary_categories secondary_icd1_id],
          %i[icd_secondary_2_categories secondary_icd2_id]
        ]
        key_pair.each do |_hash_key, column|
          expect(@subject).to receive(:order_and_count).with(HospitalizationDatum, column, count_all: :desc)
        end

        response = @subject.send(:icd_categories_data)

        key_pair.each do |hash_key, _column|
          expect(response).to have_key(hash_key)
        end
      end
    end

    describe 'count' do
      before do
        allow(data).to receive_message_chain(:where, :group, :count)
        @subject.send(:count, data, group)
      end

      it 'is expected to filter and count the data' do
        expect(data).to have_received(:where).with(procedure_id: procedures_ids)
        expect(data.where).to have_received(:group).with(group)
        expect(data.where.group).to have_received(:count)
      end
    end

    describe 'order_and_count' do
      before do
        allow(data).to receive_message_chain(:where, :group, :order, :count)
        @subject.send(:order_and_count, data, group, order)
      end

      it 'is expected to filter, order and count the data' do
        expect(data).to have_received(:where).with(procedure_id: procedures_ids)
        expect(data.where).to have_received(:group).with(group)
        expect(data.where.group).to have_received(:order).with(order)
        expect(data.where.group.order).to have_received(:count)
      end
    end

    describe 'join_filter_order_and_count' do
      before do
        allow(data).to receive_message_chain(:joins, :where, :group, :order, :count)
        @subject.send(:join_filter_order_and_count, data, table, group, order, filter)
      end

      it 'is expected to filter, order and count the data in respect to given table' do
        expect(data).to have_received(:joins).with(table)
        expect(data.joins).to have_received(:where).with(filter)
        expect(data.joins.where).to have_received(:group).with(group)
        expect(data.joins.where.group).to have_received(:order).with(order)
        expect(data.joins.where.group.order).to have_received(:count)
      end
    end

    {
      health_facilities_ranking: { count_all: :desc },
      health_facilities_procedures: { name: :desc }
    }.each do |method, order|
      describe 'health_facilities_procedures' do
        it 'is expected to group the data and order by name' do
          expect(@subject).to receive(:join_filter_order_and_count)
            .with(
              health_facilities,
              :hospitalization_data, :name, order,
              hospitalization_data: { procedure_id: procedures_ids }
            )

          @subject.send(method)
        end
      end
    end

    describe 'health_facilities_specialty_distances_complete' do
      distances_absolute = [1, 2, 3, 4]
      distances_relative = [10.0, 20.0, 30.0, 40.0]
      absolute_data = { health_facility: { specialty: distances_absolute } }
      final_data = { health_facility: [{ specialty: distances_absolute }, { specialty: distances_relative }] }

      before do
        expect(@subject).to receive(:health_facilities_specialty_distances_absolute).and_return(absolute_data)
      end

      it 'is expected to add the relative data' do
        response = @subject.send(:health_facilities_specialty_distances_complete)
        expect(response).to eql(final_data)
      end
    end

    describe 'health_facilities_specialty_distances_absolute' do
      distances_absolute = [1, 2, 3, 4]
      query_data = {
        [:health_facility, 0, 0] => 1,
        [:health_facility, 0, 1] => 2,
        [:health_facility, 0, 2] => 3,
        [:health_facility, 0, 3] => 4
      }
      absolute_data = { health_facility: { total: distances_absolute, 0 => distances_absolute } }

      before do
        expect(@subject).to receive(:health_facilities_distance_categories).and_return(query_data)
      end

      it 'is expected to structure query data' do
        response = @subject.send(:health_facilities_specialty_distances_absolute)
        expect(response).to eql(absolute_data)
      end
    end

    describe 'health_facilities_distance_categories' do
      it 'is expected to group the data into distance categories for each specialty for every health facility' do
        expect(@subject).to receive(:join_filter_order_and_count)
          .with(
            HospitalizationDatum, :health_facility,
            [
              :name, :specialty,
              'CASE WHEN distance <= 1 THEN 0'\
                   'WHEN distance <= 5 THEN 1'\
                   'WHEN distance <= 10 THEN 2 ELSE 3 END'
            ],
            :name, procedure_id: procedures_ids
          )
        @subject.send(:health_facilities_distance_categories)
      end
    end

    describe 'health_facility_types_count' do
      it 'is expected to group the data into facility type counts for every health facility' do
        expect(@subject).to receive(:join_filter_order_and_count)
          .with(
            health_facilities, :hospitalization_data,
            :facility_type, 'facility_type DESC',
            'hospitalization_data.procedure_id' => procedures_ids
          )
        @subject.send(:health_facility_types_count)
      end
    end

    describe 'health_facilities_beds' do
      before do
        allow(health_facilities).to receive_message_chain(:select, :joins, :where, :order, :distinct)
        @subject.send(:health_facilities_beds)
      end

      it 'is expected to group the data into facility type counts for every health facility' do
        expect(health_facilities).to have_received(:select).with(:name, :beds)
      end

      it 'is expected to group the data into facility type counts for every health facility' do
        expect(health_facilities.select).to have_received(:joins).with(:hospitalization_data)
      end

      it 'is expected to group the data into facility type counts for every health facility' do
        expect(health_facilities.select.joins).to have_received(:where)
          .with('hospitalization_data.procedure_id' => procedures_ids)
      end

      it 'is expected to order the data alphabetically' do
        expect(health_facilities.select.joins.where).to have_received(:order).with(name: :desc)
      end

      it 'is expected to group the data into facility type counts for every health facility' do
        expect(health_facilities.select.joins.where.order).to have_received(:distinct)
      end
    end
  end
end
