# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'search for a patient attribute translation' do |attribute|
  before do
    allow(subject).to receive(:patient_attribute_value).and_return(attribute.to_s)
    subject.send((attribute.to_s + '_translation').to_sym, data_hash[attribute])
  end

  it "is expected to get the translation for #{attribute}" do
    expect(subject).to(
      have_received(:patient_attribute_value)
      .exactly(data_hash[attribute].size)
      .times
    )
  end
end

RSpec.shared_examples 'translate with options' do |translation_method, attribute, translation_option, key, options|
  before do
    allow(I18n).to receive(translation_option).with(key, options)
    subject.send(translation_method, data_hash[attribute])
  end

  it 'is expected to correctly translate the keys' do
    expect(I18n).to have_received(translation_option).with(key, options)
  end
end

RSpec.shared_examples 'translate icd categories' do |translation_method, data, icd_class|
  it 'is expected to translate the categories according to the ICD class' do
    expect(subject).to receive(:icd_translations).with(data, icd_class)

    subject.send(translation_method, data)
  end
end

RSpec.describe TranslationsConcern do
  class TestClass
    include TranslationsConcern
  end

  subject { TestClass.new }

  let(:dummy_data) { { 'a' => 1, 'b' => 2, 'c' => 3 } }
  let(:data_hash) do
    {
      health_facilities: dummy_data,
      total_category: :total,
      specialty_category: 0,
      administrative_sectors: dummy_data,
      age_codes: dummy_data,
      genders: dummy_data,
      icd_categories: dummy_data,
      races: dummy_data,
      specialties: { 1 => {} },
      hospitalization_types: { 'some_type' => nil },
      historic_series: { 'some_date' => nil },
      complexities: { 1 => {} },
      financing_types: { 1 => {} },
      ranges_buckets: 0,
      educational_levels: dummy_data
    }
  end

  describe 'age_codes_translation' do
    it_behaves_like 'search for a patient attribute translation', :age_codes
  end

  describe 'genders_translation' do
    it_behaves_like 'search for a patient attribute translation', :genders
  end

  describe 'races_translation' do
    it_behaves_like 'search for a patient attribute translation', :races
  end

  describe 'educational_levels_translation' do
    it_behaves_like 'search for a patient attribute translation', :educational_levels
  end

  describe 'translations_for' do
    it 'is expected to translate when there is method definition and to skip when there is not' do
      %i[
        age_codes_translation genders_translation icd_categories_translation races_translation specialties_translation
        historic_series_translation hospitalization_types_translation complexities_translation
        financing_types_translation educational_levels_translation
      ].each do |translation_method|
        expect(subject).to receive(translation_method)
      end

      subject.translations_for(data_hash)
    end
  end

  describe 'icd_categories_translation' do
    it_behaves_like 'translate icd categories', :icd_categories_translation, {}, IcdCategory
  end

  describe 'icd_secondary_categories_translation' do
    it_behaves_like 'translate icd categories', :icd_secondary_categories_translation, {}, IcdSubcategory
  end

  describe 'specialties_translation' do
    it_behaves_like 'translate with options',
                    :specialties_translation, :specialties, :t, 1, scope: 'hospitalization_datum.specialty'
  end

  describe 'hospitalization_types_translation' do
    it_behaves_like 'translate with options',
                    :hospitalization_types_translation,
                    :hospitalization_types,
                    :t,
                    'hospitalization_type_some_type',
                    scope: 'hospitalization_datum'
  end

  describe 'historic_series_translation' do
    it_behaves_like 'translate with options',
                    :historic_series_translation,
                    :historic_series,
                    :l,
                    'some_date',
                    format: '%b %Y'
  end

  describe 'complexities_translation' do
    it_behaves_like 'translate with options',
                    :complexities_translation, :complexities, :t, 1, scope: 'hospitalization_datum.complexity'
  end

  describe 'financing_types_translation' do
    it_behaves_like 'translate with options',
                    :financing_types_translation, :financing_types, :t, 1, scope: 'hospitalization_datum.financing'
  end

  describe 'icd_translations' do
    before do
      allow(IcdCategory).to receive_message_chain(:where, :pluck, :join).and_return('a')
      subject.icd_translations(data_hash[:icd_categories], IcdCategory)
    end

    it 'is expected to search for Icd code and description' do
      n = data_hash[:icd_categories].size

      expect(IcdCategory).to have_received(:where).with(id: 'a')
      expect(IcdCategory.where).to have_received(:pluck).with(:code, :description).exactly(n).times
      expect(IcdCategory.where.pluck).to have_received(:join).with(' - ').exactly(n).times
    end
  end

  describe 'patient_attribute_value' do
    let(:attribute) { 'gender' }
    let(:value) { 'M' }

    before do
      allow(I18n).to receive(:t)
    end

    it 'is expected to get a translation for a patient attribute value' do
      subject.patient_attribute_value(attribute, value)
      expect(I18n).to have_received(:t).with("patient_datum.#{attribute}_#{value}")
    end
  end

  describe 'health_facilities_specialty_distances_translation' do
    specialties_before = { :total => [], 0 => [] }
    specialties_after = { total_translation: [], specialty_translation: [] }
    before_translation = { data: { health_facility: [specialties_before] }, options: {} }
    after_translation = { data: { health_facility: [specialties_after] }, options: {} }

    before do
      expect(subject).to receive(:translate_category_name).with(:total).and_return(:total_translation)
      expect(subject).to receive(:translate_category_name).with(0).and_return(:specialty_translation)
    end

    it 'is expected to structure query data' do
      response = subject.send(:health_facilities_specialty_distances_translation, before_translation)
      expect(response).to eql(after_translation)
    end
  end

  describe 'distances_buckets_translation' do
    before_translation = { 0 => 10, 1 => 20 }
    after_translation = { 'bucket0' => 10, 'bucket1' => 20 }

    before do
      expect(subject).to receive(:translate_range_bucket).with(0).and_return('bucket0')
      expect(subject).to receive(:translate_range_bucket).with(1).and_return('bucket1')
    end

    it 'is expected to translate the buckets of ranges' do
      response = subject.send(:distances_buckets_translation, before_translation)
      expect(response).to eql(after_translation)
    end
  end

  describe 'private method' do
    describe 'translate_category_name' do
      context 'when it is the total category' do
        it_behaves_like 'translate with options',
                        :translate_category_name,
                        :total_category,
                        :t,
                        :total,
                        scope: 'general_data.health_facilities_specialty_distances'
      end

      context 'when it is a specialty category' do
        it_behaves_like 'translate with options',
                        :translate_category_name,
                        :specialty_category,
                        :t,
                        0,
                        scope: 'hospitalization_datum.specialty'
      end
    end

    describe 'translate_range_bucket' do
      it_behaves_like 'translate with options',
                      :translate_range_bucket,
                      :ranges_buckets, :t, 'group_0',
                      scope: 'general_data.distance'
    end
  end
end
