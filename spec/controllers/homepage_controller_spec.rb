# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomepageController, type: :controller do
  describe 'GET #index' do
    before do
      allow(HospitalizationDatum).to receive(:count)
      allow(HealthFacility).to receive(:count)
      allow(HospitalizationDatum).to receive_message_chain(:select, :distinct, :count)
      allow(HospitalizationDatum).to receive(:average)

      get :index
    end

    it { is_expected.to respond_with(:success) }

    it { is_expected.to_not use_before_action(:authenticate_user!) }

    it 'is expected to count hospitalizations' do
      expect(HospitalizationDatum).to have_received(:count)
    end

    it 'is expected to count health facilities' do
      expect(HealthFacility).to have_received(:count)
    end

    it 'is expected to count specialties' do
      expect(HospitalizationDatum).to have_received(:select).with(:specialty)
      expect(HospitalizationDatum.select).to have_received(:distinct)
      expect(HospitalizationDatum.select.distinct).to have_received(:count)
    end

    it 'is expected to compute the distance average' do
      expect(HospitalizationDatum).to have_received(:average).with(:distance)
    end
  end
end
