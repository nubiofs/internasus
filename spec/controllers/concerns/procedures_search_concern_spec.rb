# frozen_string_literal: true

require 'rails_helper'

class ProceduresSearchConcernTest
  include ProceduresSearchConcern

  attr_accessor :params
end

RSpec.describe ProceduresSearchConcern do
  subject { ProceduresSearchConcernTest.new }

  describe 'set_search_results' do
    let(:health_facilities) { double('health_facilities') }
    let(:patients_data) { double('patients_data') }
    let(:search) { double('search') }
    let(:procedures_ids) { double('procedures_ids') }
    let(:dataset_proportion) { double('dataset_proportion') }

    before do
      subject.params = {}

      allow(ProceduresSearchService).to receive(:search)
        .and_return([search, health_facilities, patients_data, procedures_ids, dataset_proportion])

      allow(ProceduresSearchService).to receive(:active_filters)

      subject.set_search_results
    end

    it 'is expected to invoke the ProceduresSearchService' do
      expect(ProceduresSearchService).to have_received(:search)
    end

    it 'is expected to get the active filters from ProceduresSearchService' do
      expect(ProceduresSearchService).to have_received(:active_filters)
    end
  end
end
