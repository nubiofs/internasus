# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'successful search action' do |method, action|
  before do
    expect(subject).to receive(:set_administrative_entities)
    expect(subject).to receive(:set_search_results)

    send(method, action)
  end

  it { is_expected.to respond_with(:success) }

  it { is_expected.to use_before_action(:set_administrative_entities) }

  it { is_expected.to use_before_action(:set_search_results) }

  it { is_expected.to use_before_action(:save_and_load_filters) }
end

RSpec.describe ProceduresController, type: :controller do
  describe 'action' do
    before do
      expect(subject).to receive(:authenticate_user!)
    end

    describe 'GET #index' do
      it_behaves_like 'successful search action', :get, :index
    end

    describe 'POST #search' do
      let(:census_sectors_procedures_counter_service) do
        double('census_sectors_procedures_counter_service')
      end
      let(:params) do
        { 'csw_heat_map' => {
          'women' => 'true',
          'men' => 'true',
          'black' => 'false',
          'mulatto' => 'false',
          'native' => 'false',
          'white' => 'false',
          'yellow' => 'false'
        } }
      end

      before do
        allow(CensusSectorsProceduresCounterService).to receive(:new)
          .and_return(census_sectors_procedures_counter_service)
        allow(census_sectors_procedures_counter_service).to receive(:counts)
        allow(census_sectors_procedures_counter_service).to receive(:set_filter)

        expect(subject).to receive(:set_administrative_entities)
        expect(subject).to receive(:set_search_results)

        post :search, params: params
      end

      it { is_expected.to respond_with(:success) }

      it { is_expected.to use_before_action(:set_administrative_entities) }

      it { is_expected.to use_before_action(:set_search_results) }

      it { is_expected.to use_before_action(:save_and_load_filters) }

      it 'is expected to count the procedures for each census sector, its total population and rate' do
        expect(CensusSectorsProceduresCounterService).to have_received(:new)
        expect(census_sectors_procedures_counter_service).to have_received(:counts)
      end

      it 'is expected to set filters for CensusSectorsProceduresCounterService' do
        CensusSectorsProceduresCounterService::ALL_FILTERS.each do |filter|
          expect(census_sectors_procedures_counter_service).to have_received(:set_filter)
            .with(
              filter,
              params['csw_heat_map'][filter.to_s] == 'true'
            )
        end
      end
    end

    describe 'GET #show' do
      let(:id) { '67a2ebd6-9079-48b5-b709-a98616b40904' }

      before do
        allow(PatientDatum).to receive_message_chain(:includes, :find_by!)
        allow(HospitalizationDatum).to receive_message_chain(:includes, :find_by!)

        get :show, params: { id: id }
      end

      it { is_expected.to respond_with(:success) }

      it 'is expected to find the PatientDatum' do
        expect(PatientDatum).to have_received(:includes).with(
          :administrative_sector, :subprefecture, :technical_health_supervision,
          :regional_health_coordination, :census_sector
        )
        expect(PatientDatum.includes).to have_received(:find_by!).with(procedure_id: id)
      end

      it 'is expected to find the HospitalizationDatum' do
        expect(HospitalizationDatum).to have_received(:includes).with(:icd_category)
        expect(HospitalizationDatum.includes).to have_received(:find_by!).with(procedure_id: id)
      end
    end

    describe 'GET #export' do
      let(:procedures_ids) { double('procedures_ids') }

      before do
        subject.instance_variable_set(:@procedures_ids, procedures_ids)

        allow(PatientDatum).to receive_message_chain(:includes, :where)
        expect(subject).to receive(:set_search_results)

        get :export, format: :csv
      end

      it { is_expected.to respond_with(:success) }

      it { is_expected.to use_before_action(:save_and_load_filters) }
      it { is_expected.to use_before_action(:set_search_results) }

      it 'is expected to query for patients data eager loading associations' do
        expect(PatientDatum).to have_received(:includes).with(
          { hospitalization_datum: %i[health_facility icd_subcategory secondary_icd1 secondary_icd2] },
          :administrative_sector,
          :subprefecture,
          :technical_health_supervision,
          :regional_health_coordination
        )
        expect(PatientDatum.includes).to have_received(:where).with(procedure_id: procedures_ids)
      end
    end

    describe 'POST #print' do
      it_behaves_like 'successful search action', :post, :print
    end
  end

  describe 'private method' do
    describe 'set_administrative_entities' do
      before do
        allow(City).to receive(:all)
        allow(CensusSector).to receive(:all)
        allow(RegionalHealthCoordination).to receive(:all)
        allow(TechnicalHealthSupervision).to receive(:all)
        allow(Subprefecture).to receive(:all)
        allow(AdministrativeSector).to receive(:all)
        allow(Ubs).to receive(:all)
        allow(FamilyHealthStrategy).to receive(:all)

        subject.send(:set_administrative_entities)
      end

      it 'is expected to list all Cities' do
        expect(City).to have_received(:all)
      end

      it 'is expected to list all CensusSector' do
        expect(CensusSector).to have_received(:all)
      end

      it 'is expected to list all RegionalHealthCoordination' do
        expect(RegionalHealthCoordination).to have_received(:all)
      end

      it 'is expected to list all TechnicalHealthSupervision' do
        expect(TechnicalHealthSupervision).to have_received(:all)
      end

      it 'is expected to list all Subprefecture' do
        expect(Subprefecture).to have_received(:all)
      end

      it 'is expected to list all AdministrativeSectors' do
        expect(AdministrativeSector).to have_received(:all)
      end

      it 'is expected to list all Ubs\'s' do
        expect(AdministrativeSector).to have_received(:all)
      end

      it 'is expected to list all FamilyHealthStrategy\'s' do
        expect(AdministrativeSector).to have_received(:all)
      end
    end
  end
end
