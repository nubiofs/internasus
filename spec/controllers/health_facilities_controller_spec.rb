# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HealthFacilitiesController, type: :controller do
  describe 'action' do
    before do
      expect(subject).to receive(:authenticate_user!)
    end

    describe 'GET #show' do
      let(:id) { '1081' }

      before do
        allow(HealthFacility).to receive_message_chain(:find)

        get :show, params: { id: id }
      end

      it { is_expected.to respond_with(:success) }

      it 'is expected to find the HealthFacility' do
        expect(HealthFacility).to have_received(:find).with(id)
      end
    end
  end
end
