# frozen_string_literal: true

require 'rails_helper'

class ApplicationControllerTest < ApplicationController
  def index; end
end

RSpec.describe ApplicationControllerTest, type: :controller do
  describe 'before_action' do
    before do
      subject.index
    end

    it { is_expected.to use_before_action(:authenticate_user!) }
  end
end
