import { LimitsFeatures } from '../../../app/javascript/packs/procedures_map/LimitsFeatures';

describe('LimitsFeatures', () => {
  let subject: LimitsFeatures;

  beforeEach(() => {
    subject = new LimitsFeatures();
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });
  });

  describe('addFeature', () => {
    const featureCollectionIdentifier = 'collection';
    const oneName = 'first feature';
    const anotherName = 'second feature';
    const oneGeometry = { 'type': 'Polygon', 'coordinates': [[[-46.52,-23.58],[-46.52,-23.58],[-46.52,-23.58]]] };
    const anotherGeometry = { 'type': 'Polygon', 'coordinates': [[[-46.25,-23.85],[-46.25,-23.85],[-46.25,-23.85]]] };

    describe('when this is the first feature of the collection', () => {
      beforeEach(() => {
        subject.addFeature(featureCollectionIdentifier, oneName, oneGeometry);
      });

      it('is expected to add an entry to the features hash', () => {
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier]).toBeDefined();
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][0].type).toEqual('Feature');
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][0].properties.name).toEqual(oneName);
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][0].geometry).toEqual(oneGeometry);
      });
    });

    describe('when there are two features for the collection', () => {
      beforeEach(() => {
        subject.addFeature(featureCollectionIdentifier, oneName, oneGeometry);
        subject.addFeature(featureCollectionIdentifier, anotherName, anotherGeometry);
      });

      it('is expected to add an entry to the features hash', () => {
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier]).toBeDefined();
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][0].type).toEqual('Feature');
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][0].properties.name).toEqual(oneName);
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][0].geometry).toEqual(oneGeometry);
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][1].type).toEqual('Feature');
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][1].properties.name).toEqual(anotherName);
        // @ts-ignore: TS2341
        expect(subject.features[featureCollectionIdentifier][1].geometry).toEqual(anotherGeometry);
      });
    });
  });

  describe('getFeatureCollection', () => {
    const identifier = 'collection';

    describe('with no features for the given identifier', () => {
      it('is expected to return undefined', () => {
        expect(subject.getFeatureCollection(identifier)).toBeUndefined();
      });
    });

    describe('with features for the given identifier', () => {
      const features = jasmine.createSpy('features');

      beforeEach(() => {
        // @ts-ignore: TS2341
        subject.features[identifier] = features;
      });

      it('is expected to return these features for the given identifier', () => {
        expect(subject.getFeatureCollection(identifier)).toEqual({
          'type': 'FeatureCollection',
          features,
        });
      });
    });
  });
});
