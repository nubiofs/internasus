import * as jQuery from 'jquery';

import { ProceduresMap } from '../../../app/javascript/packs/procedures_map/ProceduresMap';

describe('ProceduresMap', () => {
  const divId = 'map';
  const map = jasmine.createSpyObj('map', ['invalidateSize', 'addLayer', 'hasLayer', 'removeLayer', 'getZoom', 'on']);
  const tileLayer = jasmine.createSpyObj('tileLayer', ['addTo']);
  const markerClusterGroup = jasmine.createSpyObj('markerClusterGroup', ['addLayer']);
  const hfIcon = jasmine.createSpy('Icon');
  const marker = jasmine.createSpyObj('Marker', ['addTo', 'bindPopup']);
  const heatmapLayer = jasmine.createSpyObj('heatmapLayer', ['addLatLng', 'setOptions']);
  const geoJSONLayer = jasmine.createSpy('geoJSONLayer');
  const layerGroup = jasmine.createSpy('layerGroup');
  const circle = jasmine.createSpyObj('Circle', ['addTo', 'bindTooltip']);
  const leafletControl = jasmine.createSpyObj('control', {'minimap': tileLayer})
  const Leaflet = jasmine.createSpyObj('L', {
    'map': map,
    'tileLayer': tileLayer,
    'markerClusterGroup': markerClusterGroup,
    'icon': hfIcon,
    'marker': marker,
    'heatLayer': heatmapLayer,
    'geoJSON': geoJSONLayer,
    'layerGroup': layerGroup,
    'circle': circle,
  });
  Leaflet.control = leafletControl;
  const datasetProportion = 0.314;

  let subject: ProceduresMap;

  beforeEach(() => {
    heatmapLayer.setOptions.calls.reset();
    subject = new ProceduresMap(divId, Leaflet, jQuery, datasetProportion);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });

    it('is expected to create a Leaflet map', () => {
      expect(Leaflet.map).toHaveBeenCalledWith(
        divId,
        {
          center: [-23.557296, -46.669211],
          zoom: 11,
          maxZoom: 18
        }
      );
    });

    it('is expected to create the tileLayer', () => {
      expect(Leaflet.tileLayer).toHaveBeenCalledWith(
        'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
        { attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'}
      );
    });

    it('is expected to add the tileLayer to the map', () => {
      expect(tileLayer.addTo).toHaveBeenCalledWith(map);
    });

    it('is expected to create the minimap', () => {
      expect(leafletControl.minimap).toHaveBeenCalledWith(tileLayer, { toggleDisplay: true });
    });

    it('is expected to set the patients_data_cluster with a markerClusterGroup', () => {
      expect(Leaflet.markerClusterGroup).toHaveBeenCalled();
    });

    it('is expected to create a heatmap layer', () => {
      expect(Leaflet.heatLayer).toHaveBeenCalledWith([]);
    });

    it('is expected to set the default gradient for the heatmap', () => {
      expect(heatmapLayer.setOptions).toHaveBeenCalledWith({
        gradient: subject.heatmapGradients.default
      })
    });

    it('is expected to set the default gradient for the census sector weighted heatmap', () => {
      expect(heatmapLayer.setOptions).toHaveBeenCalledTimes(2);
    });

    it('is expected to create layer groups for health facilities and its circles', () => {
      expect(Leaflet.layerGroup).toHaveBeenCalledWith([]);
    });

    it('is expected to compute the heatmap intensity', () => {
      // @ts-ignore: TS2341
      expect(subject.heatmapIntensity).toEqual(1.0 + 9.0*(1.0 - datasetProportion));
    });

    it('is expected to add a zoom event on the map', () => {
      expect(map.on).toHaveBeenCalled();
    });
  });

  describe('addHealthFacility', () => {
    const imageUrl = 'img.png';
    const latitude = 1.0;
    const longitude = 2.0;
    const popupUrl = 'localhost';
    const errorMessage = 'Error';

    beforeEach(() => {
      subject.addHealthFacility(imageUrl, latitude, longitude, popupUrl, errorMessage);
    });

    it('is expected to create a custom icon', () => {
      expect(Leaflet.icon).toHaveBeenCalledWith({
        iconUrl: imageUrl,
        iconAnchor: [25, 0],
        className: 'health_facility_icon'
      });
    });

    it('is expected to create a marker', () => {
      expect(Leaflet.marker).toHaveBeenCalledWith(
        [latitude, longitude],
        { 'icon': hfIcon }
      );
    });

    it('is expected to add the marker to the map', () => {
      expect(marker.addTo).toHaveBeenCalledWith(layerGroup);
    });

    it('is expected to bind a popup to the marker', () => {
      expect(marker.bindPopup).toHaveBeenCalled();
    });
  });

  describe('setPopupContent', () => {
    const element = jasmine.createSpyObj('element', ['html']);
    const popup = jasmine.createSpyObj('popup', ['update']);
    const content = 'Very interesting';

    beforeEach(() => {
      subject.setPopupContent(element, popup, content);
    });

    it('is expected to set the element html', () => {
      expect(element.html).toHaveBeenCalledWith(content);
    });

    it('is expected to update the popup', () => {
      expect(popup.update).toHaveBeenCalled();
    });
  });

  describe('getPopupContent', () => {
    const url = 'localhost';
    const popup = jasmine.createSpyObj('popup', ['update']);
    const errorMessage = 'Danger!';

    const elementObj = jasmine.createSpyObj('element', ['css'])
    const element = jasmine.createSpy('element');
    const doneObj = jasmine.createSpyObj('doneObj', ['fail']);
    const ajax = jasmine.createSpyObj('ajax', { 'done': doneObj });

    let result: any;

    beforeEach(() => {
      elementObj[0] = element;
      // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'init').and.returnValue(elementObj);
      // I'm aware that this does not matches the jqXHR interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery, 'ajax').and.returnValue(ajax);

      result = subject.getPopupContent(url, popup, errorMessage);
    });

    it('is expected to set the width to 300px', () => {
      expect(elementObj.css).toHaveBeenCalledWith('width', '300px');
    });

    it('is expected to perform a Ajax request', () => {
      expect(jQuery.ajax).toHaveBeenCalledWith({
        url,
        dataType: 'html',
        data: ''
      });

      expect(ajax.done).toHaveBeenCalled();

      expect(doneObj.fail).toHaveBeenCalled();
    });

    it('is expected to return the element', () => {
      expect(result).toEqual(element);
    });
  });

  describe('createPatientDataMarker', () => {
    const latitude = 2.0;
    const longitude = 3.0;
    const popupUrl = '127.0.0.1';
    const errorMessage = 'Danger!';

    beforeEach(() => {
      subject.createPatientDataMarker(latitude, longitude, popupUrl, errorMessage);
    });

    it('is expected to create a Leaflet marker', () => {
      expect(Leaflet.marker).toHaveBeenCalledWith([latitude, longitude]);
    });

    it('is expected to bind a popup to the marker', () => {
      expect(marker.bindPopup).toHaveBeenCalled();
    });

    it('is expected to add the latlong to the heatmap layer', () => {
      // @ts-ignore: TS2341
      expect(heatmapLayer.addLatLng).toHaveBeenCalledWith([latitude, longitude, subject.heatmapIntensity]);
    });
  });

  describe('invalidateSize', () => {
    beforeEach(() => {
      subject.invalidateSize();
    });

    it('is expected to forward the call to the Leaflet map', () => {
      expect(map.invalidateSize).toHaveBeenCalled();
    });
  });

  describe('generateClusterLayer', () => {
    describe('dummy', () => {
      const markers = [1, 2, 3];
      const radius = 40;

      beforeEach(() => {
        map.hasLayer.and.returnValue(false);
        Leaflet.markerClusterGroup.and.returnValue(markerClusterGroup);
        markerClusterGroup.addLayer.calls.reset();
      });

      it('is expected to set a radius for the new cluster', () => {
        subject.generateClusterLayer(markers, radius);
        expect(Leaflet.markerClusterGroup).toHaveBeenCalledWith({maxClusterRadius: radius});
      });

      it('is expected to populate the cluster with markers', () => {
        subject.generateClusterLayer(markers, radius);
        expect(markerClusterGroup.addLayer).toHaveBeenCalledTimes(markers.length);
      });

      it('is expected to return a cluster layer', () => {
        const result = subject.generateClusterLayer(markers, radius);
        expect(result).toEqual(markerClusterGroup);
      });
    });
  });

  describe('toggleCluster', () => {
    describe('when the cluster is not a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);

        subject.toggleCluster();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(markerClusterGroup);
        expect(map.addLayer).toHaveBeenCalledWith(markerClusterGroup);
      });
    });

    describe('when the cluster already is a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);

        subject.toggleCluster();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(markerClusterGroup);
        expect(map.removeLayer).toHaveBeenCalledWith(markerClusterGroup);
      });
    });
  });

  describe('kilometresToPixels', () => {
    beforeEach(() => {
      map.getZoom.and.returnValue(11);
    });

    it('is expected to depend on the map zoom level', () => {
      subject.kilometresToPixels(1)
      expect(map.getZoom).toHaveBeenCalled();
    });

    it('is expected to return numbers', () => {
      expect(subject.kilometresToPixels(0)).toEqual(0);
      expect(subject.kilometresToPixels(10)).toEqual(143);
    });
  });

  describe('applyPatientCluster', () => {
    const defaultRadius = 80;
    const radius = 40;

    beforeEach(() => {
      spyOn(subject, 'generateClusterLayer').and.returnValue(markerClusterGroup);
      spyOn(subject, 'toggleCluster');
    });

    it('is expected to generate the data cluster', () => {
      subject.applyPatientCluster(radius);
      expect(subject.generateClusterLayer).toHaveBeenCalledWith([], radius);
    });

    it('is expected to have a default value for the radius', () => {
      subject.applyPatientCluster();
      expect(subject.generateClusterLayer).toHaveBeenCalledWith([], defaultRadius);
    });

    describe('when the cluster is visible', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);
        subject.applyPatientCluster();
      });

      it('is expected to toggle the cluster on and off', () => {
        expect(subject.toggleCluster).toHaveBeenCalledTimes(2);
      });
    });

    describe('when the cluster is not visible', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);
        subject.applyPatientCluster();
      });

      it('is expected to not not call toggleCluster', () => {
        expect(subject.toggleCluster).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('clusterRadiusEvent', () => {
    const input = { value: 10 };
    const finalRadius = 20;

    beforeEach(() => {
      spyOn(subject, 'kilometresToPixels').and.returnValue(finalRadius);
      spyOn(subject, 'applyPatientCluster');
      subject.clusterRadiusEvent(input);
    });

    it('is expected to convert the input value to pixels', () => {
      expect(subject.kilometresToPixels).toHaveBeenCalledWith(input.value);
    });

    it('is expected to apply the new value to the cluster', () => {
      expect(subject.applyPatientCluster).toHaveBeenCalledWith(finalRadius);
    });
  });

  describe('toggleHeatmap', () => {
    describe('when the heatmap is not a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);

        subject.toggleHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.addLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });

    describe('when the cluster already is a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);

        subject.toggleHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.removeLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });
  });

  describe('heatmapRadiusEvent', () => {
    const value = '10';
    const slider = { value };

    beforeEach(() => {
      spyOn(subject, 'kilometresToPixels').and.returnValue(Number(value));
    });

    it('is expected to set the radius to the slider value', () => {
      subject.heatmapRadiusEvent(slider);
      expect(heatmapLayer.setOptions).toHaveBeenCalledWith({
        radius: Number(value)
      });
    });

    it('is expected to set the radius for the census sector weighted heatmap', () => {
      expect(heatmapLayer.setOptions).toHaveBeenCalledTimes(2);
    });
  });

  describe('toggleHeatmapHighContrast', () => {
    it('is expected to set the heatmap layer gradient', () => {
      const gradients = subject.heatmapGradients;
      heatmapLayer.options = { gradient: gradients.default }

      // Turns on
      subject.toggleHeatmapHighContrast();
      expect(heatmapLayer.setOptions).toHaveBeenCalledWith({
        gradient: gradients.highContrast
      });

      // Turns off
      subject.toggleHeatmapHighContrast();
      expect(heatmapLayer.setOptions).toHaveBeenCalledWith({
        gradient: gradients.default
      });

      expect(heatmapLayer.setOptions).toHaveBeenCalledTimes(6);
    });
  });

  describe('heatmapOpacityEvent', () => {
    const slider = { value: "10" };
    const element = jasmine.createSpyObj('element', ['css']);

    beforeEach(() => {
      // I'm aware that this does not matches the jqXHR interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      subject.heatmapOpacityEvent(slider);
    });

    it('is expected to set the minOpacity to the slider value', () => {
      expect(element.css).toHaveBeenCalledWith('opacity', slider.value);
    });
  });

  describe('buildLimitsLayer', () => {
    const geoJSON = jasmine.createSpy('geoJSON');
    const identifier = 'identifier';
    const style = {
        'color': '#444444',
        'opacity': 0.6,
        'stroke': true,
        'fill': true,
        'fillOpacity': 0.05,
    };

    beforeEach(() => {
      subject.buildLimitsLayer(identifier, geoJSON);
    });

    it('is expected to build it using Leaflet', () => {
      expect(Leaflet.geoJSON).toHaveBeenCalledWith(geoJSON, { style });
    });
  });

  describe('toggleLimits', () => {
    const identifier = 'identifier';

    describe('when there is no layer for the identifier', () => {
      beforeEach(() => {
        // @ts-ignore: TS2341
        subject.limitsLayers = {};

        map.addLayer.calls.reset();
        map.removeLayer.calls.reset();

        subject.toggleLimits(identifier);
      });

      it('is expected to not try to add anything', () => {
        expect(map.addLayer).not.toHaveBeenCalled();
      });

      it('is expected to not try to remove anything', () => {
        expect(map.removeLayer).not.toHaveBeenCalled();
      });
    });

    describe('when there is a layer', () => {
      const limitsLayer = jasmine.createSpy('limitsLayer');

      beforeEach(() => {
        // @ts-ignore: TS2341
        subject.limitsLayers[identifier] = limitsLayer;
      });

      describe('and the map does not have the layer', () => {
        beforeEach(() => {
          map.hasLayer.and.returnValue(false);

          subject.toggleLimits(identifier);
        });

        it('is expected to add the layer', () => {
          expect(map.hasLayer).toHaveBeenCalledWith(limitsLayer);
          expect(map.addLayer).toHaveBeenCalledWith(limitsLayer);
        });
      });

      describe('and the map already has the layer', () => {
        beforeEach(() => {
          map.hasLayer.and.returnValue(true);

          subject.toggleLimits(identifier);
        });

        it('is expected to remove the layer', () => {
          expect(map.hasLayer).toHaveBeenCalledWith(limitsLayer);
          expect(map.removeLayer).toHaveBeenCalledWith(limitsLayer);
        });
      });
    });
  });

  describe('createCSWHMarker', () => {
    const latitude = 2.0;
    const longitude = 3.0;
    const rate = 0.42;

    beforeEach(() => {
      subject.createCSWHMarker(latitude, longitude, rate);
    });

    it('is expected to add the latlong to the heatmap layer', () => {
      // @ts-ignore: TS2341
      expect(heatmapLayer.addLatLng).toHaveBeenCalledWith([latitude, longitude, subject.heatmapIntensity*rate]);
    });
  });

  describe('toggleCSWHeatmap', () => {
    describe('when the heatmap is not a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);

        subject.toggleCSWHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.addLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });

    describe('when the cluster already is a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);

        subject.toggleCSWHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.removeLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });
  });

  describe('toggleHealthFacilities', () => {
    describe('when the health facilities are not a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);

        subject.toggleHealthFacilities();
      });

      it('is expected to add the health facilities group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.addLayer).toHaveBeenCalledWith(layerGroup);
      });
    });

    describe('when the health facilities already are a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);

        subject.toggleHealthFacilities();
      });

      it('is expected to add the health facilities group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.removeLayer).toHaveBeenCalledWith(layerGroup);
      });
    });
  });

  describe('addHealthFacilityPercentile', () => {
    const latitude = 12.0;
    const longitude = 13.0;
    const percentile = 25;
    const radius = 10.42;
    const labels = ['label0', 'label1'];

    beforeEach(() => {
      subject.addHealthFacilityPercentile(latitude, longitude, percentile, radius, labels);
    });

    it('is expected to create a circle', () => {
      expect(Leaflet.circle).toHaveBeenCalledWith(
        [latitude, longitude],
        {
          // @ts-ignore: TS2341
          color: subject.percentileColors[percentile],
          // @ts-ignore: TS2341
          fillColor: subject.percentileColors[percentile],
          fillOpacity: 0.2,
          radius: radius*1000,
        }
      );
    });

    it('is expected to bind a tooltip to the circle', () => {
      expect(circle.bindTooltip).toHaveBeenCalledWith(
        `<b>${labels[0]}</b>: ${percentile}%<br><b>${labels[1]}</b>: ${radius} Km`,
        {direction:'top'});
    });

    it('is expected to add the circle to the layer group', () => {
      expect(circle.addTo).toHaveBeenCalledWith(layerGroup);
    });
  });

  describe('toggleHealthFacilitiesPercentiles', () => {
    describe('when the health facilities are not a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);

        subject.toggleHealthFacilitiesPercentiles();
      });

      it('is expected to add the health facilities percentiles group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.addLayer).toHaveBeenCalledWith(layerGroup);
      });
    });

    describe('when the health facilities percentiles already are a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);

        subject.toggleHealthFacilitiesPercentiles();
      });

      it('is expected to add the health facilities percentiles group as a map layer', () => {
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.removeLayer).toHaveBeenCalledWith(layerGroup);
      });
    });
  });
});
