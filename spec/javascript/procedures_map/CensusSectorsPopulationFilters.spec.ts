import * as jQuery from 'jquery';

import { CensusSectorsPopulationFilters } from '../../../app/javascript/packs/procedures_map/CensusSectorsPopulationFilters';

describe('CensusSectorsPopulationFilters', () => {
  const genderFilters = ['female', 'male'];
  const raceFilters = ['black', 'mulatto', 'native', 'white', 'yellow'];

  let subject: CensusSectorsPopulationFilters;

  beforeEach(() => {
    subject = new CensusSectorsPopulationFilters(genderFilters, raceFilters, jQuery);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });

    it('is expected to set the genderFilters private attribute', () => {
      // @ts-ignore: TS2341
      expect(subject.genderFilters).toEqual(genderFilters);
    });

    it('is expected to set the raceFilters private attribute', () => {
      // @ts-ignore: TS2341
      expect(subject.raceFilters).toEqual(raceFilters);
    });

    it('is expected to set the jQuery private attribute', () => {
      // @ts-ignore: TS2341
      expect(subject.jQuery).toEqual(jQuery);
    });
  });

  describe('checkbox', () => {
    const element = jasmine.createSpy('element');
    const filter = 'men';

    let result: any;

    beforeEach(() => {
      // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'find').and.returnValue(element);

      result = subject.checkbox(filter);
    });

    it('is expected to query using jquery', () => {
      expect(jQuery.fn.find).toHaveBeenCalledWith(`input[name='csw_heat_map[${filter}]'][type=checkbox]`);
    });

    it('is expected to return the element', () => {
      expect(result).toEqual(element);
    });
  });

  describe('uncheckAll', () => {
    const checkbox = jasmine.createSpyObj('checkbox', ['prop']);

    beforeEach(() => {
      spyOn(subject, 'checkbox').and.returnValue(checkbox);

      subject.uncheckAll(genderFilters);
    });

    it('is expected to iterate over the parameter filters disabling them', () => {
      for(const filter of genderFilters) {
        expect(subject.checkbox).toHaveBeenCalledWith(filter);
        expect(checkbox.prop).toHaveBeenCalledWith('checked', false);
      }
    });
  });

  describe('changing', () => {
    const checkbox = jasmine.createSpyObj('checkbox', ['prop']);
    const filter = 'men';

    beforeEach(() => {
      spyOn(subject, 'checkbox').and.returnValue(checkbox);
      spyOn(subject, 'uncheckAll')
    });

    describe('when the checkbox is unchecked', () => {
      beforeEach(() => {
        checkbox.prop.and.returnValue(false);

        subject.changing(filter);
      });

      it('is expected to not uncheck any other box', () => {
        expect(subject.uncheckAll).not.toHaveBeenCalled();
      });

      it('is expected to get the checkbox state', () => {
        expect(subject.checkbox).toHaveBeenCalledWith(filter);
        expect(checkbox.prop).toHaveBeenCalledWith('checked');
      });
    });

    describe('when the checkbox is checked', () => {
      beforeEach(() => {
        checkbox.prop.and.returnValue(true);

        subject.changing(filter);
      });

      describe('and the filter is a gender filter', () => {
        beforeEach(() => {
          spyOn(genderFilters, 'includes').and.returnValue(true);

          subject.changing(filter);
        });

        it('is expected to check if the filter is a gender one', () => {
          expect(genderFilters.includes).toHaveBeenCalledWith(filter);
        })

        it('is expected to uncheck all race filters', () => {
          expect(subject.uncheckAll).toHaveBeenCalledWith(raceFilters);
        });

        it('is expected to get the checkbox state', () => {
          expect(subject.checkbox).toHaveBeenCalledWith(filter);
          expect(checkbox.prop).toHaveBeenCalledWith('checked');
        });
      });

      describe('and the filter is a race filter', () => {
        beforeEach(() => {
          spyOn(genderFilters, 'includes').and.returnValue(false);

          subject.changing(filter);
        });

        it('is expected to check if the filter is a gender one', () => {
          expect(genderFilters.includes).toHaveBeenCalledWith(filter);
        })

        it('is expected to uncheck all gender filters', () => {
          expect(subject.uncheckAll).toHaveBeenCalledWith(genderFilters);
        });

        it('is expected to get the checkbox state', () => {
          expect(subject.checkbox).toHaveBeenCalledWith(filter);
          expect(checkbox.prop).toHaveBeenCalledWith('checked');
        });
      });
    });
  });
});
