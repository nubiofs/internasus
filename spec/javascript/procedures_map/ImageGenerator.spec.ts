import { ImageGenerator } from '../../../app/javascript/packs/procedures_map/ImageGenerator';

describe('ImageGenerator', () => {
  const ext = jasmine.createSpyObj('externalMocks', [
    'html2canvas',
    'jQuery'
  ]);
  const selectedElement = jasmine.createSpy('selectedElement');
  const formId = 'something';
  const imageInputId = 'otherthing';
  const form = jasmine.createSpyObj('form', ['submit']);
  const imageInput = jasmine.createSpyObj('imageInput', ['val']);

  let subject: ImageGenerator;

  beforeEach(() => {
    ext.jQuery.withArgs(`#${formId}`).and.returnValue(form);
    ext.jQuery.withArgs(`#${imageInputId}`).and.returnValue(imageInput);

    subject = new ImageGenerator(
      ext.html2canvas,
      ext.jQuery,
      formId,
      imageInputId
    );
  });

  describe('html2canvasWrapper', () => {
    const childStyle = jasmine.createSpyObj('div child style', ['removeProperty']);
    const child = { style: childStyle };
    const testDiv = jasmine.createSpyObj('div', {'querySelector': child });
    const callback = (canvas) => undefined;

    const thenMock = jasmine.createSpyObj('then', ['then']);

    beforeEach(() => {
      ext.html2canvas.and.returnValue(thenMock);

      subject.html2canvasWrapper(testDiv, callback);
    });

    it('is expected to call html2canvas', () => {
      expect(ext.html2canvas).toHaveBeenCalledWith(testDiv, {
        allowTaint: true,
        useCORS: true,
        scrollX: 0,
        scrollY: -window.scrollY
      });
    });

    it('is expected to run our callback function', () => {
      expect(thenMock.then).toHaveBeenCalledWith(callback);
    });

    it('is expected to try and find child elements displayed in the screen', () => {
      expect(testDiv.querySelector).toHaveBeenCalledWith("[style*='display: block']");
    });

    it('is expected to remove the height property of the child', () => {
      expect(childStyle.removeProperty).toHaveBeenCalledWith('height');
    });
  });

  describe('generate', () => {
    const targetId = 'something';

    beforeEach(() => {
      ext.jQuery.withArgs(`#${targetId}`).and.returnValue([selectedElement]);
      spyOn(subject, 'html2canvasWrapper');

      subject.generate(targetId);
    });

    it('is expected to select the html element matching the targetId', () => {
      expect(ext.jQuery).toHaveBeenCalledWith(`#${targetId}`);
    });

    it('is expected to call the html2canvasWrapper', () => {
      expect(subject.html2canvasWrapper).toHaveBeenCalledWith(selectedElement, jasmine.any(Function));
    });
  });

  describe('generationCallback', () => {
    const img = jasmine.createSpy('img');
    const canvas = jasmine.createSpyObj('canvas', {'toDataURL': img });

    beforeEach(() => {
      subject.generationCallback(canvas);
    });

    it('is expected to convert the canvas to base64', () => {
      expect(canvas.toDataURL).toHaveBeenCalledWith('image/png');
    });

    it('is expected to set imageInput value', () => {
      expect(imageInput.val).toHaveBeenCalledWith(img);
    });

    it('is expected to submit the form', () => {
      expect(form.submit).toHaveBeenCalled();
    });
  });
});
