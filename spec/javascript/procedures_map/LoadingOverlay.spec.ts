import { LoadingOverlay } from '../../../app/javascript/packs/procedures_map/LoadingOverlay';

describe('LoadingOverlay', () => {
  const jQuery = jasmine.createSpy('jQuery');
  const overlayButtons = jasmine.createSpyObj('overlayButtons', ['click']);
  const loadingOverlay = jasmine.createSpyObj('loadingOverlay', ['modal']);

  let subject: LoadingOverlay;

  beforeEach(() => {
    jQuery.withArgs('.overlay-button').and.returnValue(overlayButtons);
    jQuery.withArgs('#loading-overlay').and.returnValue(loadingOverlay);

    subject = new LoadingOverlay(jQuery);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });

    it('is expected to retrieve the overlayButtons list of elements', () => {
      expect(jQuery).toHaveBeenCalledWith('.overlay-button');
    });

    it('is expected to bind the the click event to overlayButtons elements', () => {
      expect(overlayButtons.click).toHaveBeenCalledWith(jasmine.any(Function));
    });
  });

  describe('dimScreen', () => {
    beforeEach(() => {
      subject.dimScreen();
    });

    it('is expected to retrieve the loadingOverlay element', () => {
      expect(jQuery).toHaveBeenCalledWith('#loading-overlay');
    });

    it('is expected to show the loadingOverlay element', () => {
      expect(loadingOverlay.modal).toHaveBeenCalledWith('show');
    });
  });
});
