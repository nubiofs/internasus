import { GeneralData } from '../../../app/javascript/packs/general_data/GeneralData';

describe('GeneralData', () => {

  const ext = jasmine.createSpyObj('externalMocks', ['jQuery'])

  let subject: GeneralData;

  beforeEach(() => {
    subject = new GeneralData(ext.jQuery);
  });

  describe('contentSelectFor', () => {
    const select = { value: 1 };
    const category = 'ranking-table';
    const allTables = jasmine.createSpyObj('allTables', ['css']);
    const visibleTable = jasmine.createSpyObj('visibleTable', ['css']);

    beforeEach(() => {
      ext.jQuery.withArgs(`[id^="${category}"]`).and.returnValue(allTables);
      ext.jQuery.withArgs(`#${category}-1`).and.returnValue(visibleTable);

      subject.contentSelectFor(category, select);
    });

    it('is expected to hide all the tables', () => {
      expect(allTables.css).toHaveBeenCalledWith('display', 'none');
    });

    it('is expected to show the selected table', () => {
      expect(visibleTable.css).toHaveBeenCalledWith('display', 'block');
    });
  });

  describe('dynamicChartFor', () => {
    const echart = jasmine.createSpyObj('echarts', ['dispose']);
    const chart = { echarts: echart, createChart: jasmine.createSpy('chart') };
    const selected = 'chart';
    const data = { chart: { chart_type: 'bar' }};
    const element = document.createElement('div');
    document.getElementById = jasmine.createSpy('element').and.returnValue(element);
    const chartData = {};
    const options = {};

    beforeEach(() => {
      spyOn(subject, 'processChartData').and.returnValue(chartData);
      spyOn(subject, 'processCustomizationOptions').and.returnValue(options);

      subject.dynamicChartFor(selected, data, chart);
    });

    it('is expected to dispose previously created charts', () => {
      expect(document.getElementById).toHaveBeenCalledWith('dynamic-chart');
      expect(echart.dispose).toHaveBeenCalledWith(element);
    });

    it('is expected to create the desired chart with its data and customization options', () => {
      expect(subject.processChartData).toHaveBeenCalledWith(data[`chart`]);
      expect(subject.processCustomizationOptions).toHaveBeenCalledWith(data[`chart`]);
      expect(chart.createChart).toHaveBeenCalledWith(
        'dynamic-chart', '',
        chartData,
        'bar',
        options,
        '#collapseGeneralDatadynamic'
      );
    });
  });

  describe('processChartData', () => {
    describe('for pie charts', () => {
      const dataSet = { chart_type: 'pie', data: { slice: 5 }};

      it('is expected to return an array of dictionaries', () => {
        const chartData = subject.processChartData(dataSet);

        expect(chartData.length).toEqual(1);
        expect(chartData[0][`name`]).toEqual('slice');
        expect(chartData[0][`value`]).toEqual(5);
      });
    });

    describe('for treemap charts', () => {
      const dataSet = { chart_type: 'treemap', data: { 'A00 - test key': 5 }};

      it('is expected to return an array of dictionaries', () => {
        const chartData = subject.processChartData(dataSet);

        expect(chartData.length).toEqual(26);
        for (let i = 0; i < 26; i++) expect(chartData[1].children.length).toEqual(10);
        expect(chartData[0].children[0].children[0][`name`]).toEqual('A00')
        expect(chartData[0].children[0].children[0][`fullname`]).toEqual('A00 - test key')
        expect(chartData[0].children[0].children[0][`value`]).toEqual(5)
      });
    });

    describe('for other charts', () => {
      const dataSet = { chart_type: 'bar', data: { slice: 5 }};

      it('is expected to return an array of arrays', () => {
        const chartData = subject.processChartData(dataSet);

        expect(chartData.length).toEqual(1);
        expect(chartData[0][0]).toEqual('slice');
        expect(chartData[0][1]).toEqual(5);
      });
    });
  });

  describe('healthFacilitiesChartFor', () => {
    const echart = jasmine.createSpyObj('echarts', ['dispose']);
    const selectedId = '0';
    const selectedName = 'healthFacility';
    const chartName = 'Chart Name';
    const data = { 'data': { 'healthFacility': [] }, 'options': {} };
    const chart = { echarts: echart, createChart: jasmine.createSpy('chart') };
    const element = document.createElement('div');
    document.getElementById = jasmine.createSpy('element').and.returnValue(element);

    beforeEach(() => {
      spyOn(subject, 'processHealthFacilityData').and.returnValue(data.data[selectedName]);
      spyOn(subject, 'processCustomizationOptions').and.returnValue(data.options);

      subject.healthFacilitiesChartFor(selectedId, chartName, data, chart);
    });

    it('is expected to dispose previously created charts', () => {
      expect(document.getElementById).toHaveBeenCalledWith('health-facility-specialty-distances-chart');
      expect(echart.dispose).toHaveBeenCalledWith(element);
    });

    it('is expected to get the name of health facility from selected id', () => {
      expect(Object.keys(data.data)[selectedId]).toBe(selectedName);
    });

    it('is expected to create the desired chart with its data and customization options', () => {
      expect(subject.processHealthFacilityData).toHaveBeenCalledWith(data.data[selectedName]);
      expect(subject.processCustomizationOptions).toHaveBeenCalledWith(data.options);
      expect(chart.createChart).toHaveBeenCalledWith(
        'health-facility-specialty-distances-chart',
        chartName,
        [],
        'stacked-bar',
        data.options,
        '#collapseGeneralDatahealth_facilities_specialty_distances'
      );
    });
  });

  describe('processHealthFacilityData', () => {
    const absoluteDistances = [1, 2, 3, 4];
    const relativeDistances = [10.0, 20.0, 30.0, 40.0];
    const dataSet = [{ 'specialty': absoluteDistances }, { 'specialty': relativeDistances }];

    it('is expected to return a matrix', () => {
      const chartData = subject.processHealthFacilityData(dataSet);

      expect(chartData.length).toEqual(3);
      expect(chartData[1][0][0]).toEqual('specialty');
      expect(chartData[2][0][0]).toEqual('specialty');
      for (let i = 0; i < 4; i++) {
        expect(chartData[1][0][i + 1]).toEqual(absoluteDistances[i]);
        expect(chartData[2][0][i + 1]).toEqual(relativeDistances[i]);
      }
    });
  });

  describe('processCustomizationOptions', () => {
    describe('including all options', () => {
      const dataSet = { xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: true, legend: { right: 20, bottom: 10 }, numberOfBars: 4, rootName: 'root name' };

      it('is expected to include all options for chart customization', () => {
        const options = subject.processCustomizationOptions(dataSet);

        expect(options.xAxis).toEqual({type: 'category'});
        expect(options.yAxis).toEqual({type: 'value'});
        expect(options.dataZoom[0]).toEqual( {id: 'dataZoomX', type: 'slider', xAxisIndex: [0], filterMode: 'filter' });
        expect(options.dataZoom[1]).toEqual( {id: 'dataZoomY', type: 'slider', yAxisIndex: [0], filterMode: 'empty' });
        expect(options.legend).toEqual({right: 20, bottom: 10});
        expect(options.numberOfBars).toEqual(4);
        expect(options.rootName).toEqual('root name');
      });
    });

    describe('with axis with more options', () => {
      const dataSet = { xAxis: { type: 'value', max: 100, axisLabel: { formatter: '{value}%' } }, yAxis: { type: 'category', axisLabel: { interval: 0 } }, xZoom: true, yZoom: false, rootName: '' };

      it('is expected to include only specified options for chart customization', () => {
        const options = subject.processCustomizationOptions(dataSet);

        expect(options.xAxis).toEqual({ type: 'value', max: 100, axisLabel: { formatter: '{value}%' } });
        expect(options.yAxis).toEqual({ type: 'category', axisLabel: { interval: 0 } });
        expect(options.dataZoom[0]).toEqual({ id: 'dataZoomX', type: 'slider', xAxisIndex: [0], filterMode: 'filter' });
        expect(options.dataZoom[1]).toEqual(undefined);
        expect(options.rootName).toEqual(undefined);
      });
    });

    describe('with only a few options', () => {
      const dataSet = { xAxisType: 'category', yAxisType: '', xZoom: true, yZoom: false, rootName: '' };

      it('is expected to include only specified options for chart customization', () => {
        const options = subject.processCustomizationOptions(dataSet);

        expect(options.xAxis).toEqual({ type: 'category' });
        expect(options.yAxis).toEqual(undefined);
        expect(options.dataZoom[0]).toEqual({ id: 'dataZoomX', type: 'slider', xAxisIndex: [0], filterMode: 'filter' });
        expect(options.dataZoom[1]).toEqual(undefined);
        expect(options.rootName).toEqual(undefined);
      });
    });

    describe('with no options at all', () => {
      const dataSet = { xAxisType: '', yAxisType: '', xZoom: false, yZoom: false, rootName: '' };

      it('is expected to include leave empty the options for chart customization', () => {
        const options = subject.processCustomizationOptions(dataSet);

        expect(options).toEqual({});
      });
    });
  });
});
