# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomepageController do
  describe 'routing' do
    it {
      is_expected.to route(:get, '/')
        .to(controller: :homepage, action: :index)
    }
  end
end
