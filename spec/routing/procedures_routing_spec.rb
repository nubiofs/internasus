# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/controller_with_index'

RSpec.describe ProceduresController do
  describe 'routing' do
    let(:id) { '67a2ebd6-9079-48b5-b709-a98616b40904' }

    it_behaves_like 'controller with index', 'procedures'

    it {
      is_expected.to route(:post, 'procedures/search')
        .to(controller: :procedures, action: :search)
    }

    it {
      is_expected.to route(:get, 'procedures/search')
        .to(controller: :procedures, action: :index)
    }

    it {
      is_expected.to route(:get, 'procedures/export')
        .to(controller: :procedures, action: :export)
    }

    it {
      is_expected.to route(:get, "procedures/#{id}")
        .to(controller: :procedures, action: :show, id: id)
    }

    it {
      is_expected.to route(:post, 'procedures/search')
        .to(controller: :procedures, action: :search)
    }

    it {
      is_expected.to route(:post, 'procedures/print')
        .to(controller: :procedures, action: :print)
    }
  end
end
