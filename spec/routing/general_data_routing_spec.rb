# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/controller_with_index'

RSpec.describe GeneralDataController do
  describe 'routing' do
    it_behaves_like 'controller with index', 'general_data'
  end

  it {
    is_expected.to route(:post, 'general_data/print')
      .to(controller: :general_data, action: :print)
  }
end
