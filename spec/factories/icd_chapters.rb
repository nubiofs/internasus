# frozen_string_literal: true

FactoryBot.define do
  factory :icd_chapter do
    code { 1 }
    first_category { 'MyString' }
    last_category { 'MyString' }
    description { 'MyText' }
  end
end
