# frozen_string_literal: true

FactoryBot.define do
  factory :census_sector do
    code { 1 }
    shape { "{ 'type': 'Polygon', 'coordinates': [[[-46.25,-23.85],[-46.25,-23.85],[-46.25,-23.85]]] }" }
    total_population { 314 }
    women_population { 314 }
    men_population { 314 }
    black_population { 314 }
    mulatto_population { 314 }
    native_population { 314 }
    white_population { 314 }
    yellow_population { 314 }
    latitude { -23.0 }
    longitude { -46.0 }
  end
end
