# frozen_string_literal: true

FactoryBot.define do
  factory :family_health_strategy do
    name { 'MyString' }
    shape { 'MyString' }
  end
end
