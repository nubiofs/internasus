# frozen_string_literal: true

FactoryBot.define do
  factory :patient_datum do
    latitude { 1.5 }
    longitude { 1.5 }
    gender { 'F' }
    race { '99' }
    age_year { 1 }
    age_code { 'MyString' }
    educational_level { 1 }
  end
end
