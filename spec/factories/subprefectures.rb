# frozen_string_literal: true

FactoryBot.define do
  factory :subprefecture do
    name { 'MyString' }
    shape { "{ 'type': 'Polygon', 'coordinates': [[[-46.25,-23.85],[-46.25,-23.85],[-46.25,-23.85]]] }" }
  end
end
