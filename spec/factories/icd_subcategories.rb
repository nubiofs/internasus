# frozen_string_literal: true

FactoryBot.define do
  factory :icd_subcategory do
    code { 'MyString' }
    description { 'MyText' }
  end
end
