# frozen_string_literal: true

FactoryBot.define do
  factory :city do
    name { 'MyString' }
    shape { 'MyString' }
  end
end
