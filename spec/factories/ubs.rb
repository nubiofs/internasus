# frozen_string_literal: true

FactoryBot.define do
  factory :ub, class: 'Ubs' do
    name { 'MyString' }
    shape { 'MyString' }
  end
end
