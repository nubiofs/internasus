# Revision history for InternaSUS

The version numbers below try to follow the conventions at http://semver.org/.

## Unreleased

* Add CNAES to health facility dropdown filter on search page
* Change age codes so they can be ordered alphabetically more easily
* Add information icon on each general data section
* Print general data sections
* Change complexities chart from line to bar chart
* Order health facilities alphabetically on general data charts
* Show percentage of data bars on general data graphs
* Increase font size for general data charts
* Add health facilities bar charts on general data
* Add health facilities facility type chart on general data
* Display distances section in general data page
* Add scroll bar on rankings table
* Display census sectors data on general data page
* Fix characterization section on general data page
* Display health facilities specialty distances chart on general data page
* Adjust heatmap radius when map is zoomed
* Display dynamic charts' ICD diagnoses charts on general data page
* Display descriptive statistics table on general data page
* Display most of dynamic charts
* Display technical health supervision chart on general data page
* Display regional health coordinator chart on general data page
* Display administrative sector chart on general data page
* Upgrade to PostgreSQL 12 on production server
* Install fail2ban and configure on production server
* Leave only ports 20, 80 and 443 open on production environment using UFW
* Display hospitalizations per hospitalization days bar chart on general data page
* Display hospitalizations per age code bar chart on general data page
* Display hospitalizations per type pie chart on general data page
* Display hospitalizations per race pie chart on general data page
* Display hospitalizations per gender pie chart on general data page
* Display hospitalization percentage per specialty pie chart on general data page
* Display specialties bar chart on general data page
* Add time series chart to general data
* Fix loading percentiles on map when there are nil values
* Perform general node modules updates
* Perform general gem updates
* Update rspec-rails
* Update Rails to 6.0.3

## v0.4.0 - 15/05/2020

* Adjust min and max clustering radius values for map options sliders
* Fix heatmap radius units
* Fix procedures search page wording
* Fix GeneralData JS loading
* Adjust heatmap intensity accordingly to dataset proportion
* Create HealthFacility distances percentiles map tooltip
* Create a route for GET /procedures/search
* Increase NGINX max request size
* Add legends to map on Health Facilities map markers
* Group map export buttons into section
* Shorten weighted heatmap population filters section title
* Fix map pdf generation with Firefox

## v0.3.1 - 17/04/2020

* Fix lint error

## v0.3.0 - 17/04/2020

* Create FamilyHealthStrategy model
* Add UBS limits to procedures map
* Add UBS seeds
* Create Ubs model
* Add city limits to procedures map
* Add city seeds
* Create City model
* Add minimap to procedures map
* Install leaflet-minimap
* Implement General Data rankings section
* Create GeneralDataService
* Rename minimal layout to wicked_pdf
* Install wicked_pdf
* Display map for pdf generation
* Install html2canvas
* Create minimal layout
* Implement General Data characterization section
* Create General Data index page
* Plot HealthFacilities HospitalizationDatum distances percentiles
* Install descriptive_statistics
* Create HealthFacility map markers toggle
* Create HealthFacility map popup
* Create HealthFacility controller and its show action
* Add heatmap opacity option on procedures map
* Add heatmap high contrast option on procedures map
* Add heatmap radius option on procedures map
* Add patient cluster radius option on procedures map
* Create procedures search results printing page
* Perform general gem updates
* Update bootstrap
* Update JS dependencies
* Update rails
* Export filtered procedures to CSV
* Install activerecord-session_store
* Install ransack_memory
* Update browser-env
* Update @rails/webpacker
* Uncheck conflicting census sector population filters
* Create CensusSectorsPopulationFilters JS (ts) class
* Filter CensusSector total population for weighted heat map
* Create ProceduresSearchService
* Add census sector weighted heatmap layer to procedures map
* Add latitude and longitude to CensusSector
* Create CensusSectorsProceduresCounterService
* Update puma

## v0.2.0 - 27/02/2020

* Filter map data by PatientDatum attributes
* Update nokogiri
* Upgrade to Ruby 2.7
* Check for inconsistent naming after seeding
* Fix seed data inconsistent naming
* Add dependency license check to CI
* Display administrative sectors limits on map
* Display subprefecture limits on map
* Display technical health supervisions limits on map
* Display regional health coordinations limits on map
* Display census sectors limits on map
* Add heatmap layer to procedures map
* Install Leaflet.heat
* Implement PatientData cluster toggle
* Extract PatientData cluster addition to map as class method
* Extract Leaflet map size invalidation to class method
* Extract patient data map marker creation to class method
* Extract PatientData map popup creator to class method
* Extract HealthFacility map marker creation JS to class method
* Extract Leaflet map initialization to class
* Install tslint
* Install jasmine
* Install TypeScript support to webpacker
* Create map option accordion
* Filter map data by Procedure attributes

## v0.1.0 - 23/01/2020

* Set homepage counters
* Filter map data by HealthFacility attributes
* Use redis for caching
* Use ajax to load PatientData map popup
* Create Procedure show page
* Translate PatientDatum education level
* Create translation helper for PatientDatum race
* Create translation helper for PatientDatum gender
* Load PatientData and associated information to map popup
* Create search methods between PatientData and HospitalizationData
* Associate PatientData to CensusSectors
* Add gitleaks and brakeman security checks on CI
* Install ransack
* Deploy procedures seed data
* Store encrypted procedures seed data
* Load and cluster patient data to map
* Install Leaflet.markercluster
* Load health facilities to map
* Install leaflet
* Install bootstrap-select, bootstrap-slider and bootstrap-datepicker
* Set advanced search health facility, procedure and patient data fields box titles
* Set footer to page bottom if content does not fill entire page
* Create Procedures index (Advanced  Search)
* Start deployed webserver in production mode
* Seed Patient Data and Hospitalization Data
* Switch From Sqlite3 to Postgres
* Seed CensusSector data
* Add administration interface
* Add association between Hospitalization Data and Procedure
* Add association between Patient Data and Procedure
* Create Procedure entity
* Seed Health Facilities data from PMSP data
* Add association between ICD Category and Hospitalization Data
* Add association between ICD Subcategory and Hospitalization Data
* Add association between Health Facility and Hospitalization Data
* Create HospitalizationData entity
* Seed ICD 10 data
* Add association between Regional Health Coordination and Patient Data
* Add association between Technical Health Supervision and Patient Data
* Add association between Subprefecture and Patient Data
* Add association between Administrative Sector and Patient Data
* Create PatientData entity
* Install rails-i18n
* Add association between Administrative Sector and Census Sector
* Create Census Sector entity
* Add association between Regional Health Coordination and Health Facility
* Add association between Technical Health Supervision and Health Facility
* Add association between Subprefecture and Health Facility
* Add association between Administrative Sector and Health Facility
* Create HealthFacility entity
* Create RegionalHealthCoordination entity
* Create TechnicalHealthSupervision entity
* Create Subprefecture entity
* Create Administrative Sector entity
* Add association between ICD Chapter and Group
* Add association between ICD Chapter and Category
* Create ICD Chapter entity
* Add association between ICD Group and Category
* Create ICD Group entity
* Add association between ICD Category and Subcategory
* Create ICD Category entity
* Create ICD Subcategory entity
* Integrate user login and logout buttons to Devise
* Create User model
* Setup code duplication detection
* Create services section in homepage
* Create homepage counter
* Add homepage slider images
* Configure continuous delivery
* Automate NGINX proxy installation
* Automate webserver deployment
* Automate docker installation for deployment
* Automate deployment user creation
* Create deployment setup playbook
* Add responsive design to footer
* Create footer links
* Add partner logo to footer
* Add contact and address to footer
* Extract footer to layout partial
* Add partner logo to header
* Create header links
* Internationalize application title
* Extract navbar to layout partial
* Create Dockerfile
* Create CI script
* Create HomepageController with an index
* Install Bootstrap
* Set license to MPL
* Install Rubocop
* Install Bullet
* Install bundler-audit
* Install shoulda matchers
* Install FactoryBot
* Install Cucumber
* Install simplecov
* Install RSpec
* Create empty rails application
