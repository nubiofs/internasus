image: circleci/ruby:2.7-node-browsers

services:
  - postgres:latest

variables:
  POSTGRES_DB: internasus_db
  POSTGRES_USER: internasus_user
  POSTGRES_PASSWORD: internasus_pass
  RAILS_ENV: test

cache:
  key: "GLOBAL-CACHE-KEY"
  paths:
    - .bundle/
    - node_modules/

stages:
  - lint
  - test
  - build
  - deploy

before_script:
  - bundle config set path .bundle
  - bundle install -j $(nproc)
  - sudo apt-get install -y build-essential cmake wkhtmltopdf
  - cp config/database-ci.yml config/database.yml
  - ./bin/setup

bundle-audit:
  stage: lint
  script:
    - gem install bundler-audit --no-document
    - bundle audit --update

brakeman:
  stage: lint
  script:
    - bundle exec brakeman

yarn-audit:
  stage: lint
  script:
    - yarn audit
  allow_failure: true

gitleaks:
  stage: lint
  image: zricethezav/gitleaks
  cache: {}
  before_script: []
  script:
    - gitleaks --repo-path=.

rubocop:
  stage: lint
  script:
    - bundle exec rubocop

flay:
  stage: lint
  script:
    - find . -type f -name '*.rb' -not -path './.bundle/*' -exec bundle exec flay -s {} + | sort -h > current_flay_output.txt
    - diff expected_flay_output.txt current_flay_output.txt

tslint:
  stage: lint
  script:
    - yarn lint

license:
  stage: lint
  script:
    - bundle exec license_finder

rspec:
  stage: test
  script:
    - bundle exec rails spec

cucumber:
  stage: test
  script:
    - bundle exec rails cucumber

jasmine:
  stage: test
  script:
    - yarn test

deploy_dry_run:
  stage: test
  image: python
  cache: {}
  before_script:
    - echo 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main' > /etc/apt/sources.list.d/ansible.list
    - apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
    - apt-get update -y
    - apt-get install -y ansible
    - echo $DEPLOYER_SSH_KEY | base64 -d > deploy_key # encoded with `base64 deploy_key | tr -d '\n'`
    - chmod 600 deploy_key
    - echo $VAULT_PASS > deploy/.vault-pass.txt
  script:
    - pushd deploy
    - ansible-playbook --syntax-check setup.yml
    - ansible-playbook deploy.yml -C -i staging -u deployer --private-key ../deploy_key --ssh-extra-args "-o StrictHostKeyChecking=no"

build_and_push_docker:
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  image: docker:stable
  cache: {}
  only:
    - master
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY/interscity/health-dashboard/internasus
    - docker build --pull -t $CI_REGISTRY/interscity/health-dashboard/internasus .
    - docker push $CI_REGISTRY/interscity/health-dashboard/internasus

deploy:
  stage: deploy
  image: python
  cache: {}
  only:
    - master
  before_script:
    - echo 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main' > /etc/apt/sources.list.d/ansible.list
    - apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
    - apt-get update -y
    - apt-get install -y ansible
    - echo $DEPLOYER_SSH_KEY | base64 -d > deploy_key # encoded with `base64 deploy_key | tr -d '\n'`
    - chmod 600 deploy_key
    - echo $VAULT_PASS > deploy/.vault-pass.txt
  script:
    - pushd deploy
    - ansible-playbook deploy.yml -i staging -u deployer --private-key ../deploy_key --ssh-extra-args "-o StrictHostKeyChecking=no"
