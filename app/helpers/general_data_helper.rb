# frozen_string_literal: true

module GeneralDataHelper
  def descriptive_statistics_translations(descriptive_statistics)
    descriptive_statistics_titles = {
      age_year: t('general_data.descriptive_statistics.age'),
      hospitalization_rates: t('general_data.descriptive_statistics.hospitalization_rates'),
      uti_rates: t('general_data.descriptive_statistics.uti_rates'),
      ui_rates: t('general_data.descriptive_statistics.ui_rates'),
      service_values: t('general_data.descriptive_statistics.service_value'),
      hospitalization_days: t('general_data.descriptive_statistics.hospitalization_days')
    }

    descriptive_statistics.transform_keys { |key| descriptive_statistics_titles[key] }
  end

  def rankings_translations(rankings)
    ranking_titles = {
      health_facilities_ranking: HealthFacility.model_name.human,
      administrative_sectors: AdministrativeSector.model_name.human,
      age_codes: t('procedures.index.patient.age_code'),
      genders: t('procedures.index.patient.gender'),
      icd_categories: IcdCategory.model_name.human
    }

    rankings.transform_keys { |category| ranking_titles[category] }
  end

  def dynamic_section_titles
    ChartsConfigurationsConcern::DYNAMIC_CHARTS_INFO.keys.map { |key| [t(key, scope: 'general_data.dynamic'), key] }
  end

  def census_sector_rate_value_total(census_sector, counts)
    value = counts[:total][census_sector] || 0
    total = census_sector.total_population

    cell_rate_string value, total
  end

  def census_sector_rate_value(census_sector, counts, index, reference)
    value = counts[census_sector.id][index] || 0
    total = census_sector.send(reference)

    cell_rate_string value, total
  end

  def cell_rate_string(value, total)
    ratio = total.zero? ? '-' : format('%<ratio>.2f%%', ratio: (value / total.to_f))

    "#{value}/#{total} (#{ratio})"
  end
end
