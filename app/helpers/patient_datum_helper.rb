# frozen_string_literal: true

module PatientDatumHelper
  def patient_model_options(attribute)
    PatientDatum
      .select(attribute).distinct
      .where.not(attribute => nil)
      .order(attribute)
      .map do |patient_datum|
        [
          human_attribute_value(patient_datum, attribute.to_s),
          patient_datum[attribute]
        ]
      end
  end

  def age_options
    patient_model_options(:age_code)
  end

  def race_options
    patient_model_options(:race)
  end

  def education_options
    patient_model_options(:educational_level)
  end
end
