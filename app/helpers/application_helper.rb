# frozen_string_literal: true

module ApplicationHelper
  def flash_class(level)
    case level
    when 'notice' then 'alert alert-info'
    when 'success' then 'alert alert-success'
    when 'error' then 'alert alert-danger'
    when 'alert' then 'alert alert-warning'
    end
  end

  def human_attribute_value(model_instance, attribute_name)
    value = model_instance.send(attribute_name)
    model_key = model_instance.class.to_s.underscore

    I18n.t("#{model_key}.#{attribute_name}_#{value}")
  end
end
