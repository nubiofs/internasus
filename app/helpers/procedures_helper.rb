# frozen_string_literal: true

module ProceduresHelper
  def model_icd_options(model_class)
    model_class
      .distinct
      .where.not(code: nil)
      .order(:code)
      .pluck(:code, :id, :description)
      .map do |option|
        formated_option = option.clone
        formated_option[0] = option[0] + ' - ' + option[2].titleize
        formated_option[0..1]
      end
  end

  def icd_category_options
    model_icd_options(IcdCategory)
  end

  def icd_subcategory_options
    model_icd_options(IcdSubcategory)
  end

  def model_number_attribute_options(model_class, attribute)
    model_class
      .distinct
      .where
      .not(attribute => nil)
      .order(attribute)
      .pluck(attribute)
  end

  def hospitalization_type_options
    model_number_attribute_options(HospitalizationDatum, :hospitalization_type)
  end

  def complexity_options
    model_number_attribute_options(HospitalizationDatum, :complexity)
  end

  def specialty_options
    model_number_attribute_options(HospitalizationDatum, :specialty)
  end

  def competence_options
    model_number_attribute_options(HospitalizationDatum, :competence)
  end

  def hospitalization_group_options
    model_number_attribute_options(HospitalizationDatum, :hospitalization_group)
  end

  def financing_options
    model_number_attribute_options(HospitalizationDatum, :financing)
  end

  def max_hospitalization_days
    HospitalizationDatum.maximum(:hospitalization_days)
  end

  def max_hospitalization_rates
    HospitalizationDatum.maximum(:hospitalization_rates)
  end

  def max_uti_rates
    HospitalizationDatum.maximum(:uti_rates)
  end

  def max_ui_rates
    HospitalizationDatum.maximum(:ui_rates)
  end

  def max_service_value
    HospitalizationDatum.maximum(:service_value)
  end

  def max_distance
    HospitalizationDatum.maximum(:distance)
  end
end
