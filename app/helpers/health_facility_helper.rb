# frozen_string_literal: true

module HealthFacilityHelper
  def health_facilities_options
    HealthFacility.distinct.where.not(name: nil)
                  .pluck(:cnes, :name, :id).map do |option|
      ["#{option[0]} - #{option[1].titleize}", option[2]]
    end
  end

  def administration_options
    HealthFacility.distinct.where.not(administration: nil)
                  .pluck(:administration).map(&:titleize)
  end

  def max_beds
    HealthFacility.maximum(:beds)
  end

  def model_search_options(model_class)
    model_class
      .distinct
      .where
      .not(name: nil)
      .order(:name)
      .pluck(:name, :id)
      .map do |option|
        option[0] = option[0].titleize
        option
      end
  end

  def as_options
    model_search_options(AdministrativeSector)
  end

  def subprefecture_options
    model_search_options(Subprefecture)
  end

  def ths_options
    model_search_options(TechnicalHealthSupervision)
  end

  def rhc_options
    model_search_options(RegionalHealthCoordination)
  end

  def distance_percentiles(health_facility)
    distances = health_facility.hospitalization_data.pluck(:distance).extend(DescriptiveStatistics)

    percentiles = {}
    [75, 50, 25].each do |percentile|
      value = distances.percentile(percentile)
      percentiles[percentile] = value unless value.nil?
    end

    percentiles
  end
end
