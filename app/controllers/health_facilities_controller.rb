# frozen_string_literal: true

class HealthFacilitiesController < ApplicationController
  def show
    @health_facility = HealthFacility
                       .find(params[:id])

    render layout: false
  end
end
