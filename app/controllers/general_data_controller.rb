# frozen_string_literal: true

class GeneralDataController < ApplicationController
  include RansackMemory::Concern
  include ProceduresSearchConcern

  before_action :save_and_load_filters, only: %i[index]
  before_action :set_search_results, only: %i[index]

  def index
    @total_records = HospitalizationDatum.count
    general_data_service = GeneralDataService.new(@health_facilities, @procedures_ids)
    census_sectors_procedures_counter_service = CensusSectorsProceduresCounterService.new(@procedures_ids)
    @stats = general_data_service.stats
    @rankings = general_data_service.rankings
    @dynamic = general_data_service.dynamic
    @descriptive_statistics = general_data_service.descriptive_statistics
    @census_sector_counts = census_sectors_procedures_counter_service.census_sector_statistics
    @census_sectors = CensusSector.where(id: @census_sector_counts[:ids])
    @distances = general_data_service.distances
  end

  def print
    @image = print_params[:image]
    @kind = print_params[:kind]

    render pdf: "general_data_#{@kind}", page_size: 'A4', layout: 'wicked_pdf'
  end

  private

  def print_params
    params.permit(:kind, :image)
  end
end
