# frozen_string_literal: true

class ProceduresController < ApplicationController
  include RansackMemory::Concern
  include ProceduresSearchConcern

  before_action :set_administrative_entities, only: %i[index search print]
  before_action :save_and_load_filters, only: %i[search export print]
  before_action :set_search_results, only: %i[index search export print]

  def index
    @census_sectors_counts = {}
  end

  def search
    census_sectors_procedures_counter = CensusSectorsProceduresCounterService.new(@procedures_ids)
    csw_heat_map_params.each do |filter, value|
      census_sectors_procedures_counter.set_filter(filter.to_sym, value == 'true')
    end
    @census_sectors_counts = census_sectors_procedures_counter.counts

    render 'index'
  end

  def show
    @patient_datum = PatientDatum
                     .includes(
                       :administrative_sector, :subprefecture, :technical_health_supervision,
                       :regional_health_coordination, :census_sector
                     )
                     .find_by!(procedure_id: params[:id])

    @hospitalization_datum = HospitalizationDatum
                             .includes(:icd_category)
                             .find_by!(procedure_id: params[:id])

    render layout: false
  end

  def export
    @patients_data = PatientDatum.includes(
      { hospitalization_datum: %i[health_facility icd_subcategory secondary_icd1 secondary_icd2] },
      :administrative_sector,
      :subprefecture,
      :technical_health_supervision,
      :regional_health_coordination
    ).where(procedure_id: @procedures_ids)
  end

  def print
    @base64_map = print_params[:map_image]

    render pdf: 'procedures', orientation: 'Landscape', page_size: 'A4', layout: 'wicked_pdf'
  end

  private

  def set_administrative_entities
    @cities = City.all
    @census_sectors = CensusSector.all
    @regional_health_coordinations = RegionalHealthCoordination.all
    @technical_health_supervisions = TechnicalHealthSupervision.all
    @subprefectures = Subprefecture.all
    @administrative_sectors = AdministrativeSector.all
    @ubs = Ubs.all
    @fhs = FamilyHealthStrategy.all
  end

  def csw_heat_map_params
    params.fetch(:csw_heat_map).permit(CensusSectorsProceduresCounterService::ALL_FILTERS)
  end

  def print_params
    params.permit(:map_image)
  end
end
