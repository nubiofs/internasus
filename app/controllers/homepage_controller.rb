# frozen_string_literal: true

class HomepageController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @hospitalization_counter = HospitalizationDatum.count
    @health_facility_counter = HealthFacility.count
    @specialty_counter = HospitalizationDatum.select(:specialty).distinct.count
    @mean_distance = HospitalizationDatum.average(:distance)
  end
end
