# frozen_string_literal: true

module ProceduresSearchConcern
  extend ActiveSupport::Concern

  def set_search_results
    @active_filters = ProceduresSearchService.active_filters(params[:q])
    @search,
      @health_facilities,
      @patients_data,
      @procedures_ids,
      @dataset_proportion = ProceduresSearchService.search(params[:q])
  end
end
