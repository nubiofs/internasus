# frozen_string_literal: true

class PatientDatum < ApplicationRecord
  belongs_to :administrative_sector
  belongs_to :subprefecture
  belongs_to :technical_health_supervision
  belongs_to :regional_health_coordination
  belongs_to :census_sector
  belongs_to :hospitalization_datum, inverse_of: :patient_datum,
                                     foreign_key: 'procedure_id',
                                     primary_key: 'procedure_id'
end
