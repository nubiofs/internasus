# frozen_string_literal: true

class Subprefecture < ApplicationRecord
  has_many :health_facilities, dependent: :nullify
  has_many :patient_data, dependent: :nullify
end
