# frozen_string_literal: true

class HospitalizationDatum < ApplicationRecord
  belongs_to :health_facility
  belongs_to :icd_subcategory
  belongs_to :icd_category
  belongs_to :secondary_icd1, class_name: 'IcdSubcategory', optional: true
  belongs_to :secondary_icd2, class_name: 'IcdSubcategory', optional: true
  has_one :patient_datum, foreign_key: 'procedure_id',
                          primary_key: 'procedure_id',
                          dependent: :nullify,
                          inverse_of: :hospitalization_datum
end
