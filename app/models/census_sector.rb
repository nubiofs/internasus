# frozen_string_literal: true

class CensusSector < ApplicationRecord
  has_many :patient_data, dependent: :nullify
  belongs_to :administrative_sector

  alias_attribute :name, :code
end
