# frozen_string_literal: true

class HealthFacility < ApplicationRecord
  belongs_to :administrative_sector
  belongs_to :subprefecture
  belongs_to :technical_health_supervision
  belongs_to :regional_health_coordination
  has_many :hospitalization_data, dependent: :nullify
end
