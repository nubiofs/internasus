# frozen_string_literal: true

class AdministrativeSector < ApplicationRecord
  has_many :health_facilities, dependent: :nullify
  has_many :census_sectors, dependent: :nullify
  has_many :patient_data, dependent: :nullify
end
