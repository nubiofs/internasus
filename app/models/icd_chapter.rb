# frozen_string_literal: true

class IcdChapter < ApplicationRecord
  has_many :icd_categories, dependent: :nullify
  has_many :icd_groups, dependent: :nullify
end
