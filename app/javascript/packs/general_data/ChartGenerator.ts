export class ChartGenerator {
  private chart: any;

  constructor(public echarts: any, public jQuery: any, public window: any) {}

  createChart(elementId: string, title: string, data: (any|number)[], chartType: string,
    customizationOptions: {[key: string]: any}, resizeTriggerElement: string, percentage = true) {

    let option: {[key: string]: any} = {};
    this.chart = this.echarts.init(document.getElementById(elementId));

    option = this.addDefaultOption(data, title, option);
    option.xAxis = customizationOptions.xAxis;
    option.yAxis = customizationOptions.yAxis;
    option.dataZoom = customizationOptions.dataZoom;
    option.legend = customizationOptions.legend || {};
    option.series = [{type: chartType}];

    switch(chartType) {
      case 'pie':
        option = this.addDefaultPieChartOptions(option, data);
        break;
      case 'treemap':
        option = this.addDefaultTreemapChartOptions(option, data, customizationOptions.rootName);
        break;
      case 'stacked-bar':
        option = this.addDefaultStackedBarOptions(option, data, customizationOptions.numberOfBars);
        break;
      case 'bar':
      case 'line':
        if (percentage) option = this.addDefaultPercentageOptions(option, data);
    }

    this.chart.setOption(option);

    this.addResizeTriggers(resizeTriggerElement);
  }

  private addDefaultOption(data: (any|number)[], title: string, option: any): any {
    option.dataset = { source: data };
    option.grid = { containLabel: true };
    option.textStyle = { fontFamily: 'Verdana, sans-serif', fontsize: 20 };
    option.title = { text: title };
    option.tooltip = {trigger: 'axis'};
    return option;
  }

  private addResizeTriggers(elementId: string) {
    this.jQuery(elementId).bind('shown.bs.collapse', () => {
      this.chart.resize();
    });

    this.jQuery(this.window).on('resize', () => {
      if (this.chart != null && this.chart !== undefined) {
        this.chart.resize();
      }
    });
  }

  private addDefaultPercentageOptions(options: any, data: (any|number)[]): any {
    const total: number = data.reduce((a, b) => a + b[1], 0)

    options.tooltip = {
      trigger: 'axis',
      formatter(params: any) : string {
        const name = params[0].value[0];
        const value = params[0].value[1];
        const percentage: number = total === 0 ? 0 : value / total * 100

        return `<b>${name}:</b> ${value} (${percentage.toFixed(2)}%)`
      }
    };
    return options;
  }

  private addDefaultTreemapChartOptions(options: any, data: (any|number)[], rootName: string): any {
    options.label = { fontsize: 16 }
    options.series = [{
      type: 'treemap',
      name: rootName,
      data
    }];
    options.tooltip = {
      formatter(params: any) {
        return params.data.fullname + ": " + params.data.value
      }
    };
    return options;
  }

  private addDefaultPieChartOptions(option: any, data: (any|number)[]): any {
    option.legend = {
      type: 'scroll',
      orient: 'vertical',
      right: 10,
      top: 20,
      bottom: 20,
      data: data.map((datum) => { return datum.name; }),
    };
    option.series = [{
      type: 'pie',
      radius: "55%",
      center: ['40%', '50%'],
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)'
        }
      }
    }];
    option.tooltip = {
      trigger: 'item',
      formatter: (params: any) => '<b>' + params.data.name + ':</b> ' + params.data.value + ' (' + params.percent + '%)<br>'
    };

    return option;
  }

  private addDefaultStackedBarOptions(option: any, data: (any|number)[], numberOfBars: number): any {
    option.series = []
    for (let i = 0; i < numberOfBars; i++) {
      option.series.push({
        type: 'bar',
        stack: '1',
        label: {
          normal: {
            show: true,
            position: 'insideLeft',
            formatter: (params: {[key: string]: any}) => {
              if (params.data[i + 1] === 0) return ''
              else return data[1][params.dataIndex][i + 1] + ' (' + params.data[i + 1].toFixed(2) + '%)'
            }
          }
        }
      });
    }
    option.legend.data = data[0].slice(1)
    option.dataset = { source: [data[0]].concat(data[2]) };
    option.tooltip = {
      trigger: 'axis',
      axisPointer : {
        type : 'shadow'
      },
      formatter: (params: any[]) => {
        const seriesCircle = (color: string) => '<div class="rounded-circle" style="display:inline-block;width:10px;height:10px;background-color:' + color + '"></div>';
        let html = '<b>' + params[0].axisValue + '</b><br>';
        let sum = 0;
        params.forEach((item: {[key: string]: any}) => {
          sum += data[1][item.dataIndex][item.seriesIndex + 1];
          html += seriesCircle(item.color) + ' ' + item.seriesName + ': ' + data[1][item.dataIndex][item.seriesIndex + 1] + ' (' + item.data[item.seriesIndex + 1].toFixed(2) + '%)<br>';
        })
        html += 'Total: ' + sum;
        return html;
      }
    };
    return option;
  }
}
