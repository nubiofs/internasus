export class GeneralData {
  private jQuery: any;

  constructor(jQuery: any) {
    this.jQuery = jQuery;
  }

  contentSelectFor(category: string, select: any): void {
    const tableId = `#${category}-${select.value}`;

    this.jQuery(`[id^="${category}"]`).css('display', 'none');
    this.jQuery(tableId).css('display', 'block');
  }

  dynamicChartFor(selected: string, data: any, chart: any): void {
    chart.echarts.dispose(document.getElementById('dynamic-chart'));
    chart.createChart(
      'dynamic-chart', '',
      this.processChartData(data[selected]),
      data[selected][`chart_type`],
      this.processCustomizationOptions(data[selected]),
      '#collapseGeneralDatadynamic'
    );
  }

  processChartData(dataSet: any): any {
    const chartData = [];
    switch(dataSet[`chart_type`]) {
      case 'pie': {
        for (const key of Object.keys(dataSet[`data`])) {
          chartData.push({name: key, value: dataSet[`data`][key]});
        }
        break;
      }
      case 'treemap': {
        for (let i = 0; i < 26; i++) {
          chartData.push({
            name: String.fromCharCode(i + 65),
            children: []
          });
          for (let j = 0; j < 10; j++) {
            chartData[i].children.push({
              name: String.fromCharCode(i + 65, j + 48),
              children: []
            });
          }
        }
        for (const key of Object.keys(dataSet[`data`])) {
          if (!key) continue;
          chartData[key.charCodeAt(0) - 65].children[key.charAt(1)].children.push({
            fullname: key,
            name: key.slice(0, 3),
            value: dataSet[`data`][key]
          });
        }
        break;
      }
      default: {
        for (const key of Object.keys(dataSet[`data`])) {
          chartData.push([key, dataSet[`data`][key]]);
        }
      }
    }
    return chartData;
  }

  healthFacilitiesChartFor(selectedId: string, chartName: string, dataSet: any, chart: any): void {
    chart.echarts.dispose(document.getElementById('health-facility-specialty-distances-chart'));
    const selectedName = Object.keys(dataSet.data)[selectedId];
    chart.createChart(
      'health-facility-specialty-distances-chart',
      chartName,
      this.processHealthFacilityData(dataSet.data[selectedName]),
      'stacked-bar',
      this.processCustomizationOptions(dataSet.options),
      '#collapseGeneralDatahealth_facilities_specialty_distances'
      );
    }

  processHealthFacilityData(data: any[]): any {
    const series = ['Especialidade', '< 1km', '> 1km e < 5km', '> 5km e < 10km', '> 10km'];
    const chartData: [string[], (string|number)[][], (string|number)[][]] = [series, [], []];
    const specialties = Object.keys(data[0]).reverse();
    for (const key of specialties) {
      chartData[1].push([key].concat(data[0][key]));
      chartData[2].push([key].concat(data[1][key]));
    }
    return chartData;
  }

  processCustomizationOptions(dataSet: any): any {
    const options: {[key: string]: any} = {};

    if (dataSet[`xAxis`]) options.xAxis = dataSet[`xAxis`];
    if (dataSet[`yAxis`]) options.yAxis = dataSet[`yAxis`];
    if (dataSet[`xAxisType`]) options.xAxis = {type: dataSet[`xAxisType`]};
    if (dataSet[`yAxisType`]) options.yAxis = {type: dataSet[`yAxisType`]};
    if (dataSet[`xZoom`]) options.dataZoom = [{id: 'dataZoomX', type: 'slider', xAxisIndex: [0], filterMode: 'filter'}];
    if (dataSet[`yZoom`]) options.dataZoom.push({id: 'dataZoomY', type: 'slider', yAxisIndex: [0], filterMode: 'empty'});
    if (dataSet[`rootName`]) options.rootName = dataSet[`rootName`];
    if (dataSet[`legend`]) options.legend = dataSet[`legend`]
    if (dataSet[`numberOfBars`]) options.numberOfBars = dataSet[`numberOfBars`];

    return options;
  }
}
