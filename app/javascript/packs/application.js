// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

import 'bootstrap'
import 'bootstrap-select'
import 'bootstrap-slider'
import 'bootstrap-datepicker'
import 'bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR'

import 'leaflet/dist/leaflet.css';
import * as L from 'leaflet';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';
import 'leaflet.markercluster/dist/leaflet.markercluster.js';
import 'leaflet.heat/dist/leaflet-heat.js';
import 'leaflet-minimap/dist/Control.MiniMap.min.js'
import 'leaflet-minimap/dist/Control.MiniMap.min.css'

// FIX Leaflet default icons with webpack
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

import html2canvas from 'html2canvas'

import './src/application.scss'

import { GeneralData } from './general_data/GeneralData';
window.GeneralData = GeneralData

import { ChartGenerator } from './general_data/ChartGenerator';
window.ChartGenerator = ChartGenerator

import echarts from 'echarts'
window.echarts = echarts

window.html2canvas = html2canvas;
window.jQuery = $;
window.$ = $;
