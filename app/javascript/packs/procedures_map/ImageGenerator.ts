export class ImageGenerator {
  private html2canvas: any;
  private jQuery: any;
  private form: any;
  private imageInput: any;

  constructor(html2canvas: any, jQuery: any, formId: string, imageInputId: string) {
    this.html2canvas = html2canvas;
    this.jQuery = jQuery;

    this.form = this.jQuery(`#${formId}`);
    this.imageInput = this.jQuery(`#${imageInputId}`);
  }

  html2canvasWrapper(node: any, callback: (canvas: any) => void): void {
    const child = node.querySelector("[style*='display: block']")
    if (child != null) child.style.removeProperty('height');

    this.html2canvas(node, {
      allowTaint: true,
      useCORS: true,
      scrollX: 0,
      scrollY: -window.scrollY
    }).then(callback);
  }

  generate(targetId: string): void {
    const target = this.jQuery(`#${targetId}`)[0];

    this.html2canvasWrapper(target, (canvas) => this.generationCallback(canvas));
  }

  generationCallback(canvas: any) {
    const img = canvas.toDataURL('image/png');

    this.imageInput.val(img);
    this.form.submit();
  }
}

window.ImageGenerator = ImageGenerator;
