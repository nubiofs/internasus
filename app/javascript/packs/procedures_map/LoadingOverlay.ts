export class LoadingOverlay {
  private jQuery: any;

  constructor(jQuery: any) {
    this.jQuery = jQuery;
    jQuery('.overlay-button').click(() => this.dimScreen());
  }

  dimScreen() {
    this.jQuery('#loading-overlay').modal('show');
  }
}

window.LoadingOverlay = LoadingOverlay;
