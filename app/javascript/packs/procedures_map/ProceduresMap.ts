export class ProceduresMap {
  private map: any;
  private patientMarkers: any[];
  private patientsDataCluster: any;
  private Leaflet: any;
  private jQuery: any;
  private heatmapLayer: any;
  private minimap: any;
  private limitsLayers: {};
  private cSWHeatmapLayer: any;
  public heatmapGradients = {
    default: {
      0.25: "#2bffd3",
      0.62: "#fffd57",
       1.0: "#f93434"
    },
    highContrast: {
      0.25: "#D3C9F8",
      0.55: "#7B5CEB",
      0.85: "#4E25E4",
       1.0: "#3816B3"
    }
  };
  private healthFacilitiesLayer: any;
  private healthFacilitiesPercentilesLayer: any;
  private percentileColors = {
    75: '#FF4444',
    50: '#44FF44',
    25: '#4444FF',
  };
  private heatmapIntensity: number;

  constructor(divId: string, Leaflet: any, jQuery: any, datasetProportion: number) {
    this.Leaflet = Leaflet;
    this.jQuery = jQuery;

    this.map = this.Leaflet.map(divId, {
      center: [-23.557296, -46.669211],
      zoom: 11,
      maxZoom: 18
    });

    this.map.on('zoom', () => {
      this.heatmapRadiusEvent($('#heat_map_radius')[0]);
    });

    const osmUrl = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png';
    const osmAttribution =
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'

    this.Leaflet.tileLayer(osmUrl,
                           { attribution: osmAttribution }).addTo(this.map);

    const minimapLayer = this.Leaflet.tileLayer(osmUrl, { attribution: osmAttribution });
    this.minimap = this.Leaflet.control.minimap(
      minimapLayer, { toggleDisplay: true }
    ).addTo(this.map);

    this.patientMarkers = [];
    this.patientsDataCluster = this.Leaflet.markerClusterGroup();
    this.heatmapLayer = this.Leaflet.heatLayer([]);
    this.heatmapLayer.setOptions({
      gradient: this.heatmapGradients.default
    });
    this.limitsLayers = {};
    this.cSWHeatmapLayer = this.Leaflet.heatLayer([]); // Census Sector Weighted Heatmap Layer
    this.cSWHeatmapLayer.setOptions({
      gradient: this.heatmapGradients.default
    });
    this.healthFacilitiesLayer = this.Leaflet.layerGroup([]);
    this.healthFacilitiesPercentilesLayer = this.Leaflet.layerGroup([]);

    this.heatmapIntensity = 1.0 + 9.0*(1.0 - datasetProportion); // value in the [1, 10] interval
  }

  addHealthFacility(imageUrl: string, latitude: number, longitude: number, popupUrl: string, errorMessage: string) {
    const icon = this.Leaflet.icon({
      iconUrl: imageUrl,
      iconAnchor: [25, 0],
      className: 'health_facility_icon'
    });

    const marker = this.Leaflet.marker(
      [latitude, longitude],
      { icon }
    );

    marker.bindPopup((popup) => {
      return this.getPopupContent(popupUrl, popup, errorMessage);
    });

    marker.addTo(this.healthFacilitiesLayer);
  }

  setPopupContent(element: any, popup: any, content: string) {
    element.html(content);
    popup.update();
  }

  getPopupContent(url: string, popup: any, errorMessage: string) {
    const el = this.jQuery('<div/>');
    el.css('width', '300px');

    this.jQuery.ajax({
      url,
      dataType: 'html',
      data: ''
    })
    .done((data) => { this.setPopupContent(el, popup, data); })
    .fail((error) => { this.setPopupContent(el, popup, errorMessage); });

    return el[0];
  }

  createPatientDataMarker(latitude: number, longitude: number, popupUrl: string, errorMessage: string) {
    const marker = this.Leaflet.marker([latitude, longitude]);
    this.heatmapLayer.addLatLng([latitude, longitude, this.heatmapIntensity]);

    marker.bindPopup((popup) => {
      return this.getPopupContent(popupUrl, popup, errorMessage);
    });

    this.patientMarkers.push(marker);
  }

  invalidateSize() {
    this.map.invalidateSize();
  }

  generateClusterLayer(markers: any[], radius: number): any {
    const clusterGroup = this.Leaflet.markerClusterGroup({
      maxClusterRadius: radius
    });

    markers.forEach((marker) => { clusterGroup.addLayer(marker) });

    return clusterGroup;
  }

  toggleCluster() {
    if(this.map.hasLayer(this.patientsDataCluster)) {
      this.map.removeLayer(this.patientsDataCluster);
    } else {
      this.map.addLayer(this.patientsDataCluster);
    }
  }

  kilometresToPixels(kilometres:number): number {
    const latitude = -23.557296000000001;
    const c = 40075016.686;
    const toRad = Math.PI / 180;
    const zoom = this.map.getZoom();
    const metres = kilometres * 1e3;

    const scale = c * Math.abs(Math.cos(latitude * toRad)) / Math.pow(2, zoom + 8);

    return Math.ceil(metres / scale);
  }

  applyPatientCluster(radius: number = 80): void {
    const patientsCluster = this.generateClusterLayer(this.patientMarkers, radius);

    if(this.map.hasLayer(this.patientsDataCluster)) {
      this.toggleCluster()
      this.patientsDataCluster = patientsCluster;
      this.toggleCluster()
    } else {
      this.patientsDataCluster = patientsCluster;
    }
  }

  clusterRadiusEvent(slider: any): void {
    const radiusInKilometers = slider.value;
    const radiusInPixels = this.kilometresToPixels(radiusInKilometers);

    this.applyPatientCluster(radiusInPixels);
  }

  toggleHeatmap() {
    if(this.map.hasLayer(this.heatmapLayer)) {
      this.map.removeLayer(this.heatmapLayer);
    } else {
      this.map.addLayer(this.heatmapLayer);
    }
  }

  heatmapRadiusEvent(slider: any): void {
    const radiusPixels = this.kilometresToPixels(Number(slider.value));

    this.heatmapLayer.setOptions({
      radius: radiusPixels
    });
    this.cSWHeatmapLayer.setOptions({
      radius: radiusPixels
    });
  }

  toggleHeatmapHighContrast(): void {
    const highContrastEnabled =
      this.heatmapLayer.options.gradient[1] === this.heatmapGradients.default[1];

    const index = highContrastEnabled? "highContrast" : "default"

    const agradient = this.heatmapGradients[index];

    this.heatmapLayer.setOptions({
      gradient: agradient
    });

    this.cSWHeatmapLayer.setOptions({
      gradient: agradient
    });
  }

  heatmapOpacityEvent(slider: any): void {
    this.jQuery('.leaflet-heatmap-layer').css('opacity', slider.value);
  }

  buildLimitsLayer(identifier: string, geoJSON: any): void {
    const style = {
        'color': '#444444',
        'opacity': 0.6,
        'stroke': true,
        'fill': true,
        'fillOpacity': 0.05,
    };

    this.limitsLayers[identifier] = this.Leaflet.geoJSON(geoJSON, { style });
  }

  toggleLimits(identifier: string): void {
    const limitsLayer = this.limitsLayers[identifier];

    if(limitsLayer !== undefined) {
      if(this.map.hasLayer(limitsLayer)) {
        this.map.removeLayer(limitsLayer);
      } else {
        this.map.addLayer(limitsLayer);
      }
    }
  }

  createCSWHMarker(latitude: number, longitude: number, rate: number) { // Create Census Sector Weighted Heatmap Marker
    this.cSWHeatmapLayer.addLatLng([latitude, longitude, this.heatmapIntensity*rate]);
  }

  toggleCSWHeatmap() { // Toggle Census Sector Weighted Heatmap
    if(this.map.hasLayer(this.cSWHeatmapLayer)) {
      this.map.removeLayer(this.cSWHeatmapLayer);
    } else {
      this.map.addLayer(this.cSWHeatmapLayer);
    }
  }

  toggleHealthFacilities() {
    if(this.map.hasLayer(this.healthFacilitiesLayer)) {
      this.map.removeLayer(this.healthFacilitiesLayer);
    } else {
      this.map.addLayer(this.healthFacilitiesLayer);
    }
  }

  addHealthFacilityPercentile(latitude: number, longitude: number, percentile: number, radius: number, labels: string[]) {
    const circle = this.Leaflet.circle(
      [
        latitude,
        longitude,
      ],
      {
        color: this.percentileColors[percentile],
        fillColor: this.percentileColors[percentile],
        fillOpacity: 0.2,
        radius: radius * 1000
      }
    );

    circle.bindTooltip(
      `<b>${labels[0]}</b>: ${percentile}%<br><b>${labels[1]}</b>: ${radius} Km`,
      {direction:'top'}
    );
    circle.addTo(this.healthFacilitiesPercentilesLayer);
  }

  toggleHealthFacilitiesPercentiles() {
    if(this.map.hasLayer(this.healthFacilitiesPercentilesLayer)) {
      this.map.removeLayer(this.healthFacilitiesPercentilesLayer);
    } else {
      this.map.addLayer(this.healthFacilitiesPercentilesLayer);
    }
  }
}

window.ProceduresMap = ProceduresMap;
