export class LimitsFeatures {
  private features: {};

  constructor() {
    this.features = {};
  }

  addFeature(featureCollectionIdentifier: string, name: string, geometry: any): void {
    if(this.features[featureCollectionIdentifier] === undefined) {
      this.features[featureCollectionIdentifier] = [];
    }

    this.features[featureCollectionIdentifier] = this.features[featureCollectionIdentifier].concat({
      'type': 'Feature',
      'properties': { name },
      geometry,
    });
  }

  getFeatureCollection(identifier: string) {
    if(this.features[identifier] === undefined) {
      return undefined;
    }

    return {
      'type': 'FeatureCollection',
      'features': this.features[identifier],
    };
  }
}

window.LimitsFeatures = LimitsFeatures;
