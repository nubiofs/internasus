# frozen_string_literal: true

module DistancesConcern
  extend ActiveSupport::Concern

  def distances_hash
    {
      distances_buckets: distances_buckets,
      specialties_means: specialties_means,
      facility_type_means_per_specialty: facility_type_means_per_specialty
    }
  end

  private

  def distances_buckets
    count(
      HospitalizationDatum,
      'CASE WHEN distance <= 1 THEN 0'\
        'WHEN distance <= 5 THEN 1'\
        'WHEN distance <= 10 THEN 2 ELSE 3 END'
    ).sort.to_h
  end

  def specialties_means
    data = select_distinct_and_order([:specialty, 'AVG(distance) AS average_distance'], { specialty: :desc })
    data = group_by_specialties(data)
    defaults_hash = defaults_hash_from(HospitalizationDatum, { specialty: :desc }, :specialty, 0)
    truncate_averages(data).reverse_merge(defaults_hash)
  end

  def facility_type_means_per_specialty
    facility_type_distances_hash = mean_distances_per_specialty_and_facility_type

    specialties_defaults = defaults_hash_from(HospitalizationDatum, :specialty, :specialty, {})
    facility_type_distances_hash.reverse_merge!(specialties_defaults)

    facility_type_defaults = defaults_hash_from(HealthFacility, { facility_type: :desc }, :facility_type, 0)
    facility_type_distances_hash.map { |v| v.second.reverse_merge!(facility_type_defaults) }

    facility_type_distances_hash
  end

  def select_distinct_and_order(columns, order)
    HospitalizationDatum.select(columns).where(procedure_id: procedures_ids).distinct.order(order)
  end

  def group_by_specialties(data)
    data.group(:specialty).group_by(&:specialty)
  end

  def join_and_group_health_facilities(data)
    data.joins(:health_facility).group('health_facilities.facility_type')
  end

  def truncate_averages(data)
    data.transform_values! do |tuple|
      tuple.first.average_distance.to_d.truncate(2)
    end
  end

  def truncate_health_facilities_averages(data)
    data.transform_values! do |grouped_health_facilities|
      truncate_averages(grouped_health_facilities.group_by(&:health_facility_type))
    end
  end

  def defaults_hash_from(data, order, column, value)
    data.distinct.order(order).pluck(column).to_h { |v| [v, value] }
  end

  def mean_distances_per_specialty_and_facility_type
    data = select_distinct_and_order(
      [:specialty, 'health_facilities.facility_type AS health_facility_type', 'AVG(distance) AS average_distance'],
      :specialty
    )
    data = join_and_group_health_facilities(data)
    data = group_by_specialties(data)
    truncate_health_facilities_averages(data)
  end
end
