# frozen_string_literal: true

module TranslationsConcern
  extend ActiveSupport::Concern

  def translations_for(data_hash)
    data_hash.each_pair do |category, data|
      data_hash[category] = send("#{category}_translation", data)
    rescue NoMethodError
      next
    end
  end

  def age_codes_translation(age_codes_data)
    age_codes_data.transform_keys { |key| patient_attribute_value('age_code', key) }
  end

  alias ages_ordered_by_code_translation age_codes_translation

  def genders_translation(gender_data)
    gender_data.transform_keys { |key| patient_attribute_value('gender', key) }
  end

  def icd_categories_translation(icd_data)
    icd_translations(icd_data, IcdCategory)
  end

  def specialties_translation(specialties_data)
    specialties_data.transform_keys { |key| I18n.t(key, scope: 'hospitalization_datum.specialty') }
  end

  def hospitalization_types_translation(types_data)
    types_data.transform_keys { |key| I18n.t("hospitalization_type_#{key}", scope: 'hospitalization_datum') }
  end

  def races_translation(races_data)
    races_data.transform_keys { |key| patient_attribute_value('race', key) }
  end

  def historic_series_translation(historic_series_data)
    historic_series_data.transform_keys { |key| I18n.l(key, format: '%b %Y') }
  end

  def complexities_translation(complexities_data)
    complexities_data.transform_keys { |key| I18n.t(key, scope: 'hospitalization_datum.complexity') }
  end

  def financing_types_translation(financing_types_data)
    financing_types_data.transform_keys { |key| I18n.t(key, scope: 'hospitalization_datum.financing') }
  end

  def icd_secondary_categories_translation(icd_data)
    icd_translations(icd_data, IcdSubcategory)
  end

  alias icd_secondary_2_categories_translation icd_secondary_categories_translation

  def icd_translations(icd_data, icd_class)
    icd_data.transform_keys { |key| icd_class.where(id: key).pluck(:code, :description).join(' - ') }
  end

  def patient_attribute_value(attribute, value)
    I18n.t("patient_datum.#{attribute}_#{value}")
  end

  def health_facilities_specialty_distances_translation(health_facilities_specialty_distances)
    health_facilities_specialty_distances[:data].each_value do |health_facility|
      health_facility.each do |data|
        data.transform_keys! { |key| translate_category_name(key) }
      end
    end
    health_facilities_specialty_distances
  end

  def distances_buckets_translation(distances_buckets)
    distances_buckets.transform_keys { |key| translate_range_bucket(key) }
  end

  alias specialties_means_translation specialties_translation
  alias facility_type_means_per_specialty_translation specialties_translation

  def educational_levels_translation(educational_levels_data)
    educational_levels_data.transform_keys { |key| patient_attribute_value('educational_level', key) }
  end

  private

  def translate_category_name(category)
    if category == :total
      I18n.t(category, scope: 'general_data.health_facilities_specialty_distances')
    else
      I18n.t(category, scope: 'hospitalization_datum.specialty')
    end
  end

  def translate_range_bucket(bucket)
    I18n.t("group_#{bucket}", scope: 'general_data.distance')
  end
end
