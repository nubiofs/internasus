# frozen_string_literal: true

module DescriptiveStatisticsConcern
  extend ActiveSupport::Concern

  def descriptive_statistics_for(data_hash)
    descriptive_statistics_hash = {}
    data_hash.each_key do |category|
      descriptive_statistics_hash[category] = send("#{category}_list")
    rescue NoMethodError
      next
    end
    descriptive_statistics_hash
  end

  def descriptive_statistics_hash(list)
    list.extend(DescriptiveStatistics)
    {
      size: list.size, sum: list.sum.to_d.truncate(2), min: list.min, max: list.max,
      mean: list.mean.to_d.truncate(2), standard_deviation: list.standard_deviation.to_d.truncate(2)
    }
  end

  def age_year_list
    list = PatientDatum.where(procedure_id: procedures_ids).pluck(:age_year)
    descriptive_statistics_hash(list)
  end

  def hospitalization_days_list
    list = HospitalizationDatum.where(procedure_id: procedures_ids).pluck(:hospitalization_days)
    descriptive_statistics_hash(list)
  end

  def hospitalization_rates_list
    list = HospitalizationDatum.where(procedure_id: procedures_ids).pluck(:hospitalization_rates)
    descriptive_statistics_hash(list)
  end

  def uti_rates_list
    list = HospitalizationDatum.where(procedure_id: procedures_ids).pluck(:uti_rates)
    descriptive_statistics_hash(list)
  end

  def ui_rates_list
    list = HospitalizationDatum.where(procedure_id: procedures_ids).pluck(:ui_rates)
    descriptive_statistics_hash(list)
  end

  def service_values_list
    list = HospitalizationDatum.where(procedure_id: procedures_ids).pluck(:service_value)
    descriptive_statistics_hash(list)
  end
end
