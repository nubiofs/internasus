# frozen_string_literal: true

module ChartsConfigurationsConcern
  extend ActiveSupport::Concern

  RANKING_KEYS = %i[health_facilities_ranking administrative_sectors age_codes genders icd_categories].freeze
  TREEMAP_INFO = { chart_type: 'treemap', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false,
                   rootName: I18n.t('procedures', scope: 'general_data.dynamic') }.freeze
  HEALTH_FACILITIES_SPECIALTY_DISTANCES_INFO = {
    xAxis: { type: 'value', max: 100, axisLabel: { formatter: '{value}%' } },
    yAxis: { type: 'category', axisLabel: { interval: 0 } },
    legend: { right: 20, bottom: 10 }, numberOfBars: 4, xZoom: false, yZoom: false
  }.freeze
  DYNAMIC_CHARTS_INFO = {
    health_facilities: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category', xZoom: true, yZoom: true },
    competences: { chart_type: 'line', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false },
    hospitalization_groups: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category', xZoom: true, yZoom: true },
    specialties: { chart_type: 'pie', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false },
    hospitalization_types: { chart_type: 'pie', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false },
    icd_categories: TREEMAP_INFO,
    icd_secondary_categories: TREEMAP_INFO,
    icd_secondary_2_categories: TREEMAP_INFO,
    complexities: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: false, yZoom: false },
    financing_types: { chart_type: 'pie', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false },
    ages_ordered_by_code: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category', xZoom: true, yZoom: true },
    races: { chart_type: 'pie', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false },
    educational_levels: { chart_type: 'pie', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false },
    administrative_sectors: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category', xZoom: true, yZoom: true },
    subprefectures: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category', xZoom: true, yZoom: true },
    technical_health_supervisions: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category',
                                     xZoom: true, yZoom: true },
    regional_health_coordinations: { chart_type: 'bar', xAxisType: 'value', yAxisType: 'category',
                                     xZoom: true, yZoom: true },
    administrations: { chart_type: 'pie', xAxisType: '', yAxisType: '', xZoom: false, yZoom: false },
    hospitalization_days: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false },
    uti_rates: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false },
    ui_rates: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false },
    hospitalization_rates: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false },
    service_values: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false },
    distances: { chart_type: 'bar', xAxisType: 'category', yAxisType: 'value', xZoom: true, yZoom: false }
  }.freeze
end
