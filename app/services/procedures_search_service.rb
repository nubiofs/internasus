# frozen_string_literal: true

class ProceduresSearchService
  # I will not freeze these so I can keep tests creating mocks on it
  # rubocop:disable Style/MutableConstant
  DATE_FIELDS = %w[admission_date_gteq leave_date_lteq]
  ARRAY_FIELDS = %w[health_facility_administration_cont_any competence_in hospitalization_group_in]
  HUMAN_ATTR_FIELDS = {
    'hospitalization_type_in' => %w[hospitalization_datum hospitalization_type _],
    'complexity_in' => %w[hospitalization_datum complexity .],
    'specialty_in' => %w[hospitalization_datum specialty .],
    'financing_in' => %w[hospitalization_datum financing .],
    'patient_datum_gender_eq_any' => %w[patient_datum gender _],
    'patient_datum_age_code_cont_any' => %w[patient_datum age_code _],
    'patient_datum_race_cont_any' => %w[patient_datum race _],
    'patient_datum_educational_level_in' => %w[patient_datum educational_level _]
  }
  NAME_FIELDS = {
    'health_facility_id_in' => HealthFacility,
    'health_facility_administrative_sector_id_in' => AdministrativeSector,
    'health_facility_subprefecture_id_in' => Subprefecture,
    'health_facility_technical_health_supervision_id_in' => TechnicalHealthSupervision,
    'health_facility_regional_health_coordination_id_in' => RegionalHealthCoordination,
    'patient_datum_administrative_sector_id_in' => AdministrativeSector,
    'patient_datum_subprefecture_id_in' => Subprefecture,
    'patient_datum_technical_health_supervision_id_in' => TechnicalHealthSupervision,
    'patient_datum_regional_health_coordination_id_in' => RegionalHealthCoordination
  }
  ICD_FIELDS = { 'icd_category_id_in' => IcdCategory, 'icd_subcategory_id_in' => IcdSubcategory }
  RANGES_FIELDS = {
    'health_facility_beds_between' => "0,#{HealthFacility.maximum('beds')}",
    'hospitalization_days_between' => "0,#{HospitalizationDatum.maximum('hospitalization_days')}",
    'hospitalization_rates_between' => "0,#{HospitalizationDatum.maximum('hospitalization_rates')}",
    'uti_rates_between' => "0,#{HospitalizationDatum.maximum('uti_rates')}",
    'ui_rates_between' => "0,#{HospitalizationDatum.maximum('ui_rates')}",
    'service_value_between' => "0,#{HospitalizationDatum.maximum('service_value')}",
    'distance_between' => "0,#{HospitalizationDatum.maximum('distance')}"
  }
  # rubocop:enable Style/MutableConstant
  ALL_FIELDS = DATE_FIELDS + ARRAY_FIELDS + HUMAN_ATTR_FIELDS.keys + NAME_FIELDS.keys +
               ICD_FIELDS.keys + RANGES_FIELDS.keys

  class << self
    def empty
      [HealthFacility.all, PatientDatum.none]
    end

    def not_empty(search)
      hospitalization_data = search.result.includes(:health_facility, :patient_datum)

      health_facilities_ids = hospitalization_data.distinct(:health_facility_id).select(:health_facility_id)
      procedures_ids = hospitalization_data.select(:procedure_id)
      health_facilities = HealthFacility.where(id: health_facilities_ids)
      patients_data = PatientDatum.select(:id, :latitude, :longitude, :procedure_id, :updated_at)
                                  .where(procedure_id: procedures_ids)
      dataset_proportion = hospitalization_data.count / HospitalizationDatum.count.to_f

      [health_facilities, patients_data, procedures_ids, dataset_proportion]
    end

    def search(query_params)
      search = HospitalizationDatum.ransack(query_params)
      query_params.nil? ? [search] + empty + [[], 0.0] : [search] + not_empty(search)
    end

    def active_filters(query_params)
      active_filters = {}
      return active_filters if query_params.nil?

      ALL_FIELDS.each do |field|
        value = query_params[field]

        RANGES_FIELDS[field] == value && value = []
        value.reject!(&:empty?) if value.is_a?(Array)

        active_filters[field] = format_active_filter(field, value) unless value.empty?
      end

      active_filters
    end

    def format_active_filter(field, value)
      %w[date array human_attr name icd ranges].each do |prefix|
        if ProceduresSearchService.const_get("#{prefix.upcase}_FIELDS").include? field
          return send("#{prefix}_format", field, value)
        end
      end
      value
    end

    private

    def date_format(_, value)
      Date.parse(value).strftime('%d/%m/%Y')
    end

    def array_format(_, value)
      value.join('; ')
    end

    def human_attr_format(field, value)
      value.map { |val| human_attribute_value(*HUMAN_ATTR_FIELDS[field], val) }.join('; ')
    end

    def name_format(field, value)
      value.map { |id| NAME_FIELDS[field].find(id).name }.join('; ')
    end

    def icd_format(field, value)
      value.map { |id| icd_value(ICD_FIELDS[field], id) }.join('; ')
    end

    def ranges_format(_, value)
      value.gsub(',', ' - ')
    end

    def human_attribute_value(model, attribute, separator, value)
      I18n.t("#{model}.#{attribute}#{separator}#{value}")
    end

    def icd_value(model, id)
      model.where(id: id).pluck(:code, :description).join(' - ')
    end
  end
end
