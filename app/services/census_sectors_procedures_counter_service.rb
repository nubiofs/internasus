# frozen_string_literal: true

class CensusSectorsProceduresCounterService
  GENDER_FILTERS = %i[women men].freeze
  RACE_FILTERS = %i[black mulatto native white yellow].freeze
  ALL_FILTERS = GENDER_FILTERS + RACE_FILTERS

  def initialize(procedures_ids)
    self.filtered_procedures = PatientDatum.where(procedure_id: procedures_ids)

    self.total_procedures_by_census_sector = filtered_procedures
                                             .group(:census_sector)
                                             .count
    self.filters = {}
    GENDER_FILTERS.each { |filter| filters[filter] = true }
    RACE_FILTERS.each { |filter| filters[filter] = false }
  end

  def disable_all_race_filters
    RACE_FILTERS.each { |filter| filters[filter] = false }
  end

  def disable_all_gender_filters
    GENDER_FILTERS.each { |filter| filters[filter] = false }
  end

  def set_filter(type, value)
    filters[type] = value

    disable_all_race_filters if value && GENDER_FILTERS.include?(type)
    disable_all_gender_filters if value && RACE_FILTERS.include?(type)
  end

  def census_sector_filtered_total_population(census_sector)
    total = 0

    filters.filter { |_, enabled| enabled }.each_key do |filter|
      total += census_sector.send("#{filter}_population")
    end

    total
  end

  def census_sector_counts(census_sector)
    total_population = census_sector_filtered_total_population(census_sector)
    total_procedures = total_procedures_by_census_sector[census_sector]
    total_rate = total_procedures / total_population.to_f

    {
      total_population: total_population,
      total_procedures: total_procedures,
      total_rate: total_rate
    }
  end

  def counts
    census_sectors_counts = {}

    total_procedures_by_census_sector.each_key do |census_sector|
      census_sectors_counts[census_sector] = census_sector_counts(census_sector)
    end

    census_sectors_counts
  end

  def census_sector_counts_by(attribute)
    filtered_procedures
      .group(:census_sector_id, attribute)
      .count
      .group_by { |cs_id_and_attribute, _count| cs_id_and_attribute.first }
      .transform_values do |array_and_count_pair|
        array_and_count_pair.map { |array| [array.first.last, array.last] }.to_h
      end
  end

  def census_sector_statistics
    statistics = census_sector_counts_by :race
    by_gender = census_sector_counts_by :gender
    ids = (statistics.keys + by_gender.keys).uniq

    statistics.each { |k, v| v.merge!(by_gender[k]) }
    statistics.merge({
                       total: total_procedures_by_census_sector,
                       ids: ids
                     })
  end

  private

  attr_accessor :total_procedures_by_census_sector, :filters, :filtered_procedures
end
