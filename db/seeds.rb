# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'
require 'securerandom'

# ICD categories
icd_categories = []
CSV.foreach(Rails.root.join('vendor', 'cid10', 'CID-10-CATEGORIAS.CSV'), quote_char: '|', col_sep: ';', headers: true) do |icd10_row|
  icd_categories << { code: icd10_row['CAT'], description: icd10_row['DESCRICAO'], created_at: Time.now, updated_at: Time.now }
end
IcdCategory.insert_all(icd_categories)

# ICD subcategories
category = nil
icd_subcategories = []
CSV.foreach(Rails.root.join('vendor', 'cid10', 'CID-10-SUBCATEGORIAS.CSV'), quote_char: '$', col_sep: ';', headers: true) do |icd10_row|
  category_code = icd10_row['SUBCAT'][0..2]
  category = IcdCategory.find_by(code: category_code) if category&.code != category_code

  icd_subcategories << { code: icd10_row['SUBCAT'], description: icd10_row['DESCRICAO'], icd_category_id: category.id, created_at: Time.now, updated_at: Time.now }
end
IcdSubcategory.insert_all(icd_subcategories)

# ICD groups
CSV.foreach(Rails.root.join('vendor', 'cid10', 'CID-10-GRUPOS.CSV'), quote_char: '$', col_sep: ';', headers: true) do |icd10_row|
  group = IcdGroup.new(description: icd10_row['DESCRICAO'])
  group.icd_categories = IcdCategory.where(
    "code >= :first_category AND code <= :last_category",
    { first_category: icd10_row['CATINIC'], last_category: ['CATFIM'] }
  )
  group.save!
end

# ICD chapters
CSV.foreach(Rails.root.join('vendor', 'cid10', 'CID-10-CAPITULOS.CSV'), quote_char: '$', col_sep: ';', headers: true) do |icd10_row|
  chapter = IcdChapter.new(code: icd10_row['NUMCAP'], description: icd10_row['DESCRICAO'])
  chapter.icd_categories = IcdCategory.where(
    "code >= :first_category AND code <= :last_category",
    { first_category: icd10_row['CATINIC'], last_category: ['CATFIM'] }
  )
  chapter.save!
end

# HealthFacilities, AdministrativeSectors, RegionalHealthCoordination, Subprefecture, TechnicalHealthSupervision
CSV.foreach(Rails.root.join('vendor', 'pmsp_health_centres.csv'), quote_char: '$', col_sep: ',', headers: true) do |health_facility|
  administrative_sector = AdministrativeSector.find_or_create_by(name: health_facility['DA'].parameterize(separator: ' ').titleize)
  subprefecture = Subprefecture.find_or_create_by(name: health_facility['PR'].parameterize(separator: ' ').titleize.gsub(' / ','-'))
  technical_health_supervision = TechnicalHealthSupervision.find_or_create_by(name: health_facility['STS'].parameterize(separator: ' ').titleize.gsub(' / ','-'))
  regional_health_coordination = RegionalHealthCoordination.find_or_create_by(name: health_facility['CRS'].parameterize(separator: ' ').titleize)

  HealthFacility.create!(
    administrative_sector: administrative_sector,
    subprefecture: subprefecture,
    technical_health_supervision: technical_health_supervision,
    regional_health_coordination: regional_health_coordination,
    cnes: health_facility['CNES'],
    name: health_facility['CNES_DENO'],
    latitude: health_facility['LAT'],
    longitude: health_facility['LONG'],
    facility_type: health_facility['TIPO'],
    phone: health_facility['FONE'],
    beds: health_facility['LEITOS'],
    administration: health_facility['GESTAO']
  )
end

# AdministrativeSectors, RegionalHealthCoordination, Subprefecture, City, and TechnicalHealthSupervision geojson shapes
city_subdivisions = [{klass: AdministrativeSector, file: 'sp_administrative_sectors.geojson'},
                     {klass: Subprefecture, file: 'sp_subprefectures.geojson'},
                     {klass: TechnicalHealthSupervision, file: 'sp_technical_health_supervision.geojson'},
                     {klass: RegionalHealthCoordination, file: 'sp_regional_health_coordination.geojson'},
                     {klass: City, file: 'cities.geojson'},
                     {klass: Ubs, file: 'sp_ubs.geojson'},
                     {klass: FamilyHealthStrategy, file: 'sp_fhs.geojson'}
                    ]

def update_shape(klass, file)
  JSON.parse(File.read(Rails.root.join('vendor', file)))['features'].each do |geojson|
    klass_item = klass.find_or_create_by(name: geojson['properties']['Name'].parameterize(separator: ' ').titleize)
    klass_item.update_attribute(:shape, geojson['geometry'].to_json)
  end
end

city_subdivisions.each do |subdivision|
  update_shape(subdivision[:klass], subdivision[:file])
end

# CensusSectors
census_sectors = []
Dir.glob(Rails.root.join('vendor', 'census_sectors', '*.json')).each do |sector_page|
  JSON.parse(File.read(sector_page))['features'].each do |json|
    administrative_sector = AdministrativeSector.find_or_create_by(name: json['properties']['NM_DISTRIT'].parameterize(separator: ' ').titleize)
    census_sectors << { code: json['properties']['CD_GEOCODI'].to_i,
                        shape: json['geometry'].to_json,
                        total_population: json['properties']['POPULACAO_TOTAL'].split(".")[0].to_i,
                        men_population: json['properties']['POPULACAO_HOMEM'].split(".")[0].to_i,
                        women_population: json['properties']['POPULACAO_MULHER'].split(".")[0].to_i,
                        white_population: json['properties']['POPULACAO_BRANCA'].split(".")[0].to_i,
                        black_population: json['properties']['POPULACAO_PRETA'].split(".")[0].to_i,
                        yellow_population: json['properties']['POPULACAO_AMARELA'].split(".")[0].to_i,
                        mulatto_population: json['properties']['POPULACAO_PARDA'].split(".")[0].to_i,
                        native_population: json['properties']['POPULACAO_INDIGENA'].split(".")[0].to_i,
                        administrative_sector_id: administrative_sector.id,
                        created_at: Time.now,
                        updated_at: Time.now
                      }
  end
end
CensusSector.insert_all(census_sectors)

CSV.foreach(Rails.root.join('vendor', 'census_sectors', 'centroids.csv'), quote_char: '$', col_sep: ',', headers: true) do |centroid|
  CensusSector.find_by!(code: centroid['code']).update!(latitude: centroid['lat'], longitude: centroid['lng'])
end

# PatientData and HospitalizationData
AGE_CODE = %w[TP_000A004 TP_000A004 TP_000A004 TP_000A004 TP_000A004 TP_005A009 TP_005A009 TP_005A009
  TP_005A009 TP_005A009 TP_010A014 TP_010A014 TP_010A014 TP_010A014 TP_010A014 TP_015A019
  TP_015A019 TP_015A019 TP_015A019 TP_015A019 TP_020A024 TP_020A024 TP_020A024 TP_020A024
  TP_020A024 TP_025A029 TP_025A029 TP_025A029 TP_025A029 TP_025A029 TP_030A034 TP_030A034
  TP_030A034 TP_030A034 TP_030A034 TP_035A039 TP_035A039 TP_035A039 TP_035A039 TP_035A039
  TP_040A044 TP_040A044 TP_040A044 TP_040A044 TP_040A044 TP_045A049 TP_045A049 TP_045A049
  TP_045A049 TP_045A049 TP_050A054 TP_050A054 TP_050A054 TP_050A054 TP_050A054 TP_055A059
  TP_055A059 TP_055A059 TP_055A059 TP_055A059 TP_060A064 TP_060A064 TP_060A064 TP_060A064
  TP_060A064 TP_065A069 TP_065A069 TP_065A069 TP_065A069 TP_065A069 TP_070A074 TP_070A074
  TP_070A074 TP_070A074 TP_070A074 TP_075A079 TP_075A079 TP_075A079 TP_075A079 TP_075A079
  TP_080A084 TP_080A084 TP_080A084 TP_080A084 TP_080A084 TP_085A089 TP_085A089 TP_085A089
  TP_085A089 TP_085A089 TP_090A094 TP_090A094 TP_090A094 TP_090A094 TP_090A094 TP_095A099
  TP_095A099 TP_095A099 TP_095A099 TP_095A099 TP_100OUMA]

def get_age_code(age)
  if age >= 0 && age <= 99
    return AGE_CODE[age]
  elsif age > 99
    return AGE_CODE[100]
  else
    return nil
  end
end

procedures_file_path = Rails.root.join('vendor', 'procedures.csv')
if FileTest.exist?(procedures_file_path)
  patients = []
  hospitalizations = []
  CSV.foreach(procedures_file_path, quote_char: '$', col_sep: ',', headers: true) do |data|
    administrative_sector = AdministrativeSector.find_or_create_by(name: data['DA'].parameterize(separator: ' ').titleize)
    subprefecture = Subprefecture.find_or_create_by(name: data['PR'].parameterize(separator: ' ').titleize)
    technical_health_supervision = TechnicalHealthSupervision.find_or_create_by(name: data['STS'].parameterize(separator: ' ').titleize)
    regional_health_coordination = RegionalHealthCoordination.find_or_create_by(name: data['CRS'].parameterize(separator: ' ').titleize)
    health_facility = HealthFacility.find_or_create_by(cnes: data['CNES'])
    icd_subcategory = IcdSubcategory.find_by(code: data['DIAG_PR'])
    icd_category = icd_subcategory ? IcdCategory.find(icd_subcategory.icd_category_id) : nil
    secondary_icd_subcategory1 = IcdSubcategory.find_by(code: data['DIAG_SE1'])
    secondary_icd_subcategory2 = IcdSubcategory.find_by(code: data['DIAG_SE2'])
    census_sector = CensusSector.find_by(code: data['CD_GEOCODI'])
    procedure_id = SecureRandom.uuid

    patients << { administrative_sector_id: administrative_sector.id,
                  subprefecture_id: subprefecture.id,
                  technical_health_supervision_id: technical_health_supervision.id,
                  regional_health_coordination_id: regional_health_coordination.id,
                  latitude: data['LAT_SC'].to_f,
                  longitude: data['LONG_SC'].to_f,
                  age_year: data['P_IDADE'].to_i,
                  age_code: get_age_code(data['P_IDADE'].to_i),
                  educational_level: data['LV_INSTRU'].to_i,
                  gender: data['P_SEXO'],
                  race: data['P_RACA'],
                  procedure_id: procedure_id,
                  created_at: Time.now,
                  updated_at: Time.now,
                  census_sector_id: census_sector.id }
    hospitalizations << { health_facility_id: health_facility.id,
                          icd_subcategory_id: icd_subcategory.id,
                          icd_category_id: icd_category.id,
                          secondary_icd1_id: secondary_icd_subcategory1 ? secondary_icd_subcategory1.id : nil,
                          secondary_icd2_id: secondary_icd_subcategory2 ? secondary_icd_subcategory2.id : nil,
                          gestor_id: data['GESTOR_IDE'].to_i,
                          issue_date: data['DT_EMISSAO'],
                          admission_date: data['DT_INTERNA'],
                          leave_date: data['DT_SAIDA'],
                          specialty: data['ESPECIALID'].to_i,
                          hospitalization_type: data['CAR_INTEN'].to_i,
                          competence: data['CMPT'].to_i,
                          complexity: data['COMPLEXIDA'].to_i,
                          hospitalization_group: data['PROC_RE'].to_i,
                          hospitalization_days: data['DIAS_PERM'].to_i,
                          hospitalization_rates: data['DIARIAS'].to_i,
                          uti_rates: data['DIARIAS_UT'].to_i,
                          ui_rates: data['DIARIAS_UI'].to_i,
                          financing: data['FINACIAME'].to_i,
                          service_value: data['VAL_TOT'].to_f,
                          distance: data['DISTANCIA'].to_f,
                          procedure_id: procedure_id,
                          created_at: Time.now,
                          updated_at: Time.now }

    if patients.length > 999
      PatientDatum.insert_all(patients)
      HospitalizationDatum.insert_all(hospitalizations)
      patients = []
      hospitalizations = []
    end
  end
  PatientDatum.insert_all(patients)
  HospitalizationDatum.insert_all(hospitalizations)
else
  message = 'Did not find the file vendor/procedures.csv for seeds'
  Rails.logger.warn(message)
  puts "\nWARNING: #{message}\n"
end

[AdministrativeSector, RegionalHealthCoordination, TechnicalHealthSupervision, Subprefecture].each do |klass|
  if klass.where(shape: nil).exists?
    puts "\nWARNING: There is a #{klass} instance with nil shape."\
      'This is probably due using different names for the same entity between the seed input files.\n'
  end
end
