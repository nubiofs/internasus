# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_16_213249) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "administrative_sectors", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_administrative_sectors_on_name", unique: true
  end

  create_table "census_sectors", force: :cascade do |t|
    t.bigint "code"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "administrative_sector_id"
    t.integer "total_population"
    t.integer "men_population"
    t.integer "women_population"
    t.integer "white_population"
    t.integer "black_population"
    t.integer "yellow_population"
    t.integer "mulatto_population"
    t.integer "native_population"
    t.float "latitude"
    t.float "longitude"
    t.index ["administrative_sector_id"], name: "index_census_sectors_on_administrative_sector_id"
    t.index ["code"], name: "index_census_sectors_on_code", unique: true
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "family_health_strategies", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "health_facilities", force: :cascade do |t|
    t.integer "cnes"
    t.string "name"
    t.float "latitude"
    t.float "longitude"
    t.string "facility_type"
    t.string "phone"
    t.integer "beds"
    t.string "administration"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "administrative_sector_id"
    t.bigint "subprefecture_id"
    t.bigint "technical_health_supervision_id"
    t.bigint "regional_health_coordination_id"
    t.index ["administrative_sector_id"], name: "index_health_facilities_on_administrative_sector_id"
    t.index ["cnes"], name: "index_health_facilities_on_cnes", unique: true
    t.index ["regional_health_coordination_id"], name: "index_health_facilities_on_regional_health_coordination_id"
    t.index ["subprefecture_id"], name: "index_health_facilities_on_subprefecture_id"
    t.index ["technical_health_supervision_id"], name: "index_health_facilities_on_technical_health_supervision_id"
  end

  create_table "hospitalization_data", force: :cascade do |t|
    t.date "issue_date"
    t.date "admission_date"
    t.date "leave_date"
    t.integer "specialty"
    t.integer "hospitalization_type"
    t.integer "competence"
    t.integer "complexity"
    t.integer "hospitalization_group"
    t.integer "hospitalization_days"
    t.integer "hospitalization_rates"
    t.integer "uti_rates"
    t.integer "ui_rates"
    t.integer "financing"
    t.float "service_value"
    t.float "distance"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "health_facility_id"
    t.bigint "icd_subcategory_id"
    t.bigint "icd_category_id"
    t.string "procedure_id"
    t.integer "gestor_id"
    t.bigint "secondary_icd1_id"
    t.bigint "secondary_icd2_id"
    t.index ["competence"], name: "index_hospitalization_data_on_competence"
    t.index ["complexity"], name: "index_hospitalization_data_on_complexity"
    t.index ["distance"], name: "index_hospitalization_data_on_distance"
    t.index ["financing"], name: "index_hospitalization_data_on_financing"
    t.index ["health_facility_id"], name: "index_hospitalization_data_on_health_facility_id"
    t.index ["hospitalization_days"], name: "index_hospitalization_data_on_hospitalization_days"
    t.index ["hospitalization_group"], name: "index_hospitalization_data_on_hospitalization_group"
    t.index ["hospitalization_rates"], name: "index_hospitalization_data_on_hospitalization_rates"
    t.index ["hospitalization_type"], name: "index_hospitalization_data_on_hospitalization_type"
    t.index ["icd_category_id"], name: "index_hospitalization_data_on_icd_category_id"
    t.index ["icd_subcategory_id"], name: "index_hospitalization_data_on_icd_subcategory_id"
    t.index ["procedure_id"], name: "index_hospitalization_data_on_procedure_id", unique: true
    t.index ["secondary_icd1_id"], name: "index_hospitalization_data_on_secondary_icd1_id"
    t.index ["secondary_icd2_id"], name: "index_hospitalization_data_on_secondary_icd2_id"
    t.index ["service_value"], name: "index_hospitalization_data_on_service_value"
    t.index ["specialty"], name: "index_hospitalization_data_on_specialty"
    t.index ["ui_rates"], name: "index_hospitalization_data_on_ui_rates"
    t.index ["uti_rates"], name: "index_hospitalization_data_on_uti_rates"
  end

  create_table "icd_categories", force: :cascade do |t|
    t.string "code"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "icd_group_id"
    t.bigint "icd_chapter_id"
    t.index ["code"], name: "index_icd_categories_on_code", unique: true
    t.index ["icd_chapter_id"], name: "index_icd_categories_on_icd_chapter_id"
    t.index ["icd_group_id"], name: "index_icd_categories_on_icd_group_id"
  end

  create_table "icd_chapters", force: :cascade do |t|
    t.integer "code"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_icd_chapters_on_code", unique: true
  end

  create_table "icd_groups", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "icd_chapter_id"
    t.index ["icd_chapter_id"], name: "index_icd_groups_on_icd_chapter_id"
  end

  create_table "icd_subcategories", force: :cascade do |t|
    t.string "code"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "icd_category_id"
    t.index ["code"], name: "index_icd_subcategories_on_code", unique: true
    t.index ["icd_category_id"], name: "index_icd_subcategories_on_icd_category_id"
  end

  create_table "patient_data", force: :cascade do |t|
    t.float "latitude"
    t.float "longitude"
    t.string "gender"
    t.string "race"
    t.integer "age_year"
    t.string "age_code"
    t.integer "educational_level"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "administrative_sector_id"
    t.bigint "subprefecture_id"
    t.bigint "technical_health_supervision_id"
    t.bigint "regional_health_coordination_id"
    t.string "procedure_id"
    t.bigint "census_sector_id"
    t.index ["administrative_sector_id"], name: "index_patient_data_on_administrative_sector_id"
    t.index ["age_code"], name: "index_patient_data_on_age_code"
    t.index ["census_sector_id"], name: "index_patient_data_on_census_sector_id"
    t.index ["educational_level"], name: "index_patient_data_on_educational_level"
    t.index ["gender"], name: "index_patient_data_on_gender"
    t.index ["procedure_id"], name: "index_patient_data_on_procedure_id", unique: true
    t.index ["race"], name: "index_patient_data_on_race"
    t.index ["regional_health_coordination_id"], name: "index_patient_data_on_regional_health_coordination_id"
    t.index ["subprefecture_id"], name: "index_patient_data_on_subprefecture_id"
    t.index ["technical_health_supervision_id"], name: "index_patient_data_on_technical_health_supervision_id"
  end

  create_table "regional_health_coordinations", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_regional_health_coordinations_on_name", unique: true
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "subprefectures", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_subprefectures_on_name", unique: true
  end

  create_table "technical_health_supervisions", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_technical_health_supervisions_on_name", unique: true
  end

  create_table "ubs", force: :cascade do |t|
    t.string "name"
    t.string "shape"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "admin", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "hospitalization_data", "icd_subcategories", column: "secondary_icd1_id"
  add_foreign_key "hospitalization_data", "icd_subcategories", column: "secondary_icd2_id"
  add_foreign_key "icd_subcategories", "icd_categories"
end
