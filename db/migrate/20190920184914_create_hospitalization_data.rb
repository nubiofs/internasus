class CreateHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    create_table :hospitalization_data do |t|
      t.date :issue_date
      t.date :admission_date
      t.date :leave_date
      t.integer :specialty
      t.integer :hospitalization_type
      t.integer :competence
      t.integer :complexity
      t.integer :hospitalization_group
      t.integer :hospitalization_days
      t.integer :hospitalization_rates
      t.integer :uti_rates
      t.integer :ui_rates
      t.integer :financing
      t.float :service_value
      t.float :distance

      t.timestamps
      t.index :specialty
      t.index :hospitalization_type
      t.index :competence
      t.index :complexity
      t.index :hospitalization_group
      t.index :hospitalization_days
      t.index :hospitalization_rates
      t.index :uti_rates
      t.index :ui_rates
      t.index :financing
      t.index :service_value
      t.index :distance
    end
  end
end
