class CreateCensusSectors < ActiveRecord::Migration[6.0]
  def change
    create_table :census_sectors do |t|
      t.integer :code
      t.string :shape

      t.timestamps
      t.index :code, unique: true
    end
  end
end
