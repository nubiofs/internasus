class AddIcdCategoryToHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    add_reference :hospitalization_data, :icd_category
  end
end
