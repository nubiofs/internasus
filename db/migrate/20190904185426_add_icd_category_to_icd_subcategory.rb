class AddIcdCategoryToIcdSubcategory < ActiveRecord::Migration[6.0]
  def change
    add_reference :icd_subcategories, :icd_category, foreign_key: true
  end
end
