class ChangeCensusSectorCodeToBigInt < ActiveRecord::Migration[6.0]
  def up
    change_column :census_sectors, :code, :bigint
  end

  def down
    change_column :census_sectors, :code, :integer
  end
end
