class CreateRegionalHealthCoordinations < ActiveRecord::Migration[6.0]
  def change
    create_table :regional_health_coordinations do |t|
      t.string :name
      t.string :shape

      t.timestamps
      t.index :name, unique: true
    end
  end
end
