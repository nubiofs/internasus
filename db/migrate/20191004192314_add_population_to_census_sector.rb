class AddPopulationToCensusSector < ActiveRecord::Migration[6.0]
  def change
    add_column :census_sectors, :total_population, :integer
    add_column :census_sectors, :men_population, :integer
    add_column :census_sectors, :women_population, :integer
    add_column :census_sectors, :white_population, :integer
    add_column :census_sectors, :black_population, :integer
    add_column :census_sectors, :yellow_population, :integer
    add_column :census_sectors, :mulatto_population, :integer
    add_column :census_sectors, :native_population, :integer
  end
end
