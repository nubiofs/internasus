class AddIcdSubcategoryToHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    add_reference :hospitalization_data, :icd_subcategory
  end
end
