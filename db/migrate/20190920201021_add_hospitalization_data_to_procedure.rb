class AddHospitalizationDataToProcedure < ActiveRecord::Migration[6.0]
  def change
    add_reference :procedures, :hospitalization_datum
  end
end
