class AddHealthFacilityToHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    add_reference :hospitalization_data, :health_facility
  end
end
