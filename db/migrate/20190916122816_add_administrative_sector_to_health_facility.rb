class AddAdministrativeSectorToHealthFacility < ActiveRecord::Migration[6.0]
  def change
    add_reference :health_facilities, :administrative_sector
  end
end
