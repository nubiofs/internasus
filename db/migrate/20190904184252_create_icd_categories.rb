class CreateIcdCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :icd_categories do |t|
      t.string :code
      t.text :description

      t.timestamps
      t.index :code, unique: true
    end
  end
end
