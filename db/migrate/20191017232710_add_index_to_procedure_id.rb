class AddIndexToProcedureId < ActiveRecord::Migration[6.0]
  def change
    change_table :patient_data do |t|
      t.index :procedure_id, unique: true
    end
    change_table :hospitalization_data do |t|
      t.index :procedure_id, unique: true
    end
  end
end
