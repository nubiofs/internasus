class CreateTechnicalHealthSupervisions < ActiveRecord::Migration[6.0]
  def change
    create_table :technical_health_supervisions do |t|
      t.string :name
      t.string :shape

      t.timestamps
      t.index :name, unique: true
    end
  end
end
