class CreateIcdChapters < ActiveRecord::Migration[6.0]
  def change
    create_table :icd_chapters do |t|
      t.integer :code
      t.string :first_category
      t.string :last_category
      t.text :description

      t.timestamps
    end
  end
end
