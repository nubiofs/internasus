class AddAdministrativeSectorToPatientData < ActiveRecord::Migration[6.0]
  def change
    add_reference :patient_data, :administrative_sector
  end
end
