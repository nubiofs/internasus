class AddPatientDataToProcedure < ActiveRecord::Migration[6.0]
  def change
    add_reference :procedures, :patient_datum
  end
end
