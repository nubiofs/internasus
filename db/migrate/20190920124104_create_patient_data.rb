class CreatePatientData < ActiveRecord::Migration[6.0]
  def change
    create_table :patient_data do |t|
      t.float :latitude
      t.float :longitude
      t.string :gender
      t.string :race
      t.integer :age_year
      t.string :age_code
      t.integer :educational_level

      t.timestamps
      t.index :gender
      t.index :race
      t.index :age_code
      t.index :educational_level
    end
  end
end
