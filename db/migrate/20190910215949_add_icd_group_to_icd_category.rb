class AddIcdGroupToIcdCategory < ActiveRecord::Migration[6.0]
  def change
    add_reference :icd_categories, :icd_group
  end
end
