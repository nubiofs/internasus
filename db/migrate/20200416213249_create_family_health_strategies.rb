class CreateFamilyHealthStrategies < ActiveRecord::Migration[6.0]
  def change
    create_table :family_health_strategies do |t|
      t.string :name
      t.string :shape

      t.timestamps
    end
  end
end
