Feature: Procedures map

  In order to visualize procedure related georeferenced data
  As a user
  I should be able to see it in a map

  Background:
    Given I have a logged in user
    And there are sample organizational entities
    And there are sample ICD entities
    And there is a sample HealthFacility
    And there are other distant sample PatientDatum and HospitalizationDatum
    And there are other distant sample PatientDatum and HospitalizationDatum
    And there are sample PatientDatum and HospitalizationDatum
    And there are sample PatientDatum and HospitalizationDatum

  @javascript
  Scenario: Loading with data
    When I am at "/procedures"
    Then I should have no console errors
    And I should see health facilities markers
    And I should not see the patient data markers
    And I should not see the patient data cluster marker

  @javascript
  Scenario: Seeing the minimap
    When I am at "/procedures"
    Then I should have no console errors
    And I should see the minimap
    When I minimize the minimap
    Then I should see the minimized minimap

  @javascript
  Scenario: Searching
    When I am at "/procedures"
    And I click the "Buscar" button
    And I click the health facility marker
    And I click the patient data cluster marker
    And I click the patient data cluster marker
    And I click the patient data marker
    Then I should have no console errors
    And I should be at "/procedures/search"
    And I should see the patient data markers
    And I should see a popup
    And I should have no console errors
    And I should see health facilities markers
    And I should see the patient data cluster marker
    When I disable collapse transitions
    And I click the "Mapa de calor por taxa de internações" button
    Then the "csw_heat_map_women" checkbox should be checked
    When I check the "csw_heat_map_black" checkbox
    Then the "csw_heat_map_women" checkbox should be unchecked
    And the "csw_heat_map_black" checkbox should be checked
    When I check the "csw_heat_map_women" checkbox
    Then the "csw_heat_map_black" checkbox should be unchecked
    And the "csw_heat_map_women" checkbox should be checked
    When I click the "Agrupamento" button
    And I uncheck the "clustering_enabled" checkbox
    Then I should have no console errors
    And I should not see the patient data cluster marker
    When I check the "clustering_enabled" checkbox
    Then I should have no console errors
    And I should see the patient data cluster marker
    And I should see 2 patient data cluster markers
    When I drag the clustering radius slider to a higher value
    And I should see 1 patient data cluster markers
    When I click the "Mapa de calor" button
    And I check the "heat_map_enabled" checkbox
    And I set the slider with id "heat_map_radius" value to "30"
    Then I should have no console errors
    And I set the slider with id "heat_map_opacity" value to "0.5"
    Then I should have no console errors
    When I check the "heat_map_high_contrast" checkbox
    Then I should have no console errors
    When I uncheck the "heat_map_high_contrast" checkbox
    Then I should have no console errors
    When I uncheck the "heat_map_enabled" checkbox
    Then I should have no console errors
    When I check the "csw_heat_map_enabled" checkbox
    Then I should have no console errors
    When I uncheck the "csw_heat_map_enabled" checkbox
    Then I should have no console errors
    When I click the "Limites administrativos" button
    Then I should be able to toggle "show_cs_limits"
    And I should be able to toggle "show_city_limits"
    And I should be able to toggle "show_ubs_limits"
    And I should be able to toggle "show_fhs_limits"
    And I should be able to toggle "show_rhc_limits"
    And I should be able to toggle "show_ths_limits"
    And I should be able to toggle "show_subprefecture_limits"
    And I should be able to toggle "show_administrative_sectors_limits"
    When I click the "Estabelecimentos" button
    And I uncheck the "health_facilities_enabled" checkbox
    Then I should have no console errors
    And I should not see health facilities markers
    When I check the "health_facilities_enabled" checkbox
    Then I should have no console errors
    And I should see health facilities markers
    When I check the "health_facilities_percentiles_enabled" checkbox
    Then I should have no console errors
    When I uncheck the "health_facilities_percentiles_enabled" checkbox
    Then I should have no console errors
    When I set the slider with id "q_health_facility_beds_between" values to "0" and "0"
    And I click the "Buscar" button
    Then I should have no console errors
    And I should not see the patient data markers
    And I should not see the patient data cluster marker
    And I should not see health facilities markers

  @javascript
  Scenario: Printing
    When I am at "/procedures"
    And I click the "PDF" link
    Then I should have no console errors

  Scenario: CSV download
    When I am at "/procedures"
    And I click the "Buscar" button
    And I click the "CSV" link
