# frozen_string_literal: true

Given('there is a sample HealthFacility') do
  @health_facility = FactoryBot.create(:health_facility,
                                       latitude: -23.547985,
                                       longitude: -46.665113,
                                       administrative_sector: @administrative_sector,
                                       subprefecture: @subprefecture,
                                       technical_health_supervision: @technical_health_supervision,
                                       regional_health_coordination: @regional_health_coordination)
end
