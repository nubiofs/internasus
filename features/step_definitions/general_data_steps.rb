# frozen_string_literal: true

Then('I should see {string} table') do |table_number|
  find("#ranking-table-#{table_number}")
end

Then('I should not see {string} table') do |table_number|
  find("#ranking-table-#{table_number}", visible: :hidden)
end
