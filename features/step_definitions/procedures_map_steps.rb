# frozen_string_literal: true

Given('there are sample PatientDatum and HospitalizationDatum') do
  @procedure_id = SecureRandom.uuid

  @hospitalization_datum = FactoryBot.create(:hospitalization_datum,
                                             health_facility: @health_facility,
                                             icd_category: @icd_category,
                                             icd_subcategory: @icd_subcategory,
                                             procedure_id: @procedure_id)

  @patient_datum = FactoryBot.create(:patient_datum,
                                     latitude: -23.54398,
                                     longitude: -46.63639,
                                     administrative_sector: @administrative_sector,
                                     subprefecture: @subprefecture,
                                     technical_health_supervision: @technical_health_supervision,
                                     regional_health_coordination: @regional_health_coordination,
                                     census_sector: @census_sector,
                                     procedure_id: @procedure_id)
end

Given('there are other distant sample PatientDatum and HospitalizationDatum') do
  coord = { lat: -23.54598, lon: -46.69639 }

  @procedure_id = SecureRandom.uuid

  @hospitalization_datum = FactoryBot.create(:hospitalization_datum,
                                             health_facility: @health_facility,
                                             icd_category: @icd_category,
                                             icd_subcategory: @icd_subcategory,
                                             procedure_id: @procedure_id)

  @patient_datum = FactoryBot.create(:patient_datum,
                                     latitude: coord[:lat],
                                     longitude: coord[:lon],
                                     administrative_sector: @administrative_sector,
                                     subprefecture: @subprefecture,
                                     technical_health_supervision: @technical_health_supervision,
                                     regional_health_coordination: @regional_health_coordination,
                                     census_sector: @census_sector,
                                     procedure_id: @procedure_id)
end

When('I minimize the minimap') do
  find('a.leaflet-control-minimap-toggle-display').click
end

When('I click the patient data cluster marker') do
  find('div.marker-cluster div span', match: :first).click
end

When('I click the patient data marker') do
  find('img.leaflet-marker-icon:not(.health_facility_icon)', match: :first).click
end

When('I click the health facility marker') do
  find('img.leaflet-marker-icon.health_facility_icon', match: :first).click
end

Then('I should see health facilities markers') do
  expect(page).to have_selector('img.leaflet-marker-icon.health_facility_icon')
end

Then('I should see the patient data cluster marker') do
  expect(page).to have_selector('div.marker-cluster div span')
end

Then('I should see {int} patient data cluster markers') do |markers|
  expect(find_all('div.marker-cluster div span').count).to eq(markers)
end

When('I drag the clustering radius slider to a higher value') do
  el = page.driver.browser.find_element(css: 'div#clustering_radius_container .min-slider-handle')
  page.driver.browser.action.drag_and_drop_by(el, 100, 0).perform
end

Then('I should see the patient data markers') do
  expect(page).to have_selector('img.leaflet-marker-icon:not(.health_facility_icon)')
end

Then('I should see a popup') do
  expect(page).to have_selector('div.leaflet-popup-content')
end

Then('I should not see health facilities markers') do
  expect(page).to_not have_selector('img.leaflet-marker-icon.health_facility_icon')
end

Then('I should not see the patient data cluster marker') do
  expect(page).to_not have_selector('img.leaflet-marker-icon:not(.health_facility_icon)')
end

Then('I should not see the patient data markers') do
  expect(page).to_not have_selector('img.leaflet-marker-icon:not(.health_facility_icon)')
end

Then('I should see the minimap') do
  expect(page).to have_selector('div.leaflet-control-minimap')
end

Then('I should see the minimized minimap') do
  expect(page).to have_selector('a.minimized-bottomright')
end

Then('I should be able to toggle {string}') do |field|
  step "I check the \"#{field}\" checkbox"
  step 'I should have no console errors'
  step "I uncheck the \"#{field}\" checkbox"
  step 'I should have no console errors'
end
