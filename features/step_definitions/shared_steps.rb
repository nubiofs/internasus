# frozen_string_literal: true

Given('I am at {string}') do |path|
  visit(path)
end

Given('there are sample organizational entities') do
  @administrative_sector ||= FactoryBot.create(:administrative_sector)
  @subprefecture ||= FactoryBot.create(:subprefecture)
  @technical_health_supervision ||= FactoryBot.create(:technical_health_supervision)
  @regional_health_coordination ||= FactoryBot.create(:regional_health_coordination)
  @census_sector ||= FactoryBot.create(:census_sector, administrative_sector: @administrative_sector)
end

Given('there are sample ICD entities') do
  @icd_category ||= FactoryBot.create(:icd_category)
  @icd_subcategory ||= FactoryBot.create(:icd_subcategory, icd_category: @icd_category)
end

When('I fill the {string} input with {string}') do |input_name, value|
  fill_in input_name, with: value
end

When('I click the {string} button') do |button_text|
  click_button(button_text)
end

When('I set the slider with id {string} values to {string} and {string}') do |id, start, ending|
  page.execute_script("$(\'##{id}\').slider('setValue', [#{start}, #{ending}], true, true)")
end

When('I set the slider with id {string} value to {string}') do |id, value|
  page.execute_script("$(\'##{id}\').slider('setValue', #{value}, true, true)")
end

When('I select {string} from {string}') do |option, select_identifier|
  select option, from: select_identifier
end

When('I check the {string} checkbox') do |checkbox_id|
  check(checkbox_id)
end

When('I uncheck the {string} checkbox') do |checkbox_id|
  uncheck(checkbox_id)
end

When('I disable collapse transitions') do
  page.execute_script("$('.collapse').css('transition', 'none')")
end

When('I click the {string} link') do |link|
  click_link(link)
end

Then('I should see {string}') do |text|
  expect(page).to have_content(text)
end

Then('I should have no console errors') do
  errors = page.driver.browser.manage.logs.get(:browser)

  if errors.present?
    aggregate_failures 'javascript errors' do
      errors.each do |error|
        expect(error.level).not_to eq('SEVERE'), error.message
        next unless error.level == 'WARNING'

        warn 'WARN: javascript warning'
        warn error.message
      end
    end
  end
end

Then('I should be at {string}') do |path|
  expect(page).to have_current_path(path)
end

Then('the {string} checkbox should be checked') do |checkbox_id|
  expect(find("##{checkbox_id}")).to be_checked
end

Then('the {string} checkbox should be unchecked') do |checkbox_id|
  expect(find("##{checkbox_id}")).to_not be_checked
end
