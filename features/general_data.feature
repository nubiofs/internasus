Feature: General data

  In order to visualize procedure related charts and tables
  As a user
  I should be able to see those tables and charts divided into sections

  Background:
    Given I have a logged in user
    When I am at "/general_data"

  Scenario: Fixed characterization section
    Then I should see "Base de Dados"
    When I click the "Rankings" button
    Then I should see "Base de Dados"

  @javascript
  Scenario: Loading Rankings data
    Given I disable collapse transitions
    When I click the "Rankings" button
    And I select "Setor Administrativo" from "ranking_category"
    Then I should see "0" table
    And I should not see "2" table
    When I select "Sexo" from "ranking_category"
    Then I should see "2" table
    And I should not see "0" table
    Then I should have no console errors

  @javascript
  Scenario: Loading Dynamic charts
    Given I disable collapse transitions
    When I click the "Gráfico Dinâmico (selecione a variável)" button
    Then I should have no console errors
    When I select "Faixa Etária" from "dynamic_chart_options"
    Then I should have no console errors

  @javascript
  Scenario: Charts
    When I click the "Série Histórica" button
    Then I should have no console errors
    When I click the "Especialidades" button
    Then I should have no console errors
    When I click the "Estabelecimentos" button
    Then I should have no console errors
    When I click the "Distâncias Por Especialidade dos Estabelecimentos" button
    Then I should have no console errors
    When I click the "Internações / Pacientes" button
    Then I should have no console errors
    When I click the "Territórios e Regiões Administrativas" button
    Then I should have no console errors
    When I click the "Distâncias e Deslocamento" button
    Then I should have no console errors

  Scenario: Census sector data
    When I click the "Taxas Por Setor Censitário" button
    Then I should see "População total"
