Rails.application.routes.draw do
  devise_for :users
  root 'homepage#index'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :procedures, only: [:index, :show] do
    post 'search/', to: 'procedures#search', on: :collection
    get 'search/', to: 'procedures#index', on: :collection
    get 'export/', to: 'procedures#export', on: :collection
    post 'print/', to: 'procedures#print', on: :collection
  end

  resources :health_facilities, only: [:show]

  resources :general_data, only: [:index] do
    post 'print/', to: 'general_data#print', on: :collection
  end
end
